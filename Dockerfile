# Utilisez une image nginx stable
FROM nginx:stable-alpine

# Copiez les fichiers de construction de Docusaurus dans le répertoire de contenu nginx
COPY build/ /usr/share/nginx/html/

# Copiez la configuration nginx pour le site Docusaurus
COPY nginx.conf /etc/nginx/nginx.conf

# Exposez le port nginx par défaut
EXPOSE 80

# Démarrez nginx
CMD ["nginx", "-g", "daemon off;"]
