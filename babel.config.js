module.exports = {
  presets: [require.resolve('@docusaurus/core/lib/babel/preset')],
};
const siteConfig = {
  // ...
  markdownPlugins: [
    // Highlight admonitions.
    require('remarkable-admonitions')({ icon: 'svg-inline' })
  ]
};