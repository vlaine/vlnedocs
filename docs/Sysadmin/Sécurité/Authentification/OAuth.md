OAuth
---
Linked to [[Sysadmin/Sécurité/Authentification/Authentification Windows]]

En gros c'est le fait de se connecter à une session avec un compte d'un autre site, comme quand on se co avec google ou facebook sur un site tiers.

![Pasted image 20230306111859.png](img/Pasted%20image%2020230306111859.png)