Sort IDS
---
- https://www.manpagez.com/man/8/snort/
- https://snort-org-site.s3.amazonaws.com/production/document_files/files/000/000/596/original/Rules_Writers_Guide_to_Snort_3_Rules.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAU7AK5ITMJQBJPARJ%2F20230508%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230508T105215Z&X-Amz-Expires=172800&X-Amz-SignedHeaders=host&X-Amz-Signature=933f93e450b7997e0d0245f414ff615ce256807905c716d2611369f672e05a48
- https://www.sapphire.net/security/snort-rules-examples/
- https://cyvatar.ai/write-configure-snort-rules/


### Installation 
https://snort-org-site.s3.amazonaws.com/production/release_files/files/000/031/458/original/snort_user.html?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAU7AK5ITMJQBJPARJ%2F20230508%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230508T131356Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=6e850542354b22529ce9d28078fc7d1049618e0ff9b7c20b28b2459130f1267f#_dependencies

```bash
1.  sudo apt-get install liblzma-dev libhwloc-dev
```

-   daq from [https://github.com/snort3/libdaq](https://github.com/snort3/libdaq) for packet IO
-   hwloc from [https://www.open-mpi.org/projects/hwloc/](https://www.open-mpi.org/projects/hwloc/) for CPU affinity management


Snort has several actions which can be used:

-   **alert** generate an alert using the selected alert method, and then log the packet
-   **log** log the packet
-   **pass** ignore the packet
-   **activate** alert and then turn on another dynamic rule
-   **dynamic** remain idle until activated by an activate rule , then act as a log rule
-   **drop** block and log the packet
-   **reject** block the packet, log it, and then send a TCP reset if the protocol is TCP or an ICMP port unreachable message if the protocol is UDP.
-   **sdrop** block the packet but do not log it.

```bash
alert icmp any any -> $HOME_NET any (msg:"ICMP test"; sid:10000001; rev:001;)
alert icmp any any -> $HOME_NET any (msg: "NMAP ping sweep Scan"; dsize:0;sid:10000004; rev: 1;)
alert tcp any any -> $HOME_NET any (msg: "NMAP TCP Scan";sid:10000005; rev:2; )
alert tcp any any -> $HOME_NET any (msg:"Nmap XMAS Tree Scan"; flags:FPU; sid:1000006; rev:1; )
alert tcp any any -> $HOME_NET any (msg:"Nmap FIN Scan"; flags:F; sid:1000008; rev:1;)
alert tcp any any -> $HOME_NET any (msg:"Nmap NULL Scan"; flags:0; sid:1000009; rev:1; )
alert udp any any -> $HOME_NET any ( msg:"Nmap UDP Scan"; sid:1000010; rev:1; )
```

Ecouter pour vérifier ses règles : 

```css
sudo snort -A console -q -u snort -g snort -c /etc/snort/snort.conf -i eth0
```

Valider sa conf :

```bash
sudo snort -T -c /etc/snort/snort.conf
```

psad : 

https://www.unixmen.com/how-to-block-port-scan-attacks-with-psad-on-ubuntu-debian/

