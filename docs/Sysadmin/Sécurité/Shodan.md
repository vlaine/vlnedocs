Shodan
---

- 21 FTP anonymous : `"220" "230 Login successful." port:21`
	- 220 : Search for login FTP page
	- 230 : Login successful request

- Android IP webcam scanner : `"Server: IP Webcam Server" "200 OK"`
- Another webcam stuff : `cgi-bin/guestimage.html`

- SMB disabled authentification : `"Authentication: disabled" port:445`
- Hacked Ubiquiti Devices: `hacked Ubiquiti`
- Find ESXI compromised : `html:"We hacked your company successfully" title:"How to Restore Your Files"`
	- Une attaque mondiale par ransomware a touché des milliers de serveurs utilisant l'hyperviseur VMware ESxi, et de nombreux autres serveurs devraient être affectés.
- Already logged in as root via telnet : `"root@" port:23 -login -password -name -Session`
- Exposed Wordpress files which contain database credentials : `http.html:"* The wp-config.php creation script uses this file"`
- Get Access to Surveillance Cams with Login and Password : `NETSurveillance uc-httpd Server: uc-httpd 1.0.0`
	- Attention HoneyPot fréquents
-  Manage MongoDB without Authentication : `"Set-Cookie: mongo-express=""200 OK"`
- Show docker container exposed : `Docker
- Uncovering the Hidden: Techniques for Locating Website Admin Panels : 
```vbnet
ssl.cert.subject.cn:"company.com "http.title:" admin "  

ssl: "company.com" http.title: "admin"  

ssl.cert.subject.cn:"company.com "admin  

ssl: "company.com" admin
```
`

- 

