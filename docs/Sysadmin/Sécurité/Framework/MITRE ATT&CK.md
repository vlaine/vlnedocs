# MITRE ATT&CK

---

## Le webinaire 
##### Synthèse webinaire Identity Days
https://www.youtube.com/watch?v=Z5DRooPwsT0

Par Sylvain Cortes
Sponsorisé par Hackuity, agrégateur de vulnérabilité

![Pasted image 20230207172108.png](../img/mitre/Pasted%20image%2020230207172108.png)


Hackuity va récupérer les différentes informations des différents scanners de vulnérabilité, ainsi que d'éventuels autres outils d'audit, pour les agréger après un traitement pour vous indiquer les menaces prioritaires à gérer.

# Le Mitre

:::note  Définition
> 
> ### Définition 
> 
> Mitre est un Framework de sécurité qui aide la partie défensive à comprendre la partie offensive.
> 
> C'est l'équivalent d'une association, financé par le "gouvernement Américain". C'est une grande organisation qui à pour objectif de synchroniser et d'organiser tous les efforts nationaux en termine de Cyber aux États-Unis.
> 
> Dans ce cadre ils ont énormément de mandats. Un mandat c'est une sorte de projet à gérer ou une action à réaliser pour le gouvernement américain au nom du gouvernement américain.
> 
> Au travers ces mandats ils ont énormément de partenariat avec d'autres entités américaines qui peuvent être soit des autres entités qui travaillent dans le monde de la cyber ou tout simplement des entités nationales autour ou des ministères particuliers aux États-Unis.
> 
> Retrouvez des infos sur le site de Mitre. www.mitre.org
:::


Nous nous intéresserons ici à Mitre Att&ck mais ils ont d'autres projets liés à d'autre domaines. Comme par exemple la base CVE (NVD) (National Vulnerability Database)
https://mitre-engenuity.org/

Le Framework Mitre Att&ck n'est qu'un des projets de Mitre. Ne pas confonde Mitre et Mitre Engenuity.

Mitre et une association qui a pour vocation d'aider et de développer des grands projets "[agnostique](https://fr.wiktionary.org/wiki/agnostique)" en terme d'usage. Typiquement la base de données CVE ou le Framework Mitre Att&ck sont pour le publique tandis que Mitre Engenuity est le pendant professionnel de Mitre.

Donc [Mitre Engenuity](https://mitre-engenuity.org/) a été créé pour appliquer ou pour aider à l'application des grands principes qui vont être édictés parmi Mitre.

Il faut vraiment voir Mitre comme quelque chose de gouvernemental, une association qui génère des grands projets et des projets génériques et Mitre Engenuity est l'application professionnelle de ces projets. Ils sont porteurs de d'éléments importants vis-à-vis du Framework Mitre Att&ck.

https://attack.mitre.org/

---
### Les autres organisations

- Le CISA (cisa.org) : Cert US, fourni des services de cert aux entités publiques américaines.
https://www.cisa.gov/uscert/ncas/alerts/aa21-048a


- Le Nist (nist.gov) : Standards techniques précis et publication de livre blanc de bonnes pratiques de sécurités. 
	- Certains projets sont commun avec Mitre.


## Les Framework Mitre 

#### Trois grand Framework principaux.

- Enterprise : Framework accessible au public toutes technos confondus
- Mobile : Déclinaison de la partie entreprise mais dans le monde des technologies mobiles
- ics : Déclinaison dans le monde industriel

### C'est quoi un Framework ?

:::info 
> 
> Un Framework a pour rôle de vous donner des pistes pour comprendre ce que les attaquants font et en réaction, être en capacité de mettre en place les mesures défensives une fois que vous avez bien compris ce qui est fait par l'attaquant.
:::

#### Mise à jour de Framework

:::note  Les évolutions du Framework
> 
> https://attack.mitre.org/resources/versions/ 
:::

#### Contre-mesures : 

:::note  Pendant défensif de Mitre Att&ck
> 
> https://attack.mitre.org/mitigations/enterprise/
Liste de tâche à faire pour contrer des éléments présents dans le Framework Mitre Att&ck. Codifier par M et un numéro (M1550).
:::

## Mitre Att&ck

A la base Mitre provient, d'une certaine façon, de l'évolution de la Cyber Kill Chain.

L'objectif c'est de la Cyber Kill Chain c'est de décrire les étapes qu'un attaquant va réaliser pour attaquer votre organisation.

![Pasted image 20230207180736.png](../img/mitre/Pasted%20image%2020230207180736.png)

Cela ne donne pas les techniques utilisées par les attaquants, mais plutôt les grands principes.
Cela reste imprécis, l'idée de Mitre Att&ck Entreprise est de définir  tous les éléments qui seront utilisé par un attaquant pour réaliser ce qu'on appelle un **chemin d'attaque**.

:::note 
> L'attaquant a pour objectif le point B, mais il a plusieurs moyens d'accéder à  ses objectifs. Cela peut être des sauts d'une CVE non patché, une mauvaise configuration etc...
> 
> Nous pouvons résumer "toutes les attaques" à l'exploitation d'une CVE non patché ou l'exploitation d'une mauvaise configuration du système.
:::

Ce chemin d'attaque, cette arborescence de la KillChain, passe par certaines techniques utilisés, qui sont justement décrites par le Mitre Att&ck Enterprise.


:::tip  Lien
> 
> Matrix : 
>  https://attack.mitre.org/matrices/enterprise/
> 
:::


Dans la matrice mitre, nous trouverons ce qu'on appelle les tactiques.
Ce sont les étapes qui vont être utilisées par l'attaquant pour compromettre votre environnement.

On peut considérer plus ou moins que les tactiques que vous voyez à l'écran sont chronologique, ce n'est  pas toujours exactement le cas, mais typiquement nous allons d'abord faire de la reconnaissance et ensuite on va aller attaquer système.

---

Maintenant parfois il y a des tactiques en amont de pas mal d'action au niveau du système, et c'est quelque chose qui peut être répétitif.

Il peut y avoir un premier Discovery qui est lié à un LAN, puis un deuxième Discovery sur un autre LAN en rebond, grâce à une montée en privilège par exemple, qui sera donc une autre partie du réseau.

Il y a certaines tactiques qu'on va retrouvera dans la Kill Chain et dans la séquence d'attaque plusieurs fois.

Mais globalement on peut considérer que dans une **tactique**, vous avez **l'appel des techniques**. Les techniques qui seront les techniques utilisées par les attaquants pour réaliser au sein de cette tactique et les différentes actions nécessaires depuis Mitre Att&ck.

Egalement inséré au sein de certaines techniques, il y a ce qu'on appelle la matrice.
Toutes les techniques avec l'encart gris à droite correspondent à des techniques associées à la techniques de base, appelé les subs-techniques. Les subs-techniques sont une manière d'effectuer la technique de différentes manières, par exemple pour l'élévation de privilège : 

![Pasted image 20230209105325.png](../img/mitre/Pasted%20image%2020230209105325.png)

Dans le Framework Mitre Att&ck nous voyons marqué reconnaissance et Resource Development. Ce sont deux tactiques sont également connus comme s'appelant la **pré-matrice**.


Pourquoi ? Ces deux tactiques sont des choses qui sont faites en amont de la première charge offensive, au niveau de la matrice, c'est tout ce qui concerne la reconnaissance externe.
![Pasted image 20230210120445.png](../img/mitre/Pasted%20image%2020230210120445.png)


La reconnaissance externe est différente de la reconnaissance interne, car la reconnaissance externe peut utiliser des sources ouvertes, telles que les réseaux sociaux, pour trouver des informations sur la cible. 

Par exemple, si une entreprise recrute un spécialiste Cisco, cela signifie qu'ils ont probablement des pare-feux fixes à l'intérieur. De même, si une entreprise recherche un spécialiste du stockage, cela signifie qu'ils ont peut-être des problèmes avec leur système de stockage (cqfd). 

:::info 
> 
> Il existe plusieurs moyens de faire la reconnaissance externe, et en utilisant simplement des sources ouvertes, un expert en la matière peut trouver beaucoup d'informations. 
> 
:::

Ces deux tactiques sont souvent connues sous le nom de **"pré-attaque"**, car elles se produisent avant l'attaque réelle au niveau du système. Lorsque vous utilisez le modèle de données de la matrice ATT&CK, vous pouvez voir les différentes tactiques et techniques associées à une technique en particulier. Par exemple, lorsque vous cliquez sur une technique sur la page web de la matrice, vous accédez à un modèle de données qui vous permet de collecter des informations. Chaque objet dans le framework a un numéro commençant par une lettre.

Les tactiques de reconnaissance interne et externe sont souvent considérées comme faisant partie de ce qu'on appelle la "pré-matrice". Cela signifie qu'elles sont effectuées avant l'attaque réelle sur le système. Lorsqu'on examine la matrice, on trouve différentes tactiques et techniques. 
Par exemple, en cliquant sur une technique dans la matrice sur le site web, on peut accéder à un modèle de données pour recueillir des informations. 

:::info 
> 
> Chaque technique ou objet dans le Framework a un numéro, commençant par une lettre, qui peut être utilisé pour explorer la matrice et collecter des informations.
:::


---

:::info Important  Pour résumer
> 
> 1.  **Matrices**: Les matrices de Mitre Att&ck sont des outils visuels qui permettent de visualiser et de comprendre les méthodes et les techniques utilisées par les attaquants pour menacer la sécurité informatique. Elles présentent les informations de manière structurée et cohérente, permettant aux utilisateurs de voir les tendances et les patters de menace. Il existe plusieurs matrices dans Att&ck, telles que les matrices d'entreprise, de plate-forme et de logiciel.
> 
> 2.  **Sources de données (Data Sources**): Les sources de données sont des références à des bases de données, des outils, des blogs et des articles qui peuvent être utilisés pour aider à comprendre et à analyser les techniques et les menaces Att&ck.
>  
> 3.  **Exemples de procédures (Procedures Examples)**: Les exemples de procédures sont des illustrations de comment les techniques et les tactiques Att&ck peuvent être mises en œuvre dans le monde réel. Ils fournissent un aperçu concret de comment les menaces peuvent être utilisées pour compromettre la sécurité informatique.
> 
> 4.  **Mitigations**: Les mitigations sont des mesures de défense mises en place pour empêcher ou limiter les menaces. Elles incluent des pratiques de sécurité telles que la configuration sécurisée des systèmes, la formation du personnel et la mise en place de solutions de détection et de réponse aux incidents.
> 
> 5.  **Détection**: La section de détection décrit comment détecter les techniques et les tactiques Att&ck. Elle inclut des informations sur les indicateurs de compromission (IOC) qui peuvent être utilisés pour détecter les menaces et des conseils sur les stratégies de détection.
> 
> 6.  **Références**: La section des références inclut des liens vers des articles, des blogs et des publications qui peuvent aider à comprendre les menaces Att&ck.
> 
> 7.  **Groupes**: Les groupes sont des collections de techniques et de tactiques qui sont souvent utilisées ensemble par les attaquants. Les groupes peuvent être utilisés pour comprendre les tendances dans les menaces et pour élaborer des stratégies de défense.
> 
> 8.  **Logiciel (Software)**: La section de logiciel décrit les produits et les technologies de sécurité qui peuvent être utilisés pour se protéger contre les menaces Att&ck. La section Software décrit les logiciels utilisés par les groupes malveillants pour mener leurs attaques. Cette section peut inclure des informations sur les fonctionnalités du logiciel, sa popularité auprès des groupes malveillants et les vulnérabilités connues.
> 
> 9.  **Tactics**: Les tactiques décrivent les actions que les attaquants peuvent entreprendre pour accomplir leurs objectifs. Elles incluent des actions telles que l'exfiltration de données, la reconnaissance et la persistences, l'ingénierie sociale, le déploiement de logiciels malveillants...
> 
> 11. **Campagnes (Campaigns)** : décrivent des séries coordonnées d'attaques menées par des groupes malveillants contre des cibles spécifiques. Les informations sur les campagnes peuvent inclure des détails sur les cibles, la durée de la campagne et les techniques utilisées.
:::

---
## Mitre - Tools & Data

Sur le site web il y a pleins d'outils autour du framework.

**ATT&CK in Excel** : Version de Mitre Att&ck mais au format Excel.
https://attack.mitre.org/resources/working-with-attack/

### ATT&CK in STIX

Normalise les données qu'on va ingérer dans de la CTI. Très intéressant de mapper les infos de CTI avec un Framework, pour les visualiser graphiquement par exemple.
Le format des données de CTI est STIX.

:::caution 
> 
> **Ne pas confondre STXI et TAXII** !
> 
> STIX un JSON qui documenté des infos de CTI, initié par le CISA, et est maintenant fourni à Mitre et Oasis
> TAXII un protocole qui permet d'échanger entre différents outils les données de CTI
> Les deux n'ont rien à voir
:::


- https://github.com/mitre-attack/attack-stix-data : le Mitre Att&ck au format Stix (Les 3 Framework principaux)

- https://github.com/OpenCTI-Platform/opencti : Peux ingérer et traiter du Stix, à tester. Plateforme simple et bien conçu pour faire du CTI.

- https://github.com/center-for-threat-informed-defense/attack-workbench-frontend
Att&ck Workbench pour récupérer les éléments de mitre et des les enrichir en terme d'information, projet à télécharger offline aussi, en quelque sorte, un mini CTI dédié à notre usage.

---
- Vous disposez d'un ensemble de script qui vous permettent de récupérer, manipuler, ingérer extraire des données depuis mitre et qui sont sous la forme de script Python.

:::tip  Liens
> 
> - https://github.com/mitre-attack/attack-scripts : Scripts fourni par Mitre Att&ck
> - https://github.com/mitre-attack/mitreattack-python : Modules python pour manipuler les données 
:::

---
### Att&ck Navigator

Outil très connu. Interface qui permet en rajoutant des filtres des couleurs etc.. de nous représenter sur une interface nos risques dans l'entreprise.

:::tip 
> 
> Je récupère mes confs ou sont les vulnérabilités et j'ajoute dans mitre pour mettre le focus sur les éléments importants.
:::


Utilisable en local ou avec le service en ligne de navigator.
- Online : https://mitre-attack.github.io/attack-navigator/ 
- Offline : https://github.com/mitre-attack/attack-navigator

Cela nous permettra d'avoir notre matrice pour chercher des techniques, mots clef, les afficher d'une couleur...

:::tip 
> 
> Surligner des éléments qui corresponde a tel ou tel technique pour y découvrir les tactiques employés, les sub techniques auxquelles nous pourrions être vulnérable et s'en protéger via la partie mitigation.
:::


## Mitre Training

:::note 
> 
> [MITRE ATT&CK Defender™ (MAD)](https://mitre-engenuity.org/cybersecurity/mad/) est un programme de formation et d'accréditation conçu par Mitre Engenuity pour vous former et vous certifier sur le Framework. Destiné aux personnes qui cherchent à renforcer leur approche de défense en matière de sécurité en tenant compte des menaces. 
> 
> https://mitre-engenuity.org/cybersecurity/mad/
> 
:::


- Inscription puis aller dans les programmes
- Cours en ligne
- Cours sur [Cybrary ](https://www.cybrary.it/)pour le cours en ligne de la certif (y a du gratuits pour mitre et mad)
- Certif payant MAD annuel 499$
- Permet d'avoir des badges officiels délivrés par Mitre

:::info Important 
> 
> L'objectif, dans un contexte d'entreprise, n'est pas de couvrir complètement Mitre Att&ck mais de nous aider à définir nos risques et nos menaces dans l'entreprise, ce n'est pas la définition du risque en tant que tel. C'est un des outils qui permet de définir les risques associés aux méthodes des attaquants.
:::

---

### Sources & Annexes

- https://www.youtube.com/watch?v=Z5DRooPwsT0
- https://fr.linkedin.com/company/identity-days
- https://identitydays.com/ 
- https://www.hackuity.io/

Auteur de la synthèse : [Vincent LAINE](https://www.linkedin.com/in/vincent-laine5/)