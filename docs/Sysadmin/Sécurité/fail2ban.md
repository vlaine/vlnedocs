fail2ban
---

The best way to have a look at the full list of IPs that have been blocked would be to check the log file:

```
sudo zgrep 'Ban' /var/log/fail2ban.log*
```

_Edit_: this answer previously searched for `'Ban:'`, but even in 2013 the source has no colon ([ref](https://github.com/fail2ban/fail2ban/blob/dba88e842f3583ce7f213d06892db7f68d890209/fail2ban/server/actions.py#L180)).

The following command can also give you a clean list of input rules:

```
sudo iptables -L INPUT -v -n | less
```

```undefined
sudo service fail2ban status
sudo fail2ban-client status
sudo fail2ban-client status sshd
```


Voir IP banni avec country : 
```bash
cat /var/log/fail2ban.log* | grep Ban | sed 's/.*[Bb]an \(.*\)/\1/' | uniq | while read line; do geoiplookup $line; done | sort | uniq -c | sort -nr
```

Voir toutes les ip : 
```makefile
sudo fail2ban-client status | sudo grep "Jail list:" | sudo sed "s/ //g" | sudo awk '{split($2,a,",");for(i in a) system("fail2ban-client status " a[i])}' | sudo grep "Status\|IP list"
```

```css
sudo psad --fw-list
psad -S
/var/log/psad/status.out
/var/mail/root
```


http://opentodo.net/2013/04/blocking-port-scan-attacks-with-psad/