Plateforme Hack Training
---

Platforms to skill up your Hacking, Pentesting, CTF, Defense skills ✨✨  
  
🔹Academy Hackaflag BR - [https://hackaflag.com.br](https://hackaflag.com.br/)  
🔹Attack-Defense - [https://attackdefense.com](https://attackdefense.com/)  
🔹Alert to win - [https://alf.nu/alert1](https://alf.nu/alert1)  
🔹Bough Bounty Hunter - [https://lnkd.in/eXnhCnnK](https://lnkd.in/eXnhCnnK)  
🔹CTF Komodo Security - [https://ctf.komodosec.com](https://ctf.komodosec.com/)  
🔹CTF Time - [https://ctftime.org/](https://ctftime.org/)  
🔹CMD Challenge - [https://cmdchallenge.com](https://cmdchallenge.com/)  
🔹Exploitation Education - [https://exploit.education](https://exploit.education/)  
🔹Google CTF - [https://lnkd.in/eyeUUKjA](https://lnkd.in/eyeUUKjA)   
🔹HackTheBox -  [https://hackthebox.com](https://hackthebox.com/)  
🔹Hackthis - [https://hackthis.co.uk](https://hackthis.co.uk/)  
🔹Hacksplaining - [https://lnkd.in/gME2pGBr](https://lnkd.in/gME2pGBr)   
🔹Hacker101 - [https://ctf.hacker101.com](https://ctf.hacker101.com/)  
🔹Hacker Security - [https://lnkd.in/ex7R-C-e](https://lnkd.in/ex7R-C-e)  
🔹Hacking-Lab - [https://hacking-lab.com](https://hacking-lab.com/)  
🔹HackmyVM - [https://hackmyvm.eu/](https://hackmyvm.eu/)  
🔹HSTRIKE - [https://hstrike.com](https://hstrike.com/)  
🔹ImmersiveLabs - [https://immersivelabs.com](https://immersivelabs.com/)  
🔹LetsDefend - [https://letsdefend.io/](https://letsdefend.io/)  
🔹NewbieContest - [https://lnkd.in/edZJPAGj](https://lnkd.in/edZJPAGj)   
🔹OverTheWire - [http://overthewire.org](http://overthewire.org/)  
🔹Practical Pentest Labs - [https://lnkd.in/e9ipA3s8](https://lnkd.in/e9ipA3s8)   
🔹Pentestlab - [https://pentesterlab.com](https://pentesterlab.com/)  
🔹Pentest Practice Labs - [https://lnkd.in/eKEPUzpa](https://lnkd.in/eKEPUzpa)   
🔹PentestIT LAB - [https://lab.pentestit.ru](https://lab.pentestit.ru/)  
🔹PicoCTF - [https://picoctf.com](https://picoctf.com/)  
🔹Portswigger -[https://lnkd.in/eAtZzKZt](https://lnkd.in/eAtZzKZt)  
🔹PWNABLE - [https://lnkd.in/eMEwBJzn](https://lnkd.in/eMEwBJzn)  
🔹Root-Me - [https://root-me.org](https://root-me.org/)  
🔹Root in Jail - [http://rootinjail.com](http://rootinjail.com/)  
🔹SANS Challenger - [https://lnkd.in/e8Wi2kF9](https://lnkd.in/e8Wi2kF9)   
🔹SmashTheStack - [https://lnkd.in/edHfvykf](https://lnkd.in/edHfvykf)   
🔹The Cryptopals Crypto Challenges - [https://cryptopals.com](https://cryptopals.com/)  
🔹TryHackMe - [https://tryhackme.com](https://tryhackme.com/)  
🔹Try2hack - [https://try2hack.me/](https://try2hack.me/)  
🔹Virtual Hacking Labs - [https://lnkd.in/eDhywSzk](https://lnkd.in/eDhywSzk)  
🔹Vulnhub - [https://vulnhub.com](https://vulnhub.com/)  
🔹Vulnmachine -[https://vulnmachines.com/](https://vulnmachines.com/)  
🔹W3Challs - [https://w3challs.com](https://w3challs.com/)  
🔹WeChall - [http://wechall.net](http://wechall.net/)  
🔹Zenk-Security - [https://lnkd.in/eYNyB9dw](https://lnkd.in/eYNyB9dw)  
🔹247CTF -[https://247ctf.com/](https://247ctf.com/)  
  
[#Pentest](https://www.linkedin.com/feed/hashtag/?keywords=pentest&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473), [#CTF](https://www.linkedin.com/feed/hashtag/?keywords=ctf&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473), [#Defense](https://www.linkedin.com/feed/hashtag/?keywords=defense&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#hacking](https://www.linkedin.com/feed/hashtag/?keywords=hacking&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#network](https://www.linkedin.com/feed/hashtag/?keywords=network&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#software](https://www.linkedin.com/feed/hashtag/?keywords=software&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#mobile](https://www.linkedin.com/feed/hashtag/?keywords=mobile&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#web](https://www.linkedin.com/feed/hashtag/?keywords=web&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#system](https://www.linkedin.com/feed/hashtag/?keywords=system&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#forensics](https://www.linkedin.com/feed/hashtag/?keywords=forensics&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#opensource](https://www.linkedin.com/feed/hashtag/?keywords=opensource&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#linux](https://www.linkedin.com/feed/hashtag/?keywords=linux&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#windows](https://www.linkedin.com/feed/hashtag/?keywords=windows&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#infosec](https://www.linkedin.com/feed/hashtag/?keywords=infosec&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#cybersecurity](https://www.linkedin.com/feed/hashtag/?keywords=cybersecurity&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#pentesting](https://www.linkedin.com/feed/hashtag/?keywords=pentesting&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#security](https://www.linkedin.com/feed/hashtag/?keywords=security&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#education](https://www.linkedin.com/feed/hashtag/?keywords=education&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#redteam](https://www.linkedin.com/feed/hashtag/?keywords=redteam&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473) [#blueteam](https://www.linkedin.com/feed/hashtag/?keywords=blueteam&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A7037049780799209473)