**Objectif : Déployer une application simple sur un cluster Kubernetes.**

Prérequis :
Vous avez installé et configuré kubectl, l'outil de ligne de commande de Kubernetes.
Vous avez accès à un cluster Kubernetes (par exemple en utilisant Minikube ou en vous connectant à un cluster en cloud).

Instructions :
Créez un fichier de déploiement Kubernetes appelé `app.yaml` avec le contenu suivant :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp
  namespace: webapp
spec:
  replicas: 3
  selector:
    matchLabels:
      app: webapp
  template:
    metadata:
      labels:
        app: webapp
    spec:
      containers:
        - name: webapp
          image: nginx:latest
          ports:
            - containerPort: 80
```


Ce fichier définit un déploiement de type Deployment, qui va créer une réplique de l'application webapp en utilisant l'image `nginx:latest`.

2. Déployez l'application en utilisant la commande `kubectl apply -f app.yaml`.

Vérifiez que le déploiement a été créé en utilisant la commande kubectl get deployment. Vous devriez voir une entrée pour le déploiement webapp.
Exposez l'application en utilisant un service de type LoadBalancer en créant un fichier de service appelé service.yaml avec le contenu suivant :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: webapp-service
  namespace: webapp
spec:
  selector:
    app: webapp
  type: NodePort
  ports:
    - port: 80
      protocol: TCP
      targetPort: 80
      nodePort: 31000
```


Ce fichier définit un service de type LoadBalancer, qui va créer une répartition de charge et exposer l'application sur un port externe.

5. Exposez l'application en utilisant la commande `kubectl apply -f service.yaml`.

Vérifiez que le service a été créé en utilisant la commande `kubectl get service`. Vous devriez voir une entrée pour le service `webapp` avec un port externe assigné.
Accédez à l'application en utilisant l'adresse IP externe du service et le port externe assigné (par exemple, `http://<EXTERNAL-IP>:<EXTERNAL-PORT>`). Vous devriez voir la page d'accueil de nginx s'afficher.