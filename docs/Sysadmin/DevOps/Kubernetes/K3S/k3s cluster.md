k3s - Raspberry pi Cluster
---

### Getting Started 

k3 inly work with iptables-legacy : https://github.com/alexellis/k3sup/issues/89

```
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy > /dev/null
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy > /dev/null
```

Installation - master 
```
curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" sh -s
```

```
curl -sfL https://get.k3s.io | INSTALL_K3S_CHANNEL=v1.21 | K3S_KUBECONFIG_MODE="644" sh -s -
```
La variable d'environnement est la version de k3s. Nécessaire pour installer Rancher. 

```
cat /var/lib/rancher/k3s/server/node-token
K108cdc8ec1d033c1216d9fa70fe8be244a5b798094e925fa8d43dbd94a489af878::server:a315a1b293d8f375fe3477e67a507687
```

Ajout des nodes : 

```
curl -sfL https://get.k3s.io | K3S_TOKEN="K108cdc8ec1d033c1216d9fa70fe8be244a5b798094e925fa8d43dbd94a489af878::server:a315a1b293d8f375fe3477e67a507687" K3S_URL="https://192.168.1.70:6443" K3S_NODE_NAME="rpinode1" sh -
```

```
curl -sfL https://get.k3s.io | INSTALL_K3S_CHANNEL=v1.21 K3S_TOKEN="K108cdc8ec1d033c1216d9fa70fe8be244a5b798094e925fa8d43dbd94a489af878::server:a315a1b293d8f375fe3477e67a507687" K3S_URL="https://192.168.1.70:6443" K3S_NODE_NAME="rpinode1" sh -
```

#### Uninstall 

### Uninstalling Servers[​](https://docs.k3s.io/installation/uninstall#uninstalling-servers "Direct link to Uninstalling Servers")

To uninstall K3s from a server node, run:

```
/usr/local/bin/k3s-uninstall.sh
```

### Uninstalling Agents[​](https://docs.k3s.io/installation/uninstall#uninstalling-agents "Direct link to Uninstalling Agents")

To uninstall K3s from an agent node, run:

```
/usr/local/bin/k3s-agent-uninstall.sh
```


### Rancher

```less
INFO[0000] Server URL: https://192.168.1.74:8443
INFO[0000] Default admin reset. New username: admin, new Password: 6h8v6jp5xdd8rd52468q4rqvb8z6wlzv5ssvmsqzjchfb4kltsxdrx

```

```bash
curl --insecure -sfL https://192.168.1.74:8443/v3/import/7fzscdrxx82khj7pr7zwwhbb2jf99wpps8pj4w26tdx65tsrv5wkwz_c-g88ch.yaml | kubectl apply -f -
```

Ajouter dans l'API du cluster Rancher : 
https://hub.docker.com/r/rancher/rancher-agent/tags

```
rancher/rancher-agent:v2.5.17-linux-arm64
```
![Pasted image 20230402135417.png](img/Pasted%20image%2020230402135417.png)

Utiliser la même version d'agent que la commande "rancherd".

docker pull rancher/rancher-agent:v2.5.17-linux-arm64
![Pasted image 20230402144811.png](img/Pasted%20image%2020230402144811.png)

Version k3s compatible rancher et raspberry :
  
- K3S
- v1.21.14+k3s1
- Trouble : https://www.reddit.com/r/rancher/comments/s8wcny/why_will_rancher_not_pickup_my_cluster/ 

### Beginning

test avec harry.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: default
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 80
          name: nginx
          protocol: TCP
        readinessProbe:
          httpGet:
            port: nginx
            path: /
```

```undefined
kubectl apply -f harry.yaml
```


Une fois le deployment up.
Créer un NodePort ensuite pour l'exposer.
![Pasted image 20230408132230.png](img/Pasted%20image%2020230408132230.png)

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-nodeport
  namespace: default
spec:
  selector:
    app: nginx
  type: NodePort
  sessionAffinity: None
  sessionAffinityConfig:
    clientIP:
      timeoutSeconds: 10800
  ports:
  - name: nginx
    protocol: TCP
    port: 80
    targetPort: nginx
    # If you set the `spec.type` field to `NodePort` and you want a specific port number,
    # you can specify a value in the `spec.ports[*].nodePort` field.
    nodePort: 30111

```

##### LoadBalancing

On a déjà du loadbalancing ave le nodeport. Il loadbalancing automatiquement.
