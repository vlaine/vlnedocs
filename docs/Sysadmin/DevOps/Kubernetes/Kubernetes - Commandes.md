# Commandes Kubernetes Engine

---

:::note Question  kubectl
> kubectl est l'outil de ligne de commande utilisé pour gérer les clusters Kubernetes.
:::


### Gestion des ressources du cluster
- `kubectl get` : affiche une liste de ressources du cluster, telles que les pods, les services, les noeuds, etc.
- `kubectl describe` : affiche des informations détaillées sur une ressource spécifique du cluster, comme les labels, les annnotations, les events, etc.
	- kubectl describe node 192.168.1.157
- `kubectl create` : crée une nouvelle ressource dans le cluster, comme un pod, un service, etc.
- `kubectl delete` : supprime une ressource du cluster.

### Gestion des pods et des containers
- `kubectl exec` : exécute une commande dans un container de pod.
- `kubectl logs` : affiche les logs d'un container de pod.
- `kubectl attach` : se connecte à un container de pod en mode interactif.
- `kubectl port-forward` : redirige les ports d'un container de pod vers un port local.
	- kubectl port-forward --address 0.0.0.0 w 8080:80
- `kubectl label nodes node1 disktype=ssd` : Ajoute un label au nodes (ici node1) pour le scheduler en fonction d'un label par exemple

---

## Afficher la liste des pods

```shell
- kubectl get pods [En option <POD NAME>]
     -o : format de sortie
        - wide : afficher l'ip du pod et le nœud qui l'héberge
        - yaml : afficher encore plus d'informations sur un pod sous      
	        - format YAML
        - json : afficher encore plus d'informations sur un pod sous      
	        - format JSON
     --template : récupérer des informations précises de la sortie de la commande
```


- Récupérer l'IP d'un pod : 
```sql
kubectl get pod  <POD NAME>      
  --template={{.status.podIP}}
```
  
### Créer un Pod

`- kubectl create -f <filename.yaml>`
  
### Supprimer un pod

`- kubectl delete pods <POD NAME>`

---

### Appliquer des nouveaux changements à votre Pod sans le détruire

- `kubectl apply -f <filename.yaml>`

  
### Afficher les détails d'un pod

- `kubectl describe pods <POD NAME>`

### Exécuter une commande d'un conteneur de votre pod

```xml
- kubectl exec <POD NAME> -c <CONTAINER NAME> <COMMAND>

       -t : Allouer un pseudo TTY
       -i : Garder un STDIN ouvert
```

### Afficher les logs d'un conteneur dans un pod

```xml
kubectl logs <POD NAME> -c <CONTAINER NAME>
      -f : suivre en permanence les logs du conteneur
      --tail : nombre de lignes les plus récentes à afficher
      --since=1h : afficher tous les logs du conteneur au cours de la dernière heure
     --timestamps : afficher la date et l'heure de réception des logs d'un conteneur
```

--- 

### Gestion de la configuration du cluster

- `kubectl config` : gère la configuration du client kubectl, comme l'ajout ou la suppression de clusters, de contexts, etc.

Autres options courantes
- `--namespace` : spécifie le namespace dans lequel exécuter la commande.
- `--output` : spécifie le format de sortie des données de la commande (par exemple, --output=yaml pour afficher les données au format YAML).
- `--dry-run` : exécute la commande en mode simulation sans apporter de modifications réelles dans le cluster.
- `--pod-manifest-path` : spécifie le chemin vers le fichier de manifeste de pod qui décrit les pods à exécuter sur le noeud.
- `--allow-privileged` : autorise l'exécution de pods avec des privilèges élevés
- `--network-plugin` : spécifie le plugin de réseau à utiliser pour le noeud.
- `--cgroup-driver` : spécifie le pilote de cgroup à utiliser pour le noeud.

---

### Générer un fichier YAML depuis une commande

```shell
kubectl run db \
> --image mongo:4.0 \
> --dry-run=client \
> -o yaml
```
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: db
  name: db
spec:
  containers:
  - image: mongo:4.0
    name: db
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

---

:::note  Test
> 
> - `kubectl expose pod whoami --type NodePort --port=8080 --target-port=80 --dry-run=client -o yaml`
> 	- "selector: run: whoami" dans votre yaml
> 
:::

:::note  Test
> 
> - `kubectl create service nodeport whoami --tcp 8080:80 --dry-run=client -o yaml`
> 	- "selector: run: whoami" dans votre yaml
> 
:::

:::note  Test
> 
> - `kubectl run www --image=nginx --port 8080 80 --expose --dry-run=client -o yaml`
> 	- Créer un pod et l'expose vers l'exterieur
> 
:::

