Tips Vagrant
---

### Installation sur MAC

- `brew cask install virtualbox` 
- `brew cask install vagrant` 
- `brew cask install vagrant-manager (optionnel)`

## Troubleshooting

:::danger 
> 
> master 22 -> 2222
> Vagrant failed to apply generated while attempting to apply the port forwarded rule :
> Failed to detect VMX PATH
Résolution:
```bash
vagrant cap provider scrub_forwarded_ports
```
Sinon c'est pas grave, tentez le vagrant ssh pour voir si vous pouvez aller sur la VM. Ce serait plus embêtant si le provider était VirtualBox.
:::

## Vagrant with WSL (ça marche pas, à creuser) : 

:::danger 
> Vagrant encountered an unexpected communications error with the
> Vagrant VMware Utility driver. Please try to run the command
> again. If this error persists, please contact support@hashicorp.co
Résolution : https://developer.hashicorp.com/vagrant/docs/providers/vmware/vagrant-vmware-utility
:::

https://www.vagrantup.com/docs/other/wsl.html
https://github.com/hashicorp/vagrant/issues/9628
https://thedatabaseme.de/2022/02/20/vagrant-up-running-vagrant-under-wsl2/

Sur la VM WSL : 
```shell
export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"
echo "export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS=\"1\"" >> .profile
 export VAGRANT_WSL_WINDOWS_ACCESS_USER_HOME_PATH=/mnt/c/Users/Oyaji
```

```shell
export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"
echo "export PATH=\"$PATH:/mnt/c/Program\ Files\ \(x86\)/VMware/VMware\ Workstation/vmrun.exe\""
echo "export PATH=\"$PATH:/mnt/c/Program Files (x86)/VMware/VMware Workstation/vmrun.exe\"" >> .profile
```

## OVF Tools install
https://docs.vmware.com/en/VMware-Telco-Cloud-Operations/1.4.0/deployment-guide-140/GUID-95301A42-F6F6-4BA9-B3A0-A86A268754B6.html

Il faut qu'une fenetre puisse s'afficher, donc si la commande sudo ./VMware[...].bundle ne fonctionne pas, il faut activer le X11 forwarding (en ssh) : https://www.cyberciti.biz/faq/x11-connection-rejected-because-of-wrong-authentication/

```shell
apt install xauth
sudo mkdir /root/.Xauthority
mkdir /home/vagrant/.Xauthority

export DISPLAY=localhost:0.0

sudo ./VMware-ovftool-4.3.0-7948156-lin.x86_64.bundle
Extracting VMware Installer...done.
```

Si rien ne fonctionne, installer gnome puis le faire depuis l'interface graphique

```
apt install gnome
```




![Pasted image 20230301111203.png](../img/Pasted%20image%2020230301111203.png)

```
ovftool --version                                 
VMware ovftool 4.3.0 (build-7948156)
```

## Ansible with vagrant WSL
https://gist.github.com/tknerr/291b765df23845e56a29

## Vagrant install

```
apt-get build-dep vagrant ruby-libvirt
apt-get install qemu libvirt-daemon-system libvirt-clients ebtables dnsmasq-base
apt-get install libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev
apt-get install libguestfs-tools
sudo apt install ruby-libvirt qemu libvirt-daemon-system libvirt-clients ebtables dnsmasq-base libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev libguestfs-tools
```


```
# Delete all plugins and reinstall
$ vagrant plugin expunge --reinstall
vagrant plugin install my-pluginv

vagrant plugin install vagrant-libvirt vagrant-reload vagrant-vmware-esxi vagrant-winrm-syncedfolders

Installing a plugin from a local file source $ vagrant plugin install /path/to/my-plugin.gem

vagrant plugin list
```
