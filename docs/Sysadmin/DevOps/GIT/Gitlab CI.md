Gitlab CI
---

.gitlab-ci.yml.
```yaml
image: node:lts-alpine3.17

stages:
  - test
  - deploy
 
test:
  stage: test
  cache:
    - key:
        files:
          - yarn.lock
      paths:
        - node_modules/  
  script:
    - yarn add react-loadable@* typescript@>=2.7 @algolia/client-search@>=4.9.1
    - yarn add remarkable-admonitions
    - yarn install
    - yarn build
  except:
    - master

pages:
  stage: deploy
  cache:
    - key:
        files:
          - yarn.lock
      paths:
        - node_modules/    
  script:
    - yarn add react-loadable@* typescript@>=2.7 @algolia/client-search@>=4.9.1
    - yarn add remarkable-admonitions
    - yarn install
    - yarn build
    - mv ./build ./public

  artifacts:
    paths:
      - public

  only:
    - master
```



#### image

Une image de docker qui est adapté à **GitLab CI**.

#### cache

Pour éviter d’attendre entre chaque test, on peut mettre en cache le _**composer**_ et _**Phpunit**_ afin d’accélérer le processus.

#### services

L’ensemble des services qui vous permettra de simuler un environnement, une base de données, un elasticsearch, un rabbitmq etc. Par défaut, l’image docker n’a aucun service d’installer pour améliorer les performances.

#### stages

C’est les étapes qu’on veut effectuer dans notre CI, par exemple on veut faire un build, fixture, test. On peut créer autant d’étapes qu’on veut.

#### variables

Les variables de configuration qu’on veut mettre en place dans Gitlab CI.

#### build / test

**Stage :** L’étape que le script doit être utilisé.

**Script :** C’est les scripts qui vont permettre de mettre en place l’environnement de tests, lancer les tests unitaires. Pour mon environnement de tests, j’ai décidé d’installer le prochain avec les bundles dont j’ai besoin, la base de données et les  fixtures de notre dernier article.

**Dependencies :** Comme pour les [fixtures](https://blog.gary-houbre.fr/developpement/symfony-4-comment-mettre-en-place-des-fixtures), cette partie se lancera après l’étape qu’on a choisie, pour cet exemple c’est le build.