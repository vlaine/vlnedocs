- ==`git init` : initialise un nouveau dépôt Git dans le répertoire courant.==
- ==`git clone` : clone un dépôt distant dans un nouveau répertoire.==
- ==`git add` : ajoute des fichiers à l'index (étape intermédiaire avant de les valider).==
- ==`git commit` : valide les modifications ajoutées à l'index et les enregistre dans l'historique du dépôt.==
	- git commit -m "commentaire"
	- giti commit -amend : modifier un commentaire
- ==`git push` : envoie les commits vers un dépôt distant.==
- ==`git pull` : récupère les commits d'un dépôt distant et les fusionne dans le dépôt local.==

---

## La base Gitflow (avec gitlab)

```bash
mkdir toto
cd toto
git init
git init --bare --shared ./whynot.git # Pour init sur Git Server NAS Synology
touch toto
git add .
git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   toto
git commit -m "initialisation default avec toto"
```

Go gitlab
Créer le projet sur l'interface web

Puis en console

```shell
git config --global user.name "Vincent LAINE"
git config --global user.email "vincent.laine5@gmail.com"
git remote add origin git@gitlab-pipeline:vlaine/toto.git
git push -u origin main
```

Ensuite on va créer les autres branches
```shell
git branch dev
git checkout dev
git add toto (après modif du fichier)
git commit -m "Record 2eme run"
git push -u origin dev
Énumération des objets: 5, fait.
Décompte des objets: 100% (5/5), fait.
Écriture des objets: 100% (3/3), 285 octets | 285.00 Kio/s, fait.
Total 3 (delta 0), réutilisés 0 (delta 0), réutilisés du pack 0
remote:
remote: To create a merge request for dev, visit:
remote:   http://gitlab.example.com/vlaine/toto/-/merge_requests/new?merge_request%5Bsource_branch%5D=dev
remote:
To gitlab-pipeline:vlaine/toto.git
 * [new branch]      dev -> dev
La branche 'dev' est paramétrée pour suivre la branche distante 'dev' depuis 'origin'.
```
![Pasted image 20230304165131.png](img/Pasted%20image%2020230304165131.png)

Maitenant on va foncctionner en mode git flow

```shell
git branch recette
git checkout recette
git checkout dev (reviens sur dev)
git add toto
git commit -m "Record 3eme run"
git push -u origin dev
```

La modif est faite sur dev donc on repars en recette

```shell
git branch 
git checkout recette                                  
Basculement sur la branche 'recette'
cat toto                                          
Zziufgzuifgqezufybgveauifbg 2eme run
```

On passe ensuite de la branche dev a la branche release (si on suit le schéma plus haut)
```shell
/toto  recette  git merge dev                                      
Mise à jour 5157f8a..840cb4f
Fast-forward
 toto | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

cat toto
3eme run

git push -u origin recette                        
Total 0 (delta 0), réutilisés 0 (delta 0), réutilisés du pack 0
remote:
remote: To create a merge request for recette, visit:
remote:   http://gitlab.example.com/vlaine/toto/-/merge_requests/new?merge_request%5Bsource_branch%5D=recette
remote:
To gitlab-pipeline:vlaine/toto.git
 * [new branch]      recette -> recette
La branche 'recette' est paramétrée pour suivre la branche distante 'recette' depuis 'origin'.
```

On vois 2 modifs après le merge. On push puis on veux maintenant envoyer en prod. On reviens sur la branche main.

```shell
git checkout main
cat toto
	1ere run
git merge recette
Mise à jour b82f122..840cb4f
Fast-forward
 toto | 1 +
 1 file changed, 1 insertion(+)

--

git push
Total 0 (delta 0), réutilisés 0 (delta 0), réutilisés du pack 0
To gitlab-pipeline:vlaine/toto.git
   b82f122..840cb4f  main -> main
```

![Pasted image 20230304170040.png](img/Pasted%20image%2020230304170040.png)

On vois que le main est bien merge.

Ensuite très important, on va ajouter des tags pour le versionning : 
```shell
git tag -a v1.0 -m "premiere version : evolution pour le 3eme run"
git status
	Sur la branche main
	Votre branche est à jour avec 'origin/main'.
	rien à valider, la copie de travail est propre
git push --tags
Énumération des objets: 1, fait.
Décompte des objets: 100% (1/1), fait.
Écriture des objets: 100% (1/1), 192 octets | 192.00 Kio/s, fait.
Total 1 (delta 0), réutilisés 0 (delta 0), réutilisés du pack 0
To gitlab-pipeline:vlaine/toto.git
 * [new tag]         v1.0 -> v1.0
```

Vérification sur gitlab : 

![Pasted image 20230304170317.png](img/Pasted%20image%2020230304170317.png)

Voir les commits : 
![Pasted image 20230304170348.png](img/Pasted%20image%2020230304170348.png)
En ligne de commande : 

```bash
git log --oneline
840cb4f (HEAD -> main, tag: v1.0, origin/recette, origin/main, origin/dev, recette, dev) Record 3eme run
5157f8a Record 2eme run
b82f122 Initial commit
```

```shell
git show 840cb4f
commit 840cb4fc69ab92ced455714020b4a64ef1f9099d (HEAD -> main, tag: v1.0, origin/recette, origin/main, origin/dev, recette, dev)
Author: Vincent LAINE <vincent.laine5@gmail.com>
Date:   Sat Mar 4 16:53:27 2023 +0100

    Record 3eme run

diff --git a/toto b/toto
index 9de06b5..c04569f 100644
--- a/toto
+++ b/toto
@@ -1 +1 @@
-Zziufgzuifgqezufybgveauifbg 2eme run
+3eme run
```


![Pasted image 20230304170650.png](img/Pasted%20image%2020230304170650.png)


----
## Commande de base après création d'un projet


Create a new repository
```bash
git clone git@gitlab.example.com:vlaine/toto.git
cd toto
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

Push an existing folder
```sql
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.example.com:vlaine/toto.git
git add .
git commit -m "Initial commit"
git push -u origin main

```
Push an existing Git repository
```sql
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.example.com:vlaine/toto.git
git push -u origin --all
git push -u origin --tags
```

##  Useful

```shell
git remote remove origin : Supprimer le remote add 
```

- `git fetch` : récupère les commits d'un dépôt distant sans les fusionner.
- `git merge` : fusionne les commits d'une autre branche dans la branche courante.
- `git branch` : gère les branches dans le dépôt.
- `git checkout` : permet de naviguer entre les branches et de restaurer des fichiers.
- `git stash` : enregistre temporairement des modifications non validées.
- `git log` : affiche l'historique des commits d'un dépôt.
- `git show` : affiche les détails d'un commit spécifique.
- `git diff` : affiche les différences entre les commits, les fichiers ou les branches.
- Il existe également de nombreuses options et arguments qui peuvent être utilisés avec ces commandes pour personnaliser leur comportement. 
- Si vous avez besoin d'informations plus détaillées sur l'utilisation de ces commandes, je vous recommande de consulter la documentation officielle de Git.

# Gitlab

Très gros logiciel avec beaucoup d'outils
Attention à la dépendance à l'outil
gitlab.com ou on-premise

```shell
vagrant@gitlab-pipeline:~$ gitlab-ctl status
run: alertmanager: (pid 1181) 3872s; run: log: (pid 1167) 3872s
run: gitaly: (pid 1180) 3872s; run: log: (pid 1157) 3872s
run: gitlab-exporter: (pid 1182) 3872s; run: log: (pid 1166) 3872s
run: gitlab-kas: (pid 1178) 3872s; run: log: (pid 1165) 3872s
run: gitlab-workhorse: (pid 1172) 3872s; run: log: (pid 1159) 3872s
run: logrotate: (pid 3731) 272s; run: log: (pid 1168) 3872s
run: nginx: (pid 1174) 3872s; run: log: (pid 1163) 3872s
run: node-exporter: (pid 1177) 3872s; run: log: (pid 1162) 3872s
run: postgres-exporter: (pid 1185) 3872s; run: log: (pid 1171) 3872s
run: postgresql: (pid 1179) 3872s; run: log: (pid 1164) 3872s
run: prometheus: (pid 1173) 3872s; run: log: (pid 1161) 3872s
run: puma: (pid 1186) 3872s; run: log: (pid 1176) 3872s
run: redis: (pid 1184) 3872s; run: log: (pid 1169) 3872s
run: redis-exporter: (pid 1170) 3872s; run: log: (pid 1158) 3872s
run: sidekiq: (pid 1175) 3872s; run: log: (pid 1160) 3872s
```

```shell
gitlab-ctl status
gitlab-ctl start
gitlab-ctl stop
gitlab-ctl restart
gitlab-ctl restart nginx
gitlab-psql -d gitlabhq_production
	Se connecter à la bdd de gitlab
gitlab-rails console
```

Config de gitlab : `/etc/gitlab/gitlab.rb` (ruby)

![Pasted image 20230304171942.png](img/Pasted%20image%2020230304171942.png)

Activer la registry docker 
On l'utilisera en tant que dépot pour le git flow pour la partie [[Pipeline complète CI CD](../CI%20CD/Pipeline%20compl%C3%A8te%20CI%20CD.md)