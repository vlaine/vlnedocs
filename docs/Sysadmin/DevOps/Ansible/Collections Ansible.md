Collections Ansible
---
Demo : https://www.youtube.com/watch?v=WOcqhk7TdYc
Structure :

![Pasted image 20230224131230.png](img/Pasted%20image%2020230224131230.png)

Les collections Ansible sont un excellent moyen d'obtenir des contributions de contenu de divers développeurs Ansible. Auparavant, il y avait un slogan pour Ansible : "batteries included", mais maintenant la batterie est un peu petite. L'installation par défaut d'Ansible inclura toujours les bibliothèques et modules nécessaires pour votre automatisation de démarrage, mais pas l'ensemble des modules et bibliothèques Ansible.

C'est une bonne chose car les développeurs Ansible n'ont pas besoin d'attendre un cycle de publication spécifique d'Ansible pour inclure la dernière version d'un module ou d'une bibliothèque. Au lieu de cela, ils peuvent contribuer du contenu et rendre disponible les dernières versions via des collections Ansible séparées.

Quelques répertoires importants dans les collections Ansible.

Le répertoire "docs" contient une documentation locale sur la collection. Il y aura également un fichier "galaxy.yml", qui est la source de données.

Ensuite, le répertoire "playbook" contiendra le playbook qui fait partie de la collection. Ensuite, il y aura un répertoire "task" où les fichiers de tâches seront inclus. Il y aura également des fichiers de tâches inclus, importés et des plugins et modules où les modules résideront.

Il y aura un répertoire de recherche et des filtres pour les plugins de filtrage Jinja2, une connexion pour les plugins de connexion et des rôles pour les tâches qui contiendront les outils Ansible pour cette collection. Il y aura également des tests et quelques autres répertoires.

:::tip 
>
>En gros, les collections Ansible ressemblent à des rôles Ansible, mais beaucoup plus que cela. Dans un rôle Ansible, vous avez des éléments tels que des variables, des gestionnaires, des tâches, des modèles de fichiers, etc.
> 
>Mais dans une collection Ansible, vous avez plus d'éléments tels que des modules, des plugins, des filtres et des rôles Ansible. Il n'est pas garanti que chaque collection aura tous les éléments que vous avez expliqués précédemment, cela dépend de l'objectif de la collection et du type de contenu.
:::


Disons que vous avez besoin de mettre en place une automatisation pour les clusters Kubernetes dans votre organisation et que vous connaissez plusieurs modules Ansible que vous pouvez utiliser dans vos playbooks.

Vous pouvez simplement développer un playbook basé sur des modules Kubernetes et essayer de l'exécuter. Mais ça va marcher ? Bien sûr que non.

Après vérification, vous découvrez que les modules Kubernetes ne font pas partie de l'installation par défaut d'Ansible. Au lieu de cela, les modules et les plugins de K8S font partie de la collection Ansible appelée "community.kubernetes".

Alors comment installer une collection Ansible ? Vous pouvez visiter Ansible Galaxy et chercher Kubernetes. Vous pouvez voir qu'il y a plusieurs résultats, mais vous savez déjà que vous voulez installer la collection Kubernetes de la communauté.

Vous pouvez faire défiler la page jusqu'à trouver la communauté et la collection Kubernetes en dessous. Ensuite, vous pouvez cliquer sur la collection Kubernetes de la communauté et voir les détails tels que la version et comment installer cette collection.

Par défaut, la collection Ansible sera installée sous le répertoire home (~/.ansible/collections). 
C'est bien si vous êtes le seul développeur gérant et exécutant le projet et uniquement depuis votre machine. Mais si vous voulez que d'autres développeurs utilisent votre playbook Ansible, ils doivent également installer la collection sur leur machine. Si vous voulez déployer la collection en tant que partie de votre projet, installez cette collection Ansible dans le répertoire de votre projet lui-même, sous un répertoire spécifique.

```shell
ansible-galaxy collection install -p ./collections community.kubernetes
```

Puis installer la collection Ansible en mentionnant le chemin d'installation :
```shell
ansible-galaxy collection install -p ./collections community.kubernetes
```

Nous sommes prêts, mais vous devez remarquer deux éléments importants dans la sortie de la commande. Tout d'abord, l'élément installé (`/home/user/.ansible/collections`) ne fait pas partie de la configuration d'Ansible, même si vous avez installé la collection dans le répertoire de votre projet et Ansible ne la détectera pas. Vous devez donc configurer votre chemin de collection (`collections_path`) dans votre `ansible.cfg` pour ce projet spécifique. Nous allons le configurer comme suit :

```shell
collections_path = ./collections
```

Ansible recherchera les collections dans les répertoires dans l'ordre suivant : `~/.ansible/collections`, `/usr/share/ansible/collections` (la valeur par défaut de `collections_path`), puis tout autre répertoire spécifié dans `collections_path`.

Le deuxième élément important est qu'Ansible créera automatiquement un sous-répertoire appelé "ansible_collections" dans le répertoire que vous avez spécifié, et Ansible créera également des sous-répertoires supplémentaires en fonction du nom de l'organisation de la collection (`community`, `fortinet`, `ansible`, `vmware`, etc.) lorsque vous installez des collections à partir de ces organisations.

Nous avons installé une collection Ansible appelée "community.kubernetes". Maintenant, vérifions-le avec la commande suivante :

```shell
ansible-doc -t module -l | grep k8s
```

Cela devrait afficher la liste de tous les modules Ansible disponibles pour la collection Kubernetes. Maintenant, nous pouvons utiliser ces modules dans notre playbook Ansible.

--- 

Si vous souhaitez installer une collection Ansible avec une version ou une spécification spécifique, vous pouvez simplement mentionner la version lors de l'installation de la collection Ansible. Cela est utile dans les cas où vous devez utiliser une version spécifique d'une collection pour des raisons de compatibilité.

Pour installer une collection Ansible avec une version spécifique, vous pouvez utiliser la commande suivante

```shell
ansible-galaxy collection install <nom-collection>:<version>
```

Remplacez `<nom-collection>` par le nom de la collection que vous souhaitez installer, et `<version>` par la version spécifique que vous souhaitez installer.

Par exemple, pour installer la collection Kubernetes de la communauté en version 1.0.0, vous pouvez utiliser la commande suivante :

```shell
ansible-galaxy collection install community.kubernetes:1.0.0
```

Il est également possible d'installer une collection Ansible à partir d'un référentiel Git privé. Dans ce cas, vous devez spécifier le chemin du référentiel Git lors de l'installation de la collection Ansible. Voici un exemple de commande pour installer une collection Ansible à partir d'un référentiel Git privé :

```shell
ansible-galaxy collection install git+ssh://<chemin-du-repo-git>#<nom-de-la-branche>
```

Remplacez `<chemin-du-repo-git>` par le chemin d'accès au référentiel Git que vous souhaitez installer, et `<nom-de-la-branche>` par le nom de la branche que vous souhaitez installer.

Par exemple, pour installer la collection "ma-collection" à partir du référentiel Git privé "[git@github.com](mailto:git@github.com):mon-repo-git/ma-collection.git" en utilisant la branche "master", vous pouvez utiliser la commande suivante :

```shell
ansible-galaxy collection install git+ssh://git@github.com:mon-repo-git/ma-collection.git#master
```

