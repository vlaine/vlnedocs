Exercices Ansible Cours DevOPS EFREI 
---

## Exo 1

Créez un inventaire d'hôtes et configurez une connexion à un hôte en utilisant un nom d'utilisateur et un mot de passe. Vérifiez que vous pouvez vous connecter à l'hôte en exécutant une commande de base, comme "uname -a".

`inventaire.yml`
```yaml
all:
  hosts:
    192.168.10.129
  children:
    cours:
      hosts:
        192.168.10.131
```

`playbook.yml`
```yaml
- name: Exo 1
  hosts: all
  tasks:
    - name: check lnx dist
      command: uname -a
      register: dist_name
    - name:
      debug:
        var: dist_name
```

## Exo 2

- Écrivez un playbook qui installe Apache sur un hôte cible et vérifiez que le service Apache est en cours d'exécution sur l'hôte cible.
- Y inclure une instruction qui copie un fichier depuis votre ordinateur local vers un hôte cible, puis modifie les permissions du fichier sur l'hôte cible.
- Écrivez un playbook qui crée un utilisateur et un groupe sur un hôte cible, puis ajoute l'utilisateur au groupe.
- Écrivez un playbook qui installe un package sur plusieurs hôtes en utilisant une boucle "for".

```
├── Exo_2
│   ├── desktop.ini
│   ├── files
│   │   ├── desktop.ini
│   │   └── index.html
│   ├── inventaire.yml
│   ├── playbook_apache.yml
│   ├── playbook_boucle.yml
│   └── playbook_user.yml
```

`index.html`
```
<h1> Test </h1>
```

`inventaire.yml`
```yaml
all:
  hosts:
    192.168.10.129
```

`playbook_apache.yml`
```yaml
- name: Exo 2
  hosts: all
  become: yes
  tasks:
    - name: Install Apache
      apt:
        name: nginx
        state: present
    - name : Ensure apache is started
      service:
        name: nginx
        state: started
    - name: Copy file
      ansible.builtin.copy:
        src: files/index.html
        dest: /etc/index.html
        owner: global
        group: global
        mode: "0640"
```

`playbook_boucle.yml`
```yaml
- name: Exo 2
  hosts: all
  become: true
  tasks:
    - name: Install package
      apt:
        name: "{{ item }}"
        state: present
      loop:
        - nginx
        - mysql-server
        - openssl
        - openssh-server
```

`playbook_user.yml`
```yaml
- name: Exo 2
  hosts: all
  become: true
  tasks:
    - name: create group
      group:
        name: group1
        state: present
    - name : Add the user
      ansible.builtin.user:
        name: johndoe
        groups: group1
        state: absent
        append: true
```

## Exo 3

- Écrivez un playbook qui exécute une commande sur un hôte cible et enregistre la sortie de la commande dans un fichier sur votre ordinateur local.
- Écrivez un playbook qui déploie une application sur un hôte cible en utilisant des tâches conditionnelles pour vérifier si les prérequis de l'application sont déjà installés sur l'hôte cible.
- Écrivez un playbook qui exécute un script shell sur un hôte cible et envoie un rapport d'exécution par courriel à l'administrateur système.

```
├── Exo_3
│   ├── desktop.ini
│   ├── files
│   │   ├── desktop.ini
│   │   └── index.html
│   ├── inventaire.yml
│   ├── playbook_boucle.yml
│   ├── playbook_retunr_file.yml
│   └── playbook_user.yml
└── desktop.ini
```

`index.html`
```
<h1> Test </h1>
```

`inventaire.yml`
```yaml
all:
  hosts:
    192.168.10.129
```


`playbook_retunr_file.yml`
```yaml
- name: Exo 3
  hosts: localhost
  tasks:
    - name: check lnx dist
      command: uname -a
      register: dist_name
    - lineinfile:
        create: true
        dest: /tmp/test.txt
        line: "{{ dist_name }}"
```

`playbook_boucle.yml`
```yaml
- name: Exo 2
  hosts: all
  tasks:
    - name: Install Apache
      apt:
        name: "{{ item }}"
        state: present
      loop:
        - nginx
        - mysql
        - openssh
        - openssl
```

`playbook_user.yml`
```yaml
- name: Exo 2
  hosts: all
  tasks:
    - name: create group
      group:
        name: group1
        state: present
    - name : Add the user
      ansible.builtin.user:
        name: johndoe
        groups: group1
        append: true
```

## Exo 5

3 fichiers : 
- main.yml
- bashrc.j2
- interfaces.j2

Transformer ce playbook en rôle.
- Créer toute l'arborescence
 - Mettre les tasks au bon endroit
 - Utiliser des variables pour les install avec apt
 - utiliser des notify (handlers) pour le redémarrage du service
 - Utiliser un notify pour l'update du cache APT
 - Le rôle s'appellera common
Eg : 
```
```yaml
roles/
    common/               # this hierarchy represents a "role"
        tasks/            #
            main.yml      #  <-- tasks file can include smaller files if warranted
        handlers/         #
            main.yml      #  <-- handlers file
        templates/        #  <-- files for use with the template resource
            ntp.conf.j2   #  <------- templates end in .j2
        files/            #
            bar.txt       #  <-- files for use with the copy resource
            foo.sh        #  <-- script files for use with the script resource
        vars/             #
            main.yml      #  <-- variables associated with this role
        defaults/         #
            main.yml      #  <-- default lower priority variables for this role
        meta/             #
            main.yml      #  <-- role dependencies
        library/          # roles can also include custom modules
        module_utils/     # roles can also include custom module_utils
        lookup_plugins/   # or other types of plugins, like lookup in this case

    webtier/              # same kind of structure as "common" was above, done for the webtier role
    monitoring/           # ""
    fooapp/               # ""
```
```
