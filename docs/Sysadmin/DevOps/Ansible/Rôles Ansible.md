Rôles Ansible
---
Documentation : https://docs-ansible-com.translate.goog/ansible/latest/playbook_guide/playbooks_reuse_roles.html?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=fr 
Vidéo intéressante : https://www.youtube.com/watch?v=5hycyr-8EKs 

---

Un rôle Ansible a une structure de répertoires définie avec huit répertoires standard principaux. Vous devez inclure au moins un de ces répertoires dans chaque rôle. Vous pouvez omettre tous les répertoires que le rôle n'utilise pas. Par exemple:

```
# playbooks
site.yml
webservers.yml
fooservers.yml
```

```yaml
roles/
    common/               # this hierarchy represents a "role"
        tasks/            #
            main.yml      #  <-- tasks file can include smaller files if warranted
        handlers/         #
            main.yml      #  <-- handlers file
        templates/        #  <-- files for use with the template resource
            ntp.conf.j2   #  <------- templates end in .j2
        files/            #
            bar.txt       #  <-- files for use with the copy resource
            foo.sh        #  <-- script files for use with the script resource
        vars/             #
            main.yml      #  <-- variables associated with this role
        defaults/         #
            main.yml      #  <-- default lower priority variables for this role
        meta/             #
            main.yml      #  <-- role dependencies
        library/          # roles can also include custom modules
        module_utils/     # roles can also include custom module_utils
        lookup_plugins/   # or other types of plugins, like lookup in this case

    webtier/              # same kind of structure as "common" was above, done for the webtier role
    monitoring/           # ""
    fooapp/               # ""
```

---

Un rôle Ansible est une unité d'organisation pour les tâches, les variables et les fichiers nécessaires pour effectuer une tâche spécifique dans Ansible. Il est conçu pour permettre une réutilisation facile de code et une abstraction des détails d'implémentation. Les rôles permettent également de séparer les responsabilités, de hiérarchiser les variables et de simplifier les tâches récurrentes.

La structure d'un rôle Ansible est assez simple. Il contient un répertoire nommé "roles" et chaque rôle se trouve dans un sous-répertoire de celui-ci. Chaque rôle peut contenir plusieurs fichiers et répertoires, dont certains sont obligatoires.

Le fichier "tasks/main.yml" est le cœur du rôle et contient toutes les tâches à exécuter pour ce rôle. Il peut également inclure d'autres fichiers de tâches ou des fichiers de tâches inclus d'autres rôles.

Le répertoire "files" contient des fichiers que vous souhaitez copier sur la machine distante. Le répertoire "templates" contient des modèles de fichiers, qui sont des fichiers qui contiennent des variables Jinja2, qui seront remplacées par les valeurs correspondantes lors de l'exécution.

Le répertoire "vars" contient les variables spécifiques à ce rôle. Le répertoire "defaults" contient les variables par défaut qui peuvent être écrasées par les variables de l'inventaire, de la ligne de commande ou du playbook.

Le répertoire "meta" contient les informations sur le rôle, telles que la description et les dépendances. Le répertoire "handlers" contient les gestionnaires pour le rôle. Les gestionnaires sont des tâches qui sont déclenchées à la fin de l'exécution du rôle, si un changement a été effectué.

Voici un exemple de structure de rôle Ansible :

```
roles/
    common/
        tasks/
            main.yml
        files/
            file1.conf
        templates/
            template1.conf.j2
        vars/
            main.yml
        defaults/
            main.yml
        meta/
            main.yml
        handlers/
            main.yml
```

Dans cet exemple, le rôle s'appelle "common". Le fichier "tasks/main.yml" contient toutes les tâches à effectuer pour ce rôle. Le répertoire "files" contient un fichier nommé "file1.conf", qui sera copié sur la machine distante. Le répertoire "templates" contient un fichier de modèle nommé "template1.conf.j2", qui sera rempli avec des valeurs Jinja2 lors de l'exécution.

Le répertoire "vars" contient des variables spécifiques à ce rôle, qui peuvent être utilisées dans les fichiers de tâches et de modèles. Le répertoire "defaults" contient les variables par défaut qui peuvent être écrasées. Le répertoire "meta" contient des informations sur le rôle, telles que la description et les dépendances. Le répertoire "handlers" contient les gestionnaires pour le rôle.

---

En résumé, un rôle Ansible est un moyen d'organiser et de réutiliser le code et les fichiers pour effectuer des tâches spécifiques dans Ansible.

Il y a un répertoire de rôle qui contient des sous-répertoires avec des noms spécifiques qui définissent leur contenu et leur objectif. Voici les sous-répertoires typiques d'un rôle Ansible:

:::info Important 
> 
> -   Le répertoire "defaults" contient les valeurs par défaut pour les variables utilisées par le rôle.
> -   Le répertoire "files" contient les fichiers qui doivent être copiés sur les machines cibles.
> -   Le répertoire "handlers" contient les gestionnaires qui sont appelés par des tâches spécifiques ou manuellement.
> -   Le répertoire "meta" contient les informations sur le rôle, telles que les dépendances ou les tags.
> -   Le répertoire "tasks" contient les tâches à effectuer par le rôle.
> -   Le répertoire "templates" contient les modèles de fichiers à copier sur les machines cibles.
> -   Le répertoire "vars" contient les variables spécifiques au rôle.
:::


Le répertoire de rôle peut également contenir un fichier README, qui fournit une documentation sur le rôle et sa fonction, ainsi qu'un fichier "main.yml" dans le répertoire "tasks", qui contient les tâches principales à exécuter.

Les rôles Ansible peuvent être utilisés pour définir des tâches spécifiques pour des types de serveurs, des applications, des services ou des systèmes d'exploitation différents. Par exemple, un rôle "nginx" pourrait être utilisé pour installer et configurer le serveur web Nginx, tandis qu'un rôle "postgresql" pourrait être utilisé pour installer et configurer la base de données PostgreSQL.

Les rôles peuvent également être utilisés pour diviser un playbook Ansible en plusieurs fichiers plus petits et plus faciles à gérer. Par exemple, un playbook Ansible qui installe un serveur web pourrait utiliser des rôles séparés pour installer le serveur web, le serveur de base de données et le serveur d'application.

Exemple de rôle pour déployer Kubernetes avec kubeadm : 
https://github.com/vlaine5/k8scluster-eva

Ce bundle contiens 3 rôles :
- master
- worker
- common

Common sera les applis communes à installer sur les workers et master.