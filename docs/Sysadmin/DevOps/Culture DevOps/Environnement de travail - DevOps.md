Environnement de travail - DevOps
---

1) Ansible
2) Vagrant
3) Docker
4) Kubernetes

## Ansible

- 1 VM - Une machine master - (Ansible, Vagrant, Docker)
	- Sur laquelle sera installé
	- Soit une machine virtuelle - Soit WSL
- 1 VM - Une machine cliente 
	- Pour les déploiement

Obligation pour l'hyperviseur : 
- Un sous réseau qui a accès à internet mais qui n'est pas votre réseau local
	- NAT (workstation)
	- Réseau NAT (VirtualBox) - Redirection de port pour SSH 

![Pasted image 20230221160046.png](../img/Pasted%20image%2020230221160046.png)
IP Fixe sur les 2 machines
SSH installé et configuré 
#### Pour vérifier :
1) ping du master jusqu'au client
2) connexion ssh
	1) ssh root@(ipduclient)
3) Echange de clef puis re tester pour vérifier si on peut se connecter sans le mot de passe

ssh depuis le master vers le client (Sans mot de passe - Donc échange de clef)
	Création de la clef : ssh-keygen -a rsa
	ssh-copy-id root@(IP DU CLIENT)

```shell
ansible02:/mnt/iac/Exos/Exo_3# ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa):
```

#### SSH

```shell
      ~  ssh-keygen                                                                                                                             ✔  root@OYAJI-PC
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /root/.ssh/id_rsa
Your public key has been saved in /root/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:UrKGUMOd/i5KjUoSN1PYHQV+7e8vp1sU3ymoZ9VM9iY root@OYAJI-PC
The key's randomart image is:
+---[RSA 3072]----+
|   .o.o+.        |
|   +.+o. .       |
|  o o.+ o .   .o |
|   o ..= .  . =o+|
|. + . +.S .. oE+=|
| o o + .. .....o |
|. . o .. . o. .  |
| o o  . . o....  |
|  . .. .    +*.  |
+----[SHA256]-----+

      ~  ssh-copy-id root@192.168.1.40                                                                                                  ✔  3s    root@OYAJI-PC
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/root/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
root@192.168.1.40's password:
stty: 'entrée standard': Ioctl() inapproprié pour un périphérique

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'root@192.168.1.40'"
and check to make sure that only the key(s) you wanted were added.


      ~  ssh root@192.168.1.40                                                                                                                  ✔  root@OYAJI-PC
Linux bastion-ssh 5.10.0-19-amd64 #1 SMP Debian 5.10.149-2 (2022-10-21) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Tue Feb 21 14:46:05 2023 from 192.168.1.228
(mar. févr.-2 4:10:23)-(CPU 0,1%:0:Net 7)-(root@bastion-ssh:~)-(536K:36)
>
```


### MAC Virtualisation

Télécharger Fusion (ISO/LOGICIEL VIRTUALISATION)
télécharger Version ARM de Debian (pour M1)
- https://cdimage.debian.org/debian-cd/current/arm64/iso-cd/debian-11.6.0-arm64-netinst.iso
- https://www.debian.org/releases/bullseye/debian-installer/
- https://mac.getutm.app/ (si fusion marche pas)