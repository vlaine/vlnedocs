DevOps - Outils
---
## Agile 

Il existe plusieurs types de méthodes dites agiles ; mais "Agile" en soi n'est pas une méthode particulière, sauf si vous parlez de "Agile Unified Process", mais rares sont ceux qui connaissent. Scrum est une des méthodes agiles, la plus connue. 

D'autres méthodes agiles sont par exemple : extreme programming (xp), Lean. 

En l'occurence, l'enjeux de ces méthodes sont financiers : d'après les différentes éditions du «rapport chaos» proposé annuellement par «The standish group», il est plus rentable d'utiliser une méthode agile que (par exemple) du cycle en V. 

En général, un des thèmes centraux des méthodes agiles, c'est de faire itérativement et incrémentalement : Faire le plus petit produit avec une valeur ajoutée, le livrer, et recommencer. 
L'idée, c'est que si le client à besoin de se déplacer maintenant, mieux vaut lui fournir une trotinette demain et un vélo dans 1 mois (etc.), qu'une voiture dans deux ans et rien entre temps. 

Scrum c'est plein de jargon : 
"Product Owner", "Scrum Master", 
"Sprints" sont des mots qui appartiennent à la méthodologie Scrum ! Un des enjeux principaux de scrum (et un des plus grands freins à son adoption), c'est qu'il boulverse la hierarchie : Il n'y a pas de relation hierarchique entre le PO, le scrum master et l'équipe de dev ; c'est le product owner qui décide des features à effectuer et non pas la direction, le manager peut se concentrer sur le rôle de manager sans toucher de près ou de loin au produit. 

Par exemple, dans scrum, le product owner décide des features les plus rentables pour le produit, et se coordonne avec l'équipe de dev pour les y ajouter : Il est expert du métier, expert du produit, il est plus légitime que sa direction pour choisir ce qu'il faut mettre dans le backlog. 

Le product owner n'est pas un chef de projet, car il n'a personne sous lui : Il est responsable de s'assurer que le produit dont il est responsable est en adéquation avec les besoin des utilisateurs. 
L'équipe de dev n'est pas managée par le product owner, et elle a deux rôles : travailler avec le PO pour sortir des features, et s'assurer que le produit est de bonne qualité. 

Le rôle du scrum master là dedans, c'est de rappeler aux devs de s'assurer de la qualité du produit, réduire les sources de distractions qui peuvent survenir, et s'assurer de l'avancement du sprint en cours. 

En somme, son rôle est d'aider la cohésion. 

En scrum, il existe deux backlogs : 
sprint backlog: c'est l'ensemble des tâches que les devs se sont engagés à tenir sur un sprint. Il appartient aux devs. 
product backlog : c'est l'ensemble des besoins ordonnés. Il appartient au PO 

En scrum, il existe des "cérémonies", c'est à dire des réus avec des fonctions définies : 
- sprint planning: le PO et l'équipe de dev voient ensemble ce qui va se faire dans le sprint. L'équipe de dev prend un engagement.
- sprint review: c'est la présentation aux intéréssés (stakeholders, métier ...) du resultat du sprint 
- sprint retrospective: c'est le moment où toute l'équipe de dev + SM + PO se réunissent et réfléchissent ensemble à ce qui a été un frein lors du sprint, et comment éviter ces problèmes après. 
- daily scrum: (ou stand up meeting, daily stand up, ...) c'est une réunion dans laquelle on essaye d'identifier les problèmes des tickets en cours, et où on s'assure que l'US la plus importante du sprint (dite l'objectif du sprint) est bien en voie d'être résolue dans les temps.

![Pasted image 20230223082337.png](../img/Pasted%20image%2020230223082337.png)

---

![Pasted image 20230223082837.png](../img/Pasted%20image%2020230223082837.png)


---

Idem Potence
C'est la fait que quand on rejoue notre pipeline, l'orchestrateur sera capable de découvrir si l'état final est déjà présent ou pas, et si il est dékà présent ne pas agir. Eg: Création d'un user de bdd

Ordonanceur 
