Docker networking - 7 ways
---

1) `Bridge` - Sous reseau naté sur le réseau normal (hôte) - 172.17.0.0/16 par défault
2) `User defined bridge` : Réseaux bridges isolés les uns des autres 
```sql
docker network create mybridge network
```

3) `The host` - Même réseau que le host, il partage son IP et tout le reste (ports...)
4) `macvlan  (bridge mode)`  - Connecté directement à notre réseau physique, un peu comme une VM - Tu peux lui attribuer une adresse de ton réseau local
    Donner son Subnet local et Gateway (routeur), et son parent (l'interface réseau de l'hôte) pour la mapper dessus.
     
:::caution 
> 
> Cela donne une MAC address au container, et une interface ne peux pas avoir plusieurs mac sur une interface. Il faut faut accepter le mode promiscuité.
:::

```bash
ip link set eth0 promisc on
```

Activer le mode dans l'hyperviseur si disponible.

:::danger 
> - PAS de DHCP pour machines MACVLAN 
> - SPECIFIER UNE IP ADDRESS FIXE POUR LE CONTAINER, AFIN EVITER CONFLIT (VOIR DOC POUR LES OPTIONS AU DEPLOYMENT)
:::

```css
docker network create -d macvlan \
  --subnet=172.16.86.0/24 \
  --gateway=172.16.86.1 \
  -o parent=eth0 \
  pub_net
```

4) `macvlan  (802.1q mode)` - Connecté directement a notre réseau physique, un peu comme une VM - tu peux lui attribuer une adresse de ton réseau local
    Réseau sous forme de vlan - eth0.20 eth0.30 etc..
    Donner son Subnet local et Gateway (box) et son parent --> l'interface réseau de l'hôte avec un sous interface en eth0.20 par exemple (pour un VLAN 20) pour la mapper dessus
    Ce réseau crée a l'hôte des sous interfaces
  
```css
docker network create -d macvlan \
--subnet 192.168.20.0/24 
--gateway 192.168.20.1 \
-o parent=eth0.20 \
macvlan20
```

Cela rajoute la sous interface et peut être avec un réseau différent de l'hôte.

5) `IPvlan (L2)` (et (L3) existe aussi)
    Les container vont pouvoir avoir leur propre IP sur notre réseau local mais cette fois en partageant la MAC address de l'hôte
```css
docker network create -d ipvlan \
--subnet 192.168.1.0/24 
--gateway 192.168.1.1 \
-o parent=eth0 \
new_ipvlan_network
```

Puis run un container : 
```css
docker run -tid --rm --network new_ipvlan_network \
--ip 192.168.1.4/24 \
-name container01 busybox 
```


6)  `IPvlan (L3)`
Le container considère l'host (son ip) comme étant le routeur, donc l'ip du host est la gateway du réseau des containers.
On peux par exemple faire des routes statiques sur l'host, afin de faire communiquer nos sous réseau du L3.
```bash
docker network create -d ipvlan \
--subnet 192.168.94.0/24 #le réseau que nous créons
-o parent=eth0  -o ipvlan_mode=l3 \ #pas de gateway car le parent est la gateway
--subnet 192.168.95.0/24 \ #on fait a si on veux créer plusieur réseau sur la même interface
new_ipvlan_network
```

Puis pour l'utiliser :
```css
docker run -tid --rm --network new_ipvlan_network \
--ip 192.168.94.8/24 \
-name container01 busybox   
```

```css
docker run -tid --rm --network new_ipvlan_network \
--ip 192.168.95.9/24 \
-name container02 busybox
```

Vérifier les configs :
```css
docker inspect new_ipvlan_network
```

Les containers n'ont pas internet, pas de routes mais peut pinger les autres container et par le nom aussi.

Donc ajouter une vrai route sur le réseau si on veux faire communiquer vers à l'extérieur en gros ton host deviens un routeur entre tes containers. 
N:B : no broadcast traffic

7) `The NONE Network`
The null network: Il n'y a que la localhost

## Docker-compose network

![Pasted image 20230304190600.png](img/Pasted%20image%2020230304190600.png)

![Pasted image 20230304190729.png](img/Pasted%20image%2020230304190729.png)
