Notes Docker
---

```bash
/etc/docker/deamon.json
```
logs ip remap etc.. fichier de config de docker
![Pasted image 20230304183334.png](img/Pasted%20image%2020230304183334.png)

```undefined
docker info 
```
: Avoir les détails de son docker, images, la version, type de storage driver, filesystem etc.. infos sur l'host en gros

`docker-inspect element` : metadonnées importantes

docker inspect : tout en bas on a le réseau 

Supprimer tout les conteneurs: 
```bash
docker rm -rf $(docker ps -aq)
```

docker network ls
docker port conteneur_name

Voir [[Docker networking - 7 ways](Docker%20networking%20-%207%20ways.md)

![Pasted image 20230304185831.png](img/Pasted%20image%2020230304185831.png)

