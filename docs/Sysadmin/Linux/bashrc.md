.bashrc
---
```bash
export PS1= 
```
-   \a : un signal sonore (bell)
-   \d : la date actuelle sous la forme "day month year"
-   \e : le caractère d'échappement
-   \h : le nom de l'hôte (hostname)
-   \H : le nom de l'hôte jusqu'au premier point (short hostname)
-   \j : le nombre de tâches en cours d'exécution dans le terminal
-   \l : le nom du shell
-   \n : un retour à la ligne
-   \r : retour au début de la ligne
-   \s : le nom de l'interpréteur de commandes (shell)
-   \t : l'heure actuelle au format HH:MM:SS
-   \T : l'heure actuelle au format HH:MM:SS sans les secondes
-   @ : l'heure actuelle au format AM/PM
-   \u : le nom d'utilisateur (username)
-   \v : la version de l'interpréteur de commandes
-   \V : la version de l'interpréteur de commandes, y compris le numéro de révision
-   \w : le chemin absolu du répertoire de travail courant
-   \W : le nom du répertoire de travail courant (basename)
-   ! : le numéro de commande dans l'historique
-   # : le numéro de commande de cette session
-   $ : affiche le caractère "$" ou "#" selon que l'utilisateur est root ou non

En plus de ces codes d'échappement, vous pouvez également utiliser des codes de couleur pour personnaliser la couleur du texte et de l'arrière-plan. Voici quelques exemples :

-   \e[0�\e[0m : réinitialise les couleurs
-   \e[1�\e[1m : active le style gras
-   \e[4�\e[4m : active le style souligné
-   \e[31�\e[31m : définit la couleur rouge pour le texte
-   \e[41�\e[41m : définit la couleur rouge pour l'arrière-plan

En combinant ces codes d'échappement et ces codes de couleur, vous pouvez créer des invites de commandes personnalisées selon vos préférences.

En plus de ces caractères, vous pouvez utiliser les codes d'échappement de couleur pour personnaliser les couleurs de l'invite de commande. Voici quelques codes couramment utilisés :

```bash
-   \e[0m : réinitialise les couleurs.
-   \e[1m : active le mode gras.
-   \e[4m : active le soulignement.
-   \e[30m - \e[37m : définit la couleur du texte en noir, rouge, vert, jaune, bleu, magenta, cyan ou gris clair.
-   \e[90m - \e[97m : définit la couleur du texte en gris foncé, rouge clair, vert clair, jaune clair, bleu clair, magenta clair, cyan clair ou blanc.
```

Voici quelques exemples de commandes "export PS1" pour vous aider à créer votre propre invitation de commande personnalisée :

```
export PS1="\u@\h:\w\$ "
```

Cette commande affichera le nom d'utilisateur actuel, suivi de "@", suivi du nom de l'hôte jusqu'au premier ".", suivi du chemin absolu du répertoire de travail actuel et enfin, le caractère de l'invite de commande "$".

```
export PS1="\[\e[1;32m\]\u@\h\[\e[0m\]:\[\e[1;34m\]\w\[\e[0m\]\$ "
```
Cette commande affichera le nom d'utilisateur actuel en vert, suivi de "@", suivi du nom complet de l'hôte en vert, suivi de ":", suivi du nom du répertoire de travail actuel en bleu, et enfin, le caractère de l'invite de commande "$".

```
export PS1="\[\e[1;38;5;244m\]\t \[\e[1;36m\]\u@\H \[\e[1;33m\]\w \[\e[1;31m\]\$ \[\e[0m\]"
```
Cette commande affichera l'heure actuelle en gris clair, suivi du nom d'utilisateur et du nom complet de l'hôte en cyan clair, suivi du chemin absolu du répertoire de travail actuel en jaune clair, et enfin, le caractère de l'invite de commande "$" en rouge.

Ces exemples vous donnent une idée de la façon dont vous pouvez personnaliser votre invitation de commande pour afficher les informations que vous jugez importantes, en utilisant différentes couleurs et styles. Avec les caractères de la liste que j'ai fournie et les codes d'échappement de couleur, les possibilités de personnalisation de l'invite de commande sont pratiquement infinies.

Générateur :
```bash
#!/bin/bash

# Menu pour les options de PS1
PS3='Sélectionnez une option pour PS1 : '
options=("Nom d'utilisateur" "Nom d'hôte" "Répertoire de travail" "Couleur" "Quitter")
select opt in "${options[@]}"
do
    case $opt in
        "Nom d'utilisateur")
            export PS1="\u@"
            ;;
        "Nom d'hôte")
            export PS1="\h@"
            ;;
        "Répertoire de travail")
            export PS1="\w/"
            ;;
        "Couleur")
            # Menu pour les options de couleur
            PS3='Sélectionnez une couleur pour PS1 : '
            couleurs=("Vert" "Cyan" "Jaune" "Rouge" "Quitter")
            select couleur in "${couleurs[@]}"
            do
                case $couleur in
                    "Vert")
                        export PS1="\[\e[1;32m\]$PS1\[\e[0m\]"
                        break
                        ;;
                    "Cyan")
                        export PS1="\[\e[1;36m\]$PS1\[\e[0m\]"
                        break
                        ;;
                    "Jaune")
                        export PS1="\[\e[1;33m\]$PS1\[\e[0m\]"
                        break
                        ;;
                    "Rouge")
                        export PS1="\[\e[1;31m\]$PS1\[\e[0m\]"
                        break
                        ;;
                    "Quitter")
                        exit
                        ;;
                    *) echo "Option invalide $REPLY";;
                esac
            done
            ;;
        "Quitter")
            exit
            ;;
        *) echo "Option invalide $REPLY";;
    esac
done

```

### X-Driver xauth


```bash
export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0
```

