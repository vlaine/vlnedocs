Sysadmin - Processus
---

top

ps aux : process docker ou pod avec leur fonction (thanos, nginx...)
	Viens récupérer de /proc 
		eg /proc/7403/exe
			Si je supprime ce programme qui tourne ??
				on peux cp le process et le remettre plus tard
	/proc/7402/cgroup
		Important pour docker et kube
	/proc/7403/status
		Equivalent sctl

```vbnet
strace : tracer les syscall
interface entre userland et kernelland
strace ps aux
on retroue des syscall
balise "stat" importante
```



```javascript
strace -e openat ps aux 
openat(AT_FDCWD, "/proc/179/status", O_RDONLY) = 6
openat(AT_FDCWD, "/proc/179/cmdline", O_RDONLY) = 6
oyaji      179  0.0  0.0   7476  3196 pts/0    R+   22:10   0:00 ps aux
+++ exited with 0 +++
```

comme ps aux ici

Permet de tracer un peu le fonctionnement, si jamais y a des lenteurs

:::info Important 
> comprendre les group en testant à la main
:::


Essayer de bien comprendre tout top


![d3630c21a0e57270de51eed23c04ca3d_MD5.png](img/d3630c21a0e57270de51eed23c04ca3d_MD5.png)