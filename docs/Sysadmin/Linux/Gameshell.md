Gameshell
---

Instructions :

:::note Question  Consignes
> 
> Voici les étapes : 
> 
> 1) Ouvrez un terminal dans votre machine
> 
> 2) Tapez la commande "su -" puis votre mot de passe root
> 
> 3) Lancer la commande d'installation des dépendances : " apt install gettext man-db procps psmisc nano tree bsdmainutils x11-apps wget"
> 
> 4) Puis la commande pour télécharger le script "wget [https://github.com/phyver/GameShell/releases/download/latest/gameshell.sh](https://github.com/phyver/GameShell/releases/download/latest/gameshell.sh)"
> 
> 5) Vérifier avec la commande "ls" que vous voyez affiché "gameshell.sh"
> 
> 6) Normalement si vous faite la commande "pwd" vous devriez voir que vous êtes dans le dossier "/root"
> 
> 7) Copier ensuite le fichier dans votre dossier personnel : "cp gameshell.sh /home/vlaine/"
> 
> 8) Rendez vous dans le dossier : "cd /home/vlaine"
> 
> 9) Puis autoriser l'exécution "chmod +x gameshell.sh"
> 
> 10) Vous pouvez ensuite vous déconnecter du root en faisant "exit"
> 
> 11) Vous devriez normalement voir votre nom d'utilisateur dans la console, rendez vous dans votre dossier personnel "cd /home/vlaine"
> 
> 12) Vous pouvez maintenant exécuter le jeu en faisant "bash gameshell.sh"
> 
> 13) Pour sortir du jeu, tapez exit, et quand vous le relancerez, vous devrez taper "2" pour relancer votre sauvegarde
:::


Bon gameshell