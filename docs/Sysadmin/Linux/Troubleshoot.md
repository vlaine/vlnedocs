Troubleshooting divers
---


:::danger 
> 
> INFO: task jbd2/vda3-8:250 blocked for more than 120 seconds.
>  Not tainted 2.6.32-431.11.2.el6.x86_64 #1
>  kernel: "echo 0 > /proc/sys/kernel/hung_task_timeout_secs" disables this message.
:::

:::tip Done
> 
> echo 0 > /proc/sys/kernel/hung_task_timeout_secs
:::

---

