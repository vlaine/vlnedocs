# Bundle GLPI

--- 

## **PRÉREQUIS GÉNÉRAUX**

La plateforme système choisie est Debian 9.8.0 avec les package de base, serveur web, ssh.

La machine devra également avoir une IP fixe.
### **Convention**
Il est nécessaire de se connecter avec le compte root sauf précision contraire.

Pour vérifier si vous êtes bien sous le login root :

Le prompt en tant que root débute par **#**

- Le prompt à partir d’un autre utilisateur commence  **$, >, ou %**
- Modifications : modification à effectuer dans un fichier
- *Commentaires* : commentaire dans la procédure

## **GLPI – FUSION & OCS INVENTORY**

### **GLPI**
![Téléchargement GLPI 9.5.2 - Télécharger avec Le Parisien](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.001.png)

GLPI, le Gestionnaire Libre de Parc Informatique, est un logiciel libre créé il y a plus de 10 ans par une communauté d'informaticiens en charge de parcs informatiques plus ou moins étendus. C'est avant tout une application de gestion des équipements informatiques et de leurs usages. Le modèle de données et les fonctionnalités natives du logiciel (core) sont extensibles au travers de greffons (plugins).

Les outils d’Inventaire informatique permettent de réaliser l'inventaire des matériels et / ou des licences utilisées dans l’organisation. 

Les outils d'inventaire automatique peuvent être classés en deux grandes familles fonctionnelles : 

- les outils de découverte logiciel 
- les outils de découverte matériel 

Ils peuvent être également classés en deux grandes familles techniques :

- les solutions avec agent pour la découverte de logiciels par exemple 
- les solutions sans agent au niveau réseau (découverte via scan d'une plage d'adresses par le biais du protocole SNMP) 

Les outils d'inventaires font partie intégrante du bon fonctionnement des solutions de gestion de parc puisqu'ils leur fournissent les données nécessaires à la gestion de service. Ils peuvent également alimenter des bases de données existantes.

### **OCS INVENTORY**
![OCS Inventory — Wikipédia](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.002.png)

OCS Inventory NG soit Open Computer and Software Inventory est une application permettant de réaliser un inventaire sur la configuration matérielle des machines du réseau, et sur les logiciels qui y sont installés. Nous pouvons également visualiser ces informations grâce à une interface web. 

Avantages :

- Inventaire de tous les ordinateurs (configuration matériel, nom du PC) 
- Inventaire des logiciels et des clés de licences 
- Interface Web simple d'utilisation 
- Peut être utiliser sans être couplé à GLPI

Inconvénients : 

- Ne s’installe plus sous Windows
### **Fusion Inventory**
![FusionInventory — Wikipédia](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.003.png)

Fusion Inventory est né du projet OCS Inventory. En changeant son architecture de fonctionnement il n’y a plus de serveur central qui récupère les remontées d’inventaire des agents déployés sur les postes mais c’est directement GLPI qui se charge de cette tâche. 

Avantages :

- Remontées d'inventaire centralisé directement dans GLPI. 
- Aucune latence ou de problème de synchronisation avec le serveur d’inventaire.
- Possibilité de forcer la remontée immédiate d’un inventaire d’un poste 

Inconvénients :

- FusionInventory ne peut être utilisé seul. Il doit être couplé à GLPI.

### **Travaux pratiques**

Nous verrons dans ce document l’installation et la configuration de chacune de ses solutions. L’objectif est d’avoir un outil stable qui est adapté au besoin de l’entreprise.
## **INSTALLATION ET CONFIGURATION DE GLPI**
### **Prérequis**
Nous avons précédemment indiqué que nous utilisons la distribution Debian pour l’installation de GLPI. Une fois la distribution installée, nous allons mettre à jour et installer les packages nécessaires au bon déroulement de l’opération.

```bash
apt-get update && apt-get upgrade
```

### **Installation**
:::danger A lire concernant les versions de php
- Les versions de php sont très souvent mises à jours, j'ai ici documenté l'ajout de plusieurs versions différentes sur 2 versions de Debian. Il y a certainement des versions plus récentes aujourd'hui. Votre travail consistera donc également d'adapter les commandes pour installer la version de php nécessaire à la version de GLPI que vous instellerez.
- Les versions et compatibilité sont sur la doc : https://glpi-install.readthedocs.io/en/latest/prerequisites.html#php
:::

| GLPI Version | Minimum PHP | Maximum PHP |
|--------------|-------------|-------------|
|    9.5.X     |     7.2     |     8.0     |
|    10.0.X    |     7.4     |     8.2     |


Même chose pour la base de donnée :

:::caution
Actuellement, seuls les serveurs de base de données MySQL (5.7 minimum) et MariaDB (10.2 minimum) sont pris en charge par GLPI.
Pour fonctionner, GLPI a besoin d'un serveur de base de données.
:::


#### **Prérequis pour l’installation de php sur Debian 9**
Ajout des sources puis update.

```bash
apt-get install ca-certificates apt-transport-https 
```


```bash
wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
```


```bash
echo "deb https://packages.sury.org/php/ stretch main" | tee /etc/apt/sources.list.d/php.list
```


```bash
apt-get update
```
 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.004.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.005.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.006.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.007.png)
 
#### **Prérequis pour l’installation de php sur Debian 10**

```bash
apt-get install ca-certificates apt-transport-https
```


```bash
apt -y install php php-common
```


```bash
apt -y install php-cli php-fpm php-json php-pdo php-mysql php-zip php-gd  php-mbstring php-curl php-xml php-pear php-bcmath
```


```bash
apt -y install libapache2-mod-php
```


```bash
a2enmod php7.3
```


```bash
systemctl restart apache2
```


#### **Installation pour Debian 9**
D’abord installer les packages nécessaires. Dans un bash : 

```bash
 apt-get install php7.2 php7.2-curl php7.2-mysql mysql-server
```


Puis installer les modules php :

```bash
apt-get install php7.2-cli php7.2-common php7.2-curl php7.2-mbstring php7.2-mysql php7.2-xml
```


Configurez ensuite les options basiques de Mysql (MariaDB) : 

```bash
mysql_secure_installation
```

- Changez le password root de mysql

-  Ne supprimer pas les utilisateurs anonymes pour le moment.

- Ne désactivez pas la connexion à distance

- Supprimez la BDD de test

- reload les privilèges


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.008.png)

MariaDb est installé correctement
 
#### **Installation pour Debian 10**

```bash
apt-get install php7.3 php7.3-curl php7.3-mysql mariadb-server
```


Puis installer les modules php :

```bash
apt-get install php7.3-cli php7.3-common php7.3-curl php7.3-mbstring php7.3-mysql php7.3-xml
```


 - Changez le password root de mysql

- Ne supprimer pas les utilisateurs anonymes pour le moment.

- Ne désactivez pas la connexion à distance

- Supprimez la BDD de test

- Reload les privilèges


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.009.png)

MariaDb est installé correctement

#### **Création de la base de données :** 
Nous créons ensuite la base de données que GLPI utilisera.

Connexion à la base :

- `mysql -u root -p` à Connexion à mysql
- ou `mysql -u root` à Connexion à mysql sans MDP
- `Enter Password` : (mot de passe précedemment choisi)

```sql
create database dbglpi character set utf8; # Création de la base de donnée “dbglpi”
```

```sql
grant all privileges on dbglpi.* to glpi@localhost identified by 'glpi';
```
 Donner tous les privilèges d’administration de la base " dbglpi " au nouvel utilisateur " glpi " avec le mot de passe " glpi ". 
 
:::caution

 **ATTENTION AU GUILLEMET SIMPLE, SI VOUS FAITE UN COPIER COLLER, N’OUBLIEZ PAS DE VERIFIER LES GUILLEMETS, ILS DOIVENT ETRE CORRECTEMENT ENTREES.**
:::
```bash
exit # Pour quitter la base
```
 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.010.png)![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.011.png)![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.012.png)![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.013.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.014.png)

Redémarrer ensuite le service apache.
 
```bash
/etc/init.d/apache2 restart
```
 
Il est ensuite possible de voir dans le navigateur si apache a été correctement installé. Simplement en tapant l’adresse IP de notre serveur dans la barre de lien.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.015.png)

**Vous pouvez également y accéder directement depuis l’interface graphique de votre VM, ouvrez firefox puis taper " localhost ", dans la barre d’adresse :**

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.016.png)

Ensuite rendez-vous sur le site officiel de glpi et télécharger la dernière version de celui-ci. Attention, ce n'est pas forcément la dernière version ici. Attention 

:::caution
- **LA VERSION ICI PEUT AVOIR CHANGER, ESSAYER DE PRENDRE LA DERNIERE DISPONIBLE SUR LE GITHUB. CECI VAUT POUR TOUT LES LIENS PRESENTS DANS CE DOCUMENT.**
- Lien des releases : https://github.com/glpi-project/glpi/releases/
- La dernière en date de cette modification est la 10.0.6. J'ajouterai des notes au fur et à mesure pour indiquer d'eventuels changement si vous choisissez d'installer cette version.
:::

```bash
wget https://github.com/glpi-project/glpi/releases/download/9.5.2/glpi-9.5.2.tgz
```
 
Ensuite on décompresse l’archive dans le fichier /var/www/ (répertoire d’installation de site web pour apache2 par défaut)

```bash
tar xvf glpi-9.1.2.tgz –C /var/www/
```
 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.017.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.018.png)
 
Ensuite il nous faudra modifier les configuration apache.

```bash
vim /etc/apache2/sites-enabled/000-default.conf
```
 
Modifier la ligne pour avoir " DocumentRoot /var/www " comme suit. Puis quittez l’éditeur.

```bash
/etc/init.d/apache2 reload pour prendre en compte la configuration.
```
 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.019.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.020.png)
 
#### **Vérifier sa version de PHP**

Nous avons besoin de la version 7.2 ou ultérieure, sous debian 10, vous aurez normalement la 7.3, si c’est le cas passez à la page 20.
 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.021.png)
#### **Utiliser la version 7.2 de PHP (si vous êtes sous debian 9)**

Si vous avez déjà installé une autre version ; suivez les instructions pour la supprimer et passer à la version 7.2 ou ultérieure, car les dernières versions de GLPI ne fonctionnent qu’avec la 7.2 ou plus ?

**Si vous avez directement installé php 7.2, passez les étapes qui correspondent à la version de php5.6.**

Pour commencer, il faut désactiver le module actuellement actif, PHP 5.6 dans l’exemple ci-dessous

```bash
srv-glpi-deb01:~# a2dismod php5.6
```


```bash
Module php5.6 disabled
To activate the new configuration, you need to run:
systemctl restart apache2
```


```bash
systemctl restart apache2
```


Puis activer PHP 7.2

```bash
a2enmod php7.2
```


```bash
Considering dependency mpm_prefork for php7.2:
 
 Considering conflict mpm_event for mpm_prefork:
 
 Considering conflict mpm_worker for mpm_prefork:
 
 Module mpm_prefork already enabled
 
 Considering conflict php5 for php7.2:
 
 Enabling module php7.2.
 
 To activate the new configuration, you need to run:
 systemctl restart apache2
```



```bash
systemctl restart apache2
```


Vous pouvez maintenant configurer les paramètres dans le fichier de conf ci-dessous :

Vous pouvez par exemple augmenter la taille maximale pour le téléversement de fichiers (la limite de 2M par défaut est souvent trop limitée notamment pour restaurer les fichiers XML de WordPress)

```bash
srv-glpi-deb01:~# vim /etc/php/7.2/cli/php.ini
```

```bash

 ; Maximum allowed size for uploaded files.
 
 ; http://php.net/upload-max-filesize
 
 upload_max_filesize = 4G
```
 
Après cette montée de version, il est conseillé de purger les versions obsolètes qui trainent sur votre serveur.

Pour cela, identifier les versions présente :

```bash
srv-glpi-deb01:~# ls -lrt /etc/php
```

```markdown
 
 total 8
 drwxr-xr-x 5 root root 4096 nov.  25 09:52 5.6
 drwxr-xr-x 5 root root 4096 nov.  25 10:19 7.2
```



Nous supprimons ensuite les anciennes versions de PHP sur notre serveur.

Dans l’exemple ci-dessous, nous supprimons la version 5.6. (Répétez la commande pour chaque version)

```bash
srv-glpi-deb01:~# apt purge php5.6 libapache2-mod-php5.6
```
```bash
Lecture des listes de paquets... Fait
 
 Construction de l'arbre des dépendances
 
 Lecture des informations d'état... Fait

Les paquets suivants seront ENLEVÉS :

 `  `libapache2-mod-php5.6* php5.6*

 0 mis à jour, 0 nouvellement installés, 2 à enlever et 6 non mis à jour.
 
 Après cette opération, 4 815 ko d'espace disque seront libérés.
 
Souhaitez-vous continuer ? [O/n] o
 
 (Lecture de la base de données... 39986 fichiers et répertoires déjà installés.)
 
 Suppression de php5.6 (5.6.40-38+0~20201103.42+debian9~1.gbpb211e0) ...
 
 Suppression de libapache2-mod-php5.6 (5.6.40-38+0~20201103.42+debian9~1.gbpb211e0) ...
 
 apache2_invoke php5.6 prerm: No action required
 
 (Lecture de la base de données... 39971 fichiers et répertoires déjà installés.)
 
 Purge des fichiers de configuration de libapache2-mod-php5.6 (5.6.40-38+0~20201103.42+debian9~1.gbpb211e0) ...
 
 apache2_invoke postrm: Purging state for php5.6
```

Vérifier avec cette commande que tout est bien installé.

```bash
srv-glpi-deb01:~# apt-cache policy php7.2
```


```bash
 php7.2:
 
 Installé : 7.2.34-8+0~20201103.52+debian9~1.gbpafa084
 
 Candidat : 7.2.34-8+0~20201103.52+debian9~1.gbpafa084
 
 Table de version :
 
 *** 7.2.34-8+0~20201103.52+debian9~1.gbpafa084 500
 
         500 https://packages.sury.org/php stretch/main amd64 Packages
 
         100 /var/lib/dpkg/status

```
 
![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.022.png)


#### **Configuration du php.ini** 
En premier configurons celui de GLPI : 

```bash
vim /var/www/glpi/vendor/blueimp/jquery-file-upload/server/php/php.ini
```
 
![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.023.png)

Puis dans les fichiers :

- /etc/php/7.2/apache2/php.ini
- /etc/php/7.2/cli/php.ini

Modifiez la ligne " memory_limit " et définissez 128M.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.024.png)


Vous pouvez ensuite accéder au portail d’installation de GLPI avec l’IP de votre serveur. Dans un navigateur, tapez `<http://[votreIP]/glpi/>.`

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.025.png)

Choisir Français.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.026.png)

Accepter les conditions, puis continuer.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.027.png)

Avant de cliquer sur Installer, ajoutez les droits à l’utilisateur et au groupe www-data sur tout le dossier glpi.

```kotlin
chown –R www-data:www-data /var/www/glpi/
```

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.028.png)

Puis cliquez sur Installer.


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.029.png)

Nous voyons ici que nous avons plusieurs erreurs/warnings. Pour les pallier il faut :

- Installer les extensions PHP : 
  - Gd
  - Intl
  - Ldap
  - Apcu
  - Xmlrpc
  - Cas
  - Zip
  - Bz2
- Modifier un fichier apache.

:::note
**N :B : Il est possible qu’il ne manque pas éxactement les mêmes paquets sur votre installations, si c’est le cas, ciblez les paquets manquants et installez les.**
:::

:::note Seealso

Comme suit pour la version 7.2, **si vous êtes sous une autre version comme la 7.3 (Debian 10), remplacez le packets par les versions 7.3 :** 
:::


```bash
apt-get install php7.2-gd php7.2-intl php7.2-ldap php7.2-imap php-apcu php7.2-xmlrpc php-cas php7.2-zip php7.2-bz2
```


```bash
vim /etc/apache2/apache2.conf
```


Ajouter dans le fichier : 


```php
<Directory /var/www/glpi>
Options Indexes FollowSymLinks
AllowOverride limit
Require all granted
</Directory>
```



Cela doit ressembler à ça :

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.030.png)

Ensuite vous pouvez cliquer sur Réessayer, si les erreurs sont encore la rebootez votre machine et réessayer.

Après ces modifications, vous aurez normalement une fenêtre sans erreurs : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.031.png)

Cliquez sur Continuer.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.032.png)

Connectez-vous ensuite à la base de données comme sur la capture, avec l’utilisateur glpi et le mot de passe défini précédemment.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.033.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.034.png)

La jonction est effectuée.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.035.png)

Cliquez sur Continuer.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.036.png)

Continuer, sauf si vous voulez faire un don.

Puis cliquez sur Utiliser GLPI.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.037.png)

Vous arriverez ensuite sur la fenêtre de connexion.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.038.png)

:::note

Connectez-vous à l’interface avec l’utilisateur par défaut " glpi " avec comme mot de passe " glpi ".
:::


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.039.png)

Ce message va apparaître sur l’interface. Pour le supprimer, rendez-vous dans le dossier glpi et supprimer install.php.

```bash
rm /var/www/glpi/install/install.php
```


Puis cliquez sur glpi post-only tech et normal pour changer le mot de passe par défaut de tech.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.040.png)

D’autres messages similaires vont ensuite apparaitre, faite la même manipulation pour tous : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.041.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.042.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.043.png)


## **INSTALLATION DE FUSION INVENTORY**
D’abord, télécharger le plugin sur le site officiel, le décompresser, puis le déplacer dans le dossier des plugins de GLPI.

:::note Link
- https://github.com/fusioninventory/fusioninventory-for-glpi/releases
- https://github.com/fusioninventory/fusioninventory-for-glpi/archive/glpi9.5.0+1.0.tar.gz
- Version 10.0.6 : https://github.com/fusioninventory/fusioninventory-for-glpi/releases/download/glpi10.0.6%2B1.1/fusioninventory-10.0.6+1.1.tar.bz2 
:::


```bash
srv-glpi-deb01:~/sources# wget https://github.com/fusioninventory/fusioninventory-for-glpi/archive/glpi9.5.0+1.0.tar.gz
```

Eg pour fusion 10.0.6 : 
```bash
tar xvf fusioninventory-10.0.6+1.1.tar.bz2  -C /var/www/glpi/plugins/
```


Renommer ensuite le dossier comme si nécessaire : 

```bash
mv /var/www/glpi/plugins/fusioninventory-10.0.6+1.1 /var/www/glpi/plugins/fusioninventory
```


Se rendre ensuite dans Configuration Plugins dans GLPI.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.044.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.045.png)

Cliquer sur l’icone ![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.046.png) pour installer le plugin.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.047.png)

Puis sur ![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.048.png) pour l’activer.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.049.png)

Activer le crontab pour l’execution de Fusion :

```bash
crontab -e
```


Et ajoutez la ligne (7.3 pour debian 10) :

```bash
* * * * * /usr/bin/php7.2 /var/www/glpi/front/cron.php &>dev/null
```


Vous pouvez tester le crontab directement depuis votre bash, si il n’y a pas d’erreurs c’est ok :

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.050.png)

Une fois l’installation du serveur GLPI terminé ainsi que l’installation de son plugin FusionInventory, il faut installer un agent de FusionInventory sur les postes clients du parc informatique pour que cet agent envoie des paquets transportant les informations de l’inventaire à réaliser sur le serveur GLPI.

### **Installation de l’agent fusion**
Commencez par télécharger l’agent sur votre machine cliente :

https://github.com/fusioninventory/fusioninventory-agent/releases

Puis exécutez le : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.051.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.052.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.053.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.054.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.055.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.056.png)


A cette étape, plusieurs choix sont disponibles :

- Mode Local=Si vous souhaitez un inventaire local pour importer plus tard sur votre serveur GLPI
- Mode Serveurs=Si vous souhaitez que l’inventaire se fasse automatiquement et que cet inventaire remonte sur votre serveur GLPI tout seul.

Dans notre cas nous choisissons la seconde option, il suffit donc d’écrire dans le champ mode serveurs le chemin où nous souhaitons avoir notre fichier d’inventaire (ici l’adresse ip du serveur glpi). Et finir en cliquant sur "Suivant":

Dans Mode Serveurs : 

`<http://IP_DU_SERVEUR_GLPI/glpi/plugins/fusioninventory>`

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.057.png)

Puis laissez vide ici : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.058.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.059.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.060.png)

Sélectionnez le Mode d’exécution comme un service Windows.

Faite suivant jusqu’à arriver à cette page, puis cliquez sur Installer : 

Fichier de log : `C:\Program Files\FusionInventory-Agent\logs\fusioninventory-agent.log`

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.061.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.062.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.063.png)


Une fois l'installation terminée, vous verrez l’agent en mode Running dans les services Windows : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.064.png)

Je peux maintenant voir mon ordinateur qui est bien remonté dans l’inventaire GLPI : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.065.png)
## **OCS INVENTORY**
### **Installation**

:::caution

**ATTENTION A BIEN ADAPTER LES COMMANDES EN FONCTION DE VOTRE VERSION DE PHP, DANS L’EXEMPLE NOUS SOMME EN 7.2, SI VOUS ETES EN 7.3, A VOUS D’ADAPTER VOS CONFIGURATIONS/COMMANDES  :**
:::

D’abord, installer les dépendances nécessaires dans cet ordre.
```bash
apt-get install apache2-dev
apt-get install libmariadbclient-dev # (pour avoir mysql_config et éviter bien 
des ennuis après)
apt-get install php7.2-soap unzip
cpan install --force CPAN  (attention il y a 2 tirets) (cela peut prendre du temps)
cpan install YAML
cpan install Mojolicious::Lite Switch Plack::Handler # Répondez yes si une question vous est posée.
cpan install XML::Simple Compress::Zlib DBI Apache::DBI Net::IP Archive::Zip XML::Entities
apt-get install perl libxml-simple-perl libperl5.24 libdbi-perl libdbd-mysql-perl libapache-dbi-perl libnet-ip-perl libsoap-lite-perl libarchive-zip-perl make build-essential php-pclzip php-intl php-curl php-mysql php-gd php7.2-simplexml
cpan install Apache2::SOAP
```


### **Pour Debian 10 :**

```bash
apt-get install cmake gcc make build-essential
```

```bash
apt -y install libapache2-mod-perl2 libapache-dbi-perl libapache-db-perl libapache2-mod-php 
```

```bash
apt -y install perl libxml-simple-perl libcompress-zlib-perl libdbi-perl libdbd-mysql-perl libnet-ip-perl libsoap-lite-perl libio-compress-perl libapache-dbi-perl libapache2-mod-perl2 libapache2-mod-perl2-dev libdbd-mysql-perl libnet-ip-perl libxml-simple-perl libarchive-zip-perl 
```

```sql
cpan install XML::Entities Apache2::SOAP Net::IP Apache::DBI Mojolicious Switch Plack::Handler Archive::Zip
```


Une fois les packages installés, téléchargez l’archive OCS Server sur le site officiel avec " wget " puis aller dans le dossier décompressé.

:::note Liens 
- https://github.com/OCSInventory-NG/OCSInventory-ocsreports/releases/
- Version 2.11.1 : https://github.com/OCSInventory-NG/OCSInventory-ocsreports/releases/download/2.11.1/OCSNG_UNIX_SERVER-2.11.1.tar.gz 
:::

```bash
wget https://github.com/OCSInventory-NG/OCSInventory-ocsreports/releases/download/2.8/OCSNG_UNIX_SERVER_2.8.tar.gz
```

```bash
tar xvf OCSNG_UNIX_SERVER_2.8.tar.gz
```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.066.png)

Exécutez ensuite le setup.sh pour commencer l’installation.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.067.png)

Normalement, il n’y a rien à changer pendant l’installation, vérifiez que les paramètres proposés correspondent : 

:::info Les réponses aux questions


- Do you wish to continue ([y]/n)? **Entrée**
- Which host is running database server [localhost] ? **Entrée**
- On which port is running database server [3306] ? **Entrée**
- Where is Apache daemon binary [/usr/sbin/apache2ctl] ? **Entrée**
- Where is Apache main configuration file [/etc/apache2/apache2.conf] ? **Entrée**
- Where is Apache main configuration file [/etc/apache2/apache2.conf] ? **Entrée**
- Which user group is running Apache web server [www-data] ? **Entrée**
- Where is Apache Include configuration directory [/etc/apache2/conf-available] ? **Entrée**
- Where is PERL interpreter binary [/usr/bin/perl] ? **Entrée**
- Do you wish to setup Communication server on this computer ([y]/n)? **Entrée**
- Where to put Communication server log directory [/var/log/ocsinventory-server] ? **Entrée**
- Where to put Communication server plugins configuration files [/etc/ocsinventory-server/plugins] ? **Entrée**
- Where to put Communication server plugins Perl modules files [/etc/ocsinventory-server/perl]  **Entrée**
- Do you wish to setup Rest API server on this computer ([y]/n)? **Entrée**
- Where do you want the API code to be store [/usr/local/share/perl/5.24.1] ? **Entrée**
- Do you allow Setup renaming Communication Server Apache configuration file
  to ‘z-ocsinventory-server.conf’ ([y]/n) ? **Entrée**
- Do you wish to setup Administration Server (Web Administration Console)
  on this computer ([y]/n)? **Entrée**
- Do you wish to continue ([y]/n)? **Entrée**
- Where to copy Administration Server static files for PHP Web Console
  [/usr/share/ocsinventory-reports] ? **Entrée**
- Where to create writable/cache directories for deployment packages,
  administration console logs, IPDiscover and SNMP [/var/lib/ocsinventory-reports] ? **Entrée**

:::


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.068.png)

Restart le service apache.

```bash
systemctl restart apache2
```


### **Configuration**

Fichiers utiles d’OCS : 
:::note


- /etc/apache2/conf-available/z-ocsinventory-server.conf
- /etc/apache2/conf-available/ocsinventory-reports.conf
- /etc/apache2/conf-available/zz-ocsinventory-restapi.conf
:::


### **Créer la base de donnée OCS**

```sql
mysql -u root -p

CREATE DATABASE ocsweb;

CREATE USER 'ocsbdd'@'localhost' IDENTIFIED BY 'ocsinventory';

GRANT ALL PRIVILEGES ON ocsweb. * TO 'ocsbdd'@'localhost';

MariaDB [(none)]quit
```

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.069.png)


Rendez vous ensuite dans la configuration apache2, dans le dossier conf-available.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.070.png)

Puis éditez le fichier `/etc/apache2/conf-available/z-ocsinventory-server.conf` pour faire correspondre les infos à notre base de donnée OCS.


```yaml
PerlSetEnv OCS_DB_HOST localhost
PerlSetEnv OCS_DB_PORT 3306
PerlSetEnv OCS_DB_NAME ocsweb
PerlSetEnv OCS_DB_LOCAL ocsweb
PerlSetEnv OCS_DB_USER ocsbdd
PerlSetVar OCS_DB_PWD ocsinventory
```


Il s’agit donc d’insérer le nom de l’utilisateur ocsbdd, celui qu’on a créé sous MySQL et qui a les droits sur la BDD ocsweb. Votre base de données se trouve sur la même machine que OCS donc HOST est localhost, et normalement vous n’avez pas changé le port 3306. Vous n’avez donc que l’utilisateur et son mot de passe à renseigner (4-5).

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.071.png)

Pareil pour `/etc/apache2/conf-available/zz-ocsinventory-restapi.conf`

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.072.png)

Modifiez les droits sur le dossier `/var/lib/ocsinventory-reports` :

```bash
chown -R root:www-data /var/lib/ocsinventory-reports
```


```bash
chmod 775 /var/lib/ocsinventory-reports
```

Activez ensuite les configurations web d’OCS inventory pour faire monter l’interface WEB. Trois commandes sont à effectuer :

```bash
a2enconf z-ocsinventory-server
a2enconf ocsinventory-reports
a2enconf zz-ocsinventory-restapi
```


Vous aurez à chaque configuration le message : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.073.png)

Redémarrez le service Apache pour valider.

```bash
systemctl reload apache2
```


Accédez à l’interface OCS avec l’adresse [**http://VotreIP/ocsreports](http://VotreIP/ocsreports)**.**

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.074.png)

Entrez le login et mot de passe de la base de données, puis le nom de la database. Le HostName est l’adresse IP du serveur de base de données, nous sommes ici en local.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.075.png)

- Si vous avez le message " var lib dir should be writable : … " c’est que vous n’avez pas bien ajouté les droits au dossier.


- Prenez note également du bandeau qui indique ce message :

:::caution

*WARNING: You will not be able to build any deployment package with size greater than 100MB*
*You must raise both post_max_size and upload_max_filesize in your vhost configuration to increase this limit.*
:::

- Cela signifie que dans le cadre de déploiements de packages par le biais d’OCS, la limite de taille des fichiers sera de 100Mo. 
- Cliquez enfin sur **Send**.

Pour modifier :

```bash
vim /etc/apache2/conf-available/ocsinventory-reports.conf
```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.076.png)


Ces valeurs seront a adapter en fonction de vos besoins de déploiement, selon la taille des packages. `Upload_max_filesize` doit être plus grand que la somme des packages du déploiement, et `post_max_size` doit être plus grand que `upload_max_filesize`.

Si vous on demande une update de la database depuis l’interface web, faite là.

La configuration a été validé. Cliquez sur `“Click here to enter OCS-NG GUI”.`

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.077.png)

Le mot de passe et le login par défaut sont **admin/admin.**

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.078.png)

Créez un nouvel utilisateur ocsadmin (Configuration à Utilisateurs) 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.079.png)

Se connecter à OCS avec le nouveau super administrateur et supprimez l’utilisateur nommé admin.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.080.png)

Allez ensuite dans le menu **Configuration à Configuration Générale puis l’onglet Serveur** et activez l’option **Trace_Deleted.**

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.081.png)

Pour terminer, il nous faut maintenant supprimer le fichier install.php du dossier OCS comme ci-dessous.

```bash
rm /usr/share/ocsinventory-reports/ocsreports/install.php
```

Puis relancez apache et tout est bon. 

### **Installation de PHPMYADMIN**
Afin de gérer la base de données, nous installerons PhPmyAdmin. 

Installer d’abord les petites dépendances et activer le module php.

**Sur Debian 9, le package est directement disponible dans les sources, mais sur Debian 10, vous devrez l’installer manuellement.**

```bash
apt install php-mbstring php-gettext
```


Puis phpmyadmin :

```bash
apt-get install phpmyadmin
```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.082.png)

Choisissez apache2.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.083.png)

Choisissez Oui.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.084.png)

Tapez le mot de passe root de la base de données. 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.085.png)

```bash
phpenmod mbstring
```

```bash
systemctl restart apache2
```


phpMyAdmin est maintenant installé et configuré. Cependant, avant de pouvoir vous connecter et commencer à gérer vos bases de données MariaDB, vous devez vous assurer que vos utilisateurs MariaDB disposent des privilèges requis pour interagir avec le programme.

Créer donc un utilisateur qui aura tous les privilèges :

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.086.png)

Vous pourrez ensuite vous connecter à celui-ci et avoir accès à toutes les bases de données présentes sur votre serveur.

### **Installation manuelle de phpmyadmin (Pour Debian 10 avec php 7.3)**

**Il faut se rendre sur le site de PHPmyadmin à l’adresse suivante pour télécharger la dernière version du paquet** : https://www.phpmyadmin.net/downloads/, téléchargez-le directement sur le serveur avec les commandes suivantes : 

```bash linenums="1"

cd /tmp/

wget https://files.phpmyadmin.net/phpMyAdmin/4.9.2/phpMyAdmin-4.9.2-all-languages.tar.gz

tar xvf phpMyAdmin-4.9.2-all-languages.tar.gz

## Déplacer le dossier dans le répertoire /user/share

mv phpMyAdmin-4.9.2-all-languages /usr/share/phpmyadmin

#Donner les droits du dossier à l’utilisateur www-data du service apache

chown -R www-data:www-data /usr/share/phpmyadmin

## Il faut créer un dossier temporaire pour phpmyadmin

mkdir -p /var/lib/phpmyadmin/tmp

chown www-data:www-data /var/lib/phpmyadmin/tmp

#Se connecter à mysql pour créer une base pour phpmyadmin

mysql -u root

```

```sql
mysqlCREATE DATABASE phpmyadmin DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```


Les commandes suivantes vont nous permettre de créer un utilisateur phpmyadmin qui aura les droits sur la base nommée phpmyadmin avec un mot de passe

```sql
CREATE USER 'phpmyadmin'@'localhost' IDENTIFIED BY 'motdepasse';

GRANT ALL ON phpmyadmin.* TO 'phpmyadmin'@'localhost';

On applique les privilèges et on se déconnecte de Mysql

FLUSH PRIVILEGES;

exit;
```


Nous pouvons lancer l’installation des paquets additionnels d’apache pour phpmyadmin.

```bash
apt-get install -y php7.3-common php7.3-mysql php7.3-gd php7.3-imap php7.3-json php7.3-curl php7.3-zip php7.3-xml php7.3-mbstring php7.3-bz2 php7.3-intl php7.3-gmp
```


On redémarre le service apache

```bash
systemctl restart apache2
```

Nous allons créer le fichier de configuration qui va nous permettre d’accès à la page web de phpmyadmin via apache.

```bash
vi  /etc/apache2/conf-available/phpmyadmin.conf 
```


Coller et enregistrer le fichier de configuration suivant

```bash
## phpMyAdmin default Apache configuration

Alias /phpmyadmin /usr/share/phpmyadmin
<Directory /usr/share/phpmyadmin>
    Options SymLinksIfOwnerMatch
    DirectoryIndex index.php
    <IfModule mod_php5.c>
        <IfModule mod_mime.c>
            AddType application/x-httpd-php .php
        </IfModule>
        <FilesMatch ".+\.php$">
            SetHandler application/x-httpd-php
        </FilesMatch>
        php_value include_path .
        php_admin_value upload_tmp_dir /var/lib/phpmyadmin/tmp
        php_admin_value open_basedir /usr/share/phpmyadmin/:/etc/phpmyadmin/:/var/lib/phpmyadmin/:/usr/share/php/php-gettext/:/usr/share/php/php-php-gettext/:/usr/share/javascript/:/usr/share/php/tcpdf/:/usr/share/doc/phpmyadmin/:/usr/share/php/phpseclib/
        
        php_admin_value mbstring.func_overload 0

    </IfModule>
    <IfModule mod_php.c>
        <IfModule mod_mime.c>
            AddType application/x-httpd-php .php
        </IfModule>
        <FilesMatch ".+\.php$">
            SetHandler application/x-httpd-php
        </FilesMatch>
        php_value include_path .
        php_admin_value upload_tmp_dir /var/lib/phpmyadmin/tmp
        php_admin_value open_basedir /usr/share/phpmyadmin/:/etc/phpmyadmin/:/var/lib/phpmyadmin/:/usr/share/php/php-gettext/:/usr/share/php/php-php-gettext/:/usr/share/javascript/:/usr/share/php/tcpdf/:/usr/share/doc/phpmyadmin/:/usr/share/php/phpseclib/

        php_admin_value mbstring.func_overload 0

    </IfModule>
</Directory>
## Disallow web access to directories that don't need it
<Directory /usr/share/phpmyadmin/templates>
    Require all denied
</Directory>
<Directory /usr/share/phpmyadmin/libraries>
    Require all denied
</Directory>
<Directory /usr/share/phpmyadmin/setup/lib>
    Require all denied
</Directory>

```

Activer le fichier de configuration via la commande suivante

```bash
a2enconf phpmyadmin.conf
```


On redémarre le service apache

```bash
systemctl restart apache2
```


Il faut créer et modifier le fichier de configuration de phpmyadmin

```bash
cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php 
```


```bash
vi  /usr/share/phpmyadmin/config.inc.php 
```


Modifier les informations suivantes :

```bash
. . .

$cfg['blowfish_secret'] = ''; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */

. . .
```

Par


```bash
$cfg['blowfish_secret'] = '2O:.uw6-8;Oi9R=3W{tO;/QtZ]4OG:T:'; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */
```



Et les lignes suivantes :

```makefile
/* User used to manipulate with storage */

 // $cfg['Servers'][$i]['controlhost'] = '';

 // $cfg['Servers'][$i]['controlport'] = '';

 // $cfg['Servers'][$i]['controluser'] = 'pma';

 // $cfg['Servers'][$i]['controlpass'] = 'pmapass';

 /* Storage database and tables */

 // $cfg['Servers'][$i]['pmadb'] = 'phpmyadmin';

 // $cfg['Servers'][$i]['bookmarktable'] = 'pma__bookmark';

 // $cfg['Servers'][$i]['relation'] = 'pma__relation';

 // $cfg['Servers'][$i]['table_info'] = 'pma__table_info';

 // $cfg['Servers'][$i]['table_coords'] = 'pma__table_coords';

 // $cfg['Servers'][$i]['pdf_pages'] = 'pma__pdf_pages';

 // $cfg['Servers'][$i]['column_info'] = 'pma__column_info';

 // $cfg['Servers'][$i]['history'] = 'pma__history';

 // $cfg['Servers'][$i]['table_uiprefs'] = 'pma__table_uiprefs';

 // $cfg['Servers'][$i]['tracking'] = 'pma__tracking';

 // $cfg['Servers'][$i]['userconfig'] = 'pma__userconfig';

 // $cfg['Servers'][$i]['recent'] = 'pma__recent';

 // $cfg['Servers'][$i]['favorite'] = 'pma__favorite';

 // $cfg['Servers'][$i]['users'] = 'pma__users';

 // $cfg['Servers'][$i]['usergroups'] = 'pma__usergroups';

 // $cfg['Servers'][$i]['navigationhiding'] = 'pma__navigationhiding';

 // $cfg['Servers'][$i]['savedsearches'] = 'pma__savedsearches';

 // $cfg['Servers'][$i]['central_columns'] = 'pma__central_columns';

 // $cfg['Servers'][$i]['designer_settings'] = 'pma__designer_settings';

 // $cfg['Servers'][$i]['export_templates'] = 'pma__export_templates';
```

Par

```makefile
$/* User used to manipulate with storage */

 // $cfg['Servers'][$i]['controlhost'] = '';

 // $cfg['Servers'][$i]['controlport'] = '';

  $cfg['Servers'][$i]['controluser'] = 'phpmyadmin';

  $cfg['Servers'][$i]['controlpass'] = 'Votre_mot_depasse_phpmyadmin';

 /* Storage database and tables */

 $cfg['Servers'][$i]['pmadb'] = 'phpmyadmin';

 $cfg['Servers'][$i]['bookmarktable'] = 'pma__bookmark';

 $cfg['Servers'][$i]['relation'] = 'pma__relation';

 $cfg['Servers'][$i]['table_info'] = 'pma__table_info';

 $cfg['Servers'][$i]['table_coords'] = 'pma__table_coords';

 $cfg['Servers'][$i]['pdf_pages'] = 'pma__pdf_pages';

 $cfg['Servers'][$i]['column_info'] = 'pma__column_info';

 $cfg['Servers'][$i]['history'] = 'pma__history';

 $cfg['Servers'][$i]['table_uiprefs'] = 'pma__table_uiprefs';

 $cfg['Servers'][$i]['tracking'] = 'pma__tracking';

 $cfg['Servers'][$i]['userconfig'] = 'pma__userconfig';

 $cfg['Servers'][$i]['recent'] = 'pma__recent';

 $cfg['Servers'][$i]['favorite'] = 'pma__favorite';

 $cfg['Servers'][$i]['users'] = 'pma__users';

 $cfg['Servers'][$i]['usergroups'] = 'pma__usergroups';

 $cfg['Servers'][$i]['navigationhiding'] = 'pma__navigationhiding';

 $cfg['Servers'][$i]['savedsearches'] = 'pma__savedsearches';

 $cfg['Servers'][$i]['central_columns'] = 'pma__central_columns';

 $cfg['Servers'][$i]['designer_settings'] = 'pma__designer_settings';

 $cfg['Servers'][$i]['export_templates'] = 'pma__export_templates';
```

On redémarre le service apache

```bash
systemctl restart apache2
```


Maintenant vous pouvez accéder à la page web de phpmyadmin `http://VOTRE_IP_OU_DOMAINE/phpmyadmin`.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.087.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.088.png)

### **Sécuriser PhpMyAdmin**
### **.htaccess**

L’un des moyens les plus simples consiste à placer une passerelle devant toute l’application en utilisant les fonctionnalités d’authentification et d’authentification intégrées `.htaccess` d’Apache.

Pour ce faire, vous devez d’abord activer l’utilisation des substitutions de fichiers `.htaccess` en modifiant votre fichier de c :::$1 ( ou !!! \1 )onfiguration Apache.

Ajoutez dans le fichier `/etc/apache2/conf-enabled/phpmyadmin.conf`  directive `AllowOverride All` dans la section `<Directory/usr/share/phpmyadmin>` du fichier de configuration, comme suit:

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.089.png)

Activez le module Rewrite puis pour implémenter les modifications que vous avez apportées, redémarrez Apache:

```bash
a2enmod rewrite
```


```bash
systemctl restart apache2
```


Maintenant que vous avez activé l’utilisation .htaccess pour votre application, vous devez en créer un pour réellement mettre en œuvre une sécurité.

Pour que cela réussisse, le fichier doit être créé dans le répertoire de l’application. Vous pouvez créer le fichier nécessaire et l’ouvrir dans votre éditeur de texte avec les privilèges root en tapant:

```bash
vim /usr/share/phpmyadmin/.htaccess
```


Ajouter les lignes :
```nginx

AuthType Basic

AuthName "Restricted Files"

AuthUserFile /etc/phpmyadmin/.htpasswd

Require valid-user

```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.090.png)

Voici ce que chacune de ces lignes signifient :

:::info

- **AuthType Basic:** Cette ligne spécifie le type d’authentification que vous implémentez. Ce type implémentera l’authentification par mot de passe à l’aide d’un fichier de mot de passe.
- **AuthName** : Ceci définit le message pour la boîte de dialogue d’authentification. Vous devez conserver ce générique afin que les utilisateurs non autorisés ne reçoivent aucune information sur ce qui est protégé.
- **AuthUserFile**: Ceci définit l’emplacement du fichier de mot de passe qui sera utilisé pour l’authentification. Cela devrait être en dehors des répertoires qui sont servis. Nous allons créer ce fichier sous peu.
- **Require valid-user**: Ceci spécifie que seuls les utilisateurs authentifiés doivent avoir accès à cette ressource. C’est ce qui empêche les utilisateurs non autorisés d’entrer.
:::


L’emplacement que vous avez sélectionné pour votre fichier de mots de passe était `/etc/phpmyadmin/.htpasswd`.

Vous pouvez maintenant créer ce fichier et le transmettre à un utilisateur initial avec l’utilitaire htpasswd:

```bash
htpasswd -c /etc/phpmyadmin/.htpasswd adminbdd
```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.091.png)

Si vous voulez ajouter un autre utilisateur vous pouvez utiliser la même commande mais sans le " -c ", cela donnerai quelque chose comme : 

```bash
htpasswd /etc/phpmyadmin/.htpasswd adminbdd2
```


Relancez le service Apache.

Et lors de la connexion à l’interface web, on vous demandera les logins/mot de passe créés avec htaccess.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.092.png)


### **Certificats https**
Il est conseillé d’avoir ajouté la machine dans un DNS (Windows ou Bind par exemple) pour la résolution de nom.

Activation du SSL pour apache et installation openssl : 
```bash
a2enmod ssl
service apache2 restart
apt-get install openssl

# Nous devons ensuite générer un certificat : 

cd /tmp
openssl genrsa -des3 -out server.key 2048
openssl req -new -key server.key -out server.csr
cp server.key server.key.org
openssl rsa -in server.key.org -out server.key
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
mkdir /etc/apache2/certificate
cp server.crt server.key /etc/apache2/certificate
```

:::info

Remarquez que nous les plaçons notre clé et notre certificat dans /etc/apache2/certificate, mais vous pouvez très bien le placer ailleurs. De plus, j’ai utilisé server.key, server.csr… pour des raisons de simplicité, mais vous pouvez les nommer comme vous voulez. 
:::

Nous allons maintenant modifier le vhost préalablement créé afin de forcer l’utilisation de HTTPS. Mais d’abord, vérifions qu’Apache écoute bien le port 443 (qui correspond à HTTPS). Votre fichier `/etc/apache2/ports.conf` doit ressembler à ça :

```http
NameVirtualHost *:80

NameVirtualHost *:443

Listen 80

<IfModule ssl_module>

    Listen 443

</IfModule>
```



Créer ensuite le fichier `/etc/apache2/sites-available/[NOM_DE_VOTRE_SRV].[DOMAINE.LAN]`

Si vous n’avez pas de domaine : `/etc/apache2/sites-available/[NOM_DE_VOTRE_SRV].conf`

**Si vous n’avez pas de domaine : omettez les directives " syslab.lan.**

Et ajoutez-y les lignes comme sur l’exemple, en l’adaptant à votre configuration (nom etc..) : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.093.png)


Puis vérifier la configuration et activez le site :

```bash
apache2ctl configtest
```


```css
a2ensite [NOM DE VOTRE SITE]
```


```bash
service apache2 reload
```


:::info

**Si vous avez une erreur par rapport au fichier " ports.conf ", vous pouvez supprimer les 2 lignes " NameVirtualHost * :80 et NameVirtualHost * :443 ".**
:::


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.094.png)

**Ajouter également la directive " ServerName localhost " dans le fichier /etc/apache2/apache2.conf si vous avez l’erreur ci-dessous (2) :**

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.095.png)


Vous pourrez maintenant vous connecter en https sur phpmyadmin.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.096.png)


### **Installation de plugins dans glpi**
Si votre GLPI à accès à internet, vous aurez la possibilité de configurer le store directement dans l’outils, en allant dans Configuration à Plugins, une fenêtre comme celle-ci s’ouvre : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.097.png)

En faisant Oui, vous serez connecté au market. Il faudra par contre vous inscrire.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.098.png)


:::danger attention

**Nous nous intéresserons ici à l’installation de plugins Hors Ligne.** 
:::


:::info To do

D’abord télécharger des plugins sur le site officiel des plugins GLPI : http://plugins.glpi-project.org/
:::


Puis les décompresser.


Il suffit ensuite de déplacer les dossiers décompressés dans le dossier des plugins de GLPI. `/var/www/glpi/plugins`.

Puis connectez-vous à l’interface GLPI, dans l’onglet Configuration à Plugins.

Cliquez ensuite sur l’icône " Installer " pour chaque plugin.

Puis sur l’icône " Activer " pour activer les plugins.


### **Configuration du plugin OCS-NG**

De la même manière que pour les autres plugins de GLPI, télécharger le plugin OCS Inventory sur le site des plugins GLPI ou via Github et installez le.

:::note Liens
- https://github.com/pluginsGLPI/ocsinventoryng/releases
- Version 2.0.4 : https://github.com/pluginsGLPI/ocsinventoryng/releases/download/2.0.4/glpi-ocsinventoryng-2.0.4.tar.bz2
:::


```bash
wget https://github.com/pluginsGLPI/ocsinventoryng/releases/download/1.7.0/glpi-ocsinventoryng-1.7.0.tar.gz 

tar xvf glpi-ocsinventoryng-1.7.0.tar.gz

cp -r ocsinventoryng /var/www/glpi/plugins/
```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.099.png)

Rendez-vous ensuite dans le menu outils à Ocs Inventory NG.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.100.png)

Installez-le et activez-le.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.101.png)

Cliquez alors sur le nom du serveur pour aller le configurer :

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.102.png)

Puis sur Serveurs OCSNG

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.103.png)

Cliquez sur le symbole + : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.104.png)

Lier maintenant le serveur OCS comme suit.

```markdown
Nom : Serveur OCS

Hôte : localhost

Base de données : ocsweb

Utilisateur : ocsbdd

Mot de passe : **** (celui du compte bdd)
```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.105.png)

:::caution

**ATTENTION : Un bug à été reporté dans les dernières versions de GLPI (9.5.X/ OCS1.7.X) . Un bug dans un fichier php empêche l’ajout du serveur OCS à GLPI (quand vous cliquerez sur Ajouter, rien ne se passera). Cette procédure vous permettra de pallier le problème.** 
:::

En effet le problème vient du fichier `/var/www/glpi/plugins/ocsinventoryng/inc/ocsserver.class.php`. 

Vous pourrez vous rendre compte de l’erreur en visualisant le fichier de log après le premier essai : 

```bash
cat /var/www/glpi/files/_log/php-errors.log
```


` `![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.106.png)

Si vous avez cette erreur vous allez devoir modifier à la main quelques lignes du fichier `ocsserver.class.php.`

Pour cela, commencez par sauvegarder le fichier initial :

```bash
cp /var/www/glpi/plugins/ocsinventoryng/inc/ocsserver.class.php /var/www/glpi/plugins/ocsinventoryng/inc/ocsserver.class.php.old
```


Puis nous allons devoir faire ce que l’on appelle le Rollback, en se référant à un ancien fichier. Il faudra pour résumer remplacer des lignes par d’autres.

Source : https://github.com/pluginsGLPI/ocsinventoryng/commit/5c157b1c4c2192da3bd890be0798ed32533734ee

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.107.png)

Ici nous voyons une partie de notre fichier `/var/www/glpi/plugins/ocsinventoryng/inc/ocsserver.class.php`

Surligné en vert correspond à la nouvelle version, et donc cela correspondra avec ce que vous avez sur votre machine.

Surligné en rouge correspond à l’ancienne version. 

Notre travail sera de remplacer les lignes surligné en vert (donc celle présentes dans votre fichier), par les lignes surlignés en rouge. Vous pouvez voir à quel numéro de ligne elles sont sur la capture.

:::caution

**ATTENTION : NE TOUCHEZ PAS A LA LIGNE 54. (cela peut changer en fonction des versions)**
:::


Exemple : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.108.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.109.png)

- Ligne remplacée (donc en rouge)
- Ligne initiale (en vert)
- Ligne remplacée
- Ligne initiale

Une fois les modifications effectuées, vous pourrez retenter l’ajout du serveur OCS via le plugin dans GLPI, et tout devrait fonctionner.

Une fois OCS lié, aller Outils à OCS Inventory NG et configurez le serveur.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.110.png)

Nous allons ensuite automatiser le script de synchronisation grâce à un crontab.

D’abord mettez les bonnes autorisations pour éxexuter le fichier :

```bash
chmod 764 /var/www/glpi/plugins/ocsinventoryng/scripts/ocsng_fullsync.sh
```

```bash
crontab -e
```

Puis ajouter la ligne : 

```bash
*/5 * * * * /var/www/glpi/plugins/ocsinventoryng/scripts/ocsng_fullsync.sh --thread_nbr=2 --server_id=1
```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.111.png)

### **Configurations annexes**

Dans l’onglet Outils à OCS Inventory NG, cliquez sur *Configuration du serveur OCSNG : ocs*
### **Données à importer**
![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.112.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.113.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.114.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.115.png)

### **Options d’importation**

Configurez les options d’importations comme suit : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.116.png)

### **Historique général**
![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.117.png)

## **CONFIGURATIONS**
### **Configurations de GLPI** 
### **Connexion au LDAP**

Installer les dépendances nécessaires. Puis restart le service apache.

```bash
apt-get install php-ldap
```


Puis dans l’onglet Configuration à Authentification. Cliquez sur configuration.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.118.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.119.png)

Réglez les premiers paramètres. Puis allez dans l’onglet Configuration à Annuaires LDAP puis cliquez sur le petit bouton ![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.120.png)

Pour le domaine, les réglages sont les suivants.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.121.png)

:::tip Exemple

Nom : *syslab.lan*

Serveur par défaut : *Oui*

Serveur : *192.168.1.115*

Filtre de connexion : *(&(objectClass=user)(objectCategory=person)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))*

*BaseDN : DC=syslab,DC=lan*

DN du compte : *CN=Administrator,OU=users,DC=syslab,DC=lan (J’utilise ici le compte admin mais la bonne pratique voudrait qu’on utilise un compte de service dédié)*

Mot de passe : *Celui du compte Admin du domaine*

Champ de l’identifiant : *samaccountname*
:::


Vous pouvez ensuite aller dans l’onglet " Tester " pour vérifier que la connexion est effective.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.122.png)


Vous pouvez maintenant ajouter des utilisateurs de votre domaine pour la connexion à l’interface GLPI.

### **Règles d’import et de liaison des ordinateurs.**

Cliquez sur règle d’import et de liaison des ordinateurs.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.123.png)

Puis RootComputerOCS.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.124.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.125.png)

Ajoutez un critère OCS.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.126.png)


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.127.png)

### **Configurations d’OCS**
### **Onglet Inventory**
![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.128.png)

### **Onglet Server**
![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.129.png)

Attention à bien cocher uniquement Serial dans AUTO_DUPLICATE_LVL pour éviter les doublons. Il ne doit exister qu’un seul serial dans la base.

### **Création de l’agent Windows OCS**
:::info
L’agent OCS sera installé sur toutes les machines du parc cet permettra de faire remonter les informations de l’ordinateur dans la base de données. Afin de créer l’agent il nous faut télécharger le fichier OCS Inventory NG Windows Agent: https://github.com/OCSInventory-NG/WindowsAgent/releases/
:::


Puis éxécuter le fichier OCS-NG-Windows-Agent-Setup.exe.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.130.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.131.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.132.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.133.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.134.png) 

Dans Server URL mettez l’adresse **http://IP-GLPI/ocsinventory/.**

Dans Certificate File : Laissez **cacert.pm**.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.135.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.136.png)

Ajoutez en TAG " WINDOWS_SERVER". Cela catégorisera les machines dans OCS.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.137.png)

Puis cliquez sur Installer.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.138.png)

L’agent est maintenant prêt à être déployé.

### **Configuration de l’agent OCS Linux/Unix**

:::info
L’agent OCS sera installé sur toutes les machines du parc cet permettra de faire remonter les informations de l’ordinateur dans la base de données. Afin de créer l’agent il nous faut télécharger le fichier OCS Inventory NG Linux/Unix Agent: https://github.com/OCSInventory-NG/UnixAgent/releases/tag/v2.8.0
:::


```bash
wget https://github.com/OCSInventory-NG/UnixAgent/releases/download/v2.8.0/Ocsinventory-Unix-Agent-2.8.0.tar.gz
```

Décompressez le fichier, puis aller à l’intérieur :

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.139.png)

Une fois dans le dossier, tapez pour compiler le programme et repondez aux questions comme suit : 

```go
perl Makefile.PL

make

make install
```


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.140.png)

L’agent est installé.

Nous pouvons vérifier la configuration : 
```bash
vim /etc/ocsinventory-agent/ocsinventory-agent.cfg
```
```bash
snmp=1

tag=LINUX_SRV

server=http://192.168.1.155/ocsinventory

ssl=0

debug=1

basevardir=/var/lib/ocsinventory-agent

logfile=/var/log/ocsagent.log
```

Lancer ensuite l’agent avec la commande : 

```bash
ocsinventory-agent
```


### **Résultats**

Nous pouvons maintenant voir nos machines dans OCS :

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.141.png)

En forçant la synchro avec GLPI (importation de nouveaux ordianteurs) : 

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.142.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.143.png)

Je vois que je peux importer mon ordinateur, je clique sur Importer.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.144.png)

Tout est là.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.145.png)

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.146.png)

### **Sauvegarde de la Base de donnée**
Créer un dossier scripts dans /root/.

Puis un dossier backups.

Et deux fichiers de scripts avec les droits d’exécution.
```bash

mkdir /root/scripts

mkdir /root/scripts/backups

touch /root/scripts/backup_bdd.sh

touch /root/scripts/clean_backups.sh

chmod +x /root/scripts/backup_bdd.sh

chmod +x /root/scripts/clean_backups.sh

```

Script : backup_bdd.ssh pour sauvegarder la base GLPI. Le principe est le même pour OCS et autres BDD.
```bash
#!/bin/bash
#
### on se place dans le repertoire ou l'on veut sauvegarder les bases
#

cd /root/scripts/backups/

for i in dbglpi; do

### Sauvegarde des bases de donnees en fichiers .sql - VERIFIER LA COMMANDE DIRECTEMENT SUR VOTRE MACHINE POUR VERIFIER SON FONCTIONNEMENT

mysqldump -uglpi -p password ${i} ${i}_`date +"%Y-%m-%d"`.sql

### Compression des exports en tar.bz2 (le meilleur taux de compression)

tar jcf ${i}_`date +"%Y-%m-%d"`.sql.tar.bz2 ${i}_`date +"%Y-%m-%d"`.sql

### Suppression des exports non compresses

rm ${i}_`date +"%Y-%m-%d"`.sql

done
```

Script : clean_backup.ssh
```bash
#!/bin/bash
#
### Supprime les sauvegardes vieilles de plus de 5 jours
#

find /root/scripts/backups/ -type f -mtime +6 -delete
```


## **ACTIVATION DU SSL**

La sécurisation des connexions est obligatoire pour le déploiement, on va voir très rapidement ici comment l’activer sur notre apache, à l’aide d’un certificat SSL auto-signé.

Créez un dossier pour stocker le certificat :

```bash
mkdir /etc/apache2/ssl
```


Installez openssl si ce n’est déjà le cas :

```bash
apt-get install openssl
```


Générez la clé RSA (nommez le fichier comme avec le même nom que votre serveur) :

```bash
openssl genrsa 1024 /etc/apache2/ssl/srv-glpi-deb01.key
```

Créez le certificat (nommez le fichier comme vous le souhaitez) :

```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -sha256 -out /etc/apache2/ssl/srv-glpi-deb01.crt -keyout /etc/apache2/ssl/srv-glpi-deb01.key
```


:::caution

- Entrez alors les informations demandées et à la question **Common Name (e.g. server FQDN or YOUR name)**, mettez **LE NOM d’hôte du serveur**, soit **gestparc** pour moi. **C’EST TRES IMPORTANT !** Lors de la configuration de l’agent, on spécifiera le nom d’hôte du serveur, et le certificat doit être en concordance.
:::

- Pour vérifier le contenu de votre certificat : 

```bash
openssl x509 -in certificate.crt -text -noout
```


Éditez ensuite le fichier suivant :

```bash
vim /etc/apache2/sites-available/default-ssl.conf
```


Trouvez ces directives et remplacez les valeurs par le chemin vers le certificat :

```bash
SSLCertificateFile /etc/apache2/ssl/srv-glpi-deb01.crt
SSLCertificateKeyFile /etc/apache2/ssl/srv-glpi-deb01.key

```

Activer SSL et le fichier de conf :

```bash
a2enmod ssl

a2ensite default-ssl
```

- Redémarrer apache :

```bash
service apache2 restart
```


:::caution

**Si vous avez une erreur par rapport au fichier " ports.conf ", vous pouvez supprimer les 2 lignes " NameVirtualHost * :80 et NameVirtualHost * :443 ".**
:::


![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.094.png)

:::caution

**Ajouter également la directive " ServerName localhost " dans le fichier /etc/apache2/apache2.conf si vous avez l’erreur ci-dessous (2) :**

:::

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.095.png)

- Enfin, testez de vous connecter en SSL sur votre serveur : `https://IP_GLPI/glpi`

-Vous aurez un message dans le navigateur, cela vient du fait que le certificat est auto-signé.


:::tip 
Gardez le fichier de certificat (**.crt**) sur une clé usb ou autre pour plus tard.
:::

Le serveur est sécurisé.

![](Aspose.Words.25ba1c8a-87cc-4c30-a100-130044463f20.147.png)

:::caution

Attention les paramètres de l’installation de l’agent vont changer.
:::


:::info Exercice

- Exercice : Déployer un agent maintenant que le SSL est activé.

- Retrouvez le **.crt** que vous avez créé précédemment, puis copiez le dans C:\ProgramData\OCS Inventory NG\Agent\ (sous Windows). 

- Renommez le ensuite en **cacert.pm**

- Redémarrez ensuite le service Windows :

- Tapez **WindowsKey+R**, entrez **services.msc** puis **Entrée** :

- Dans la fenêtre des services, trouvez **OCS Inventory Service** puis faites un clic-droit et **Arrêter**. Ne fermer pas cette fenêtre ensuite :

- Copiez alors votre fichier cacert.pem vers le dossier **C:\ProgramData\OCS Inventory NG\Agent\ si ce n’est pas déjà fait**.

- Attention, le dossier ProgramData est caché par défaut.
:::

### **Si l’inventaire ne remonte pas :**
 

Editez le fichier `C:\ProgramData\OCS Inventory NG\Agent\ocsinventory.ini` avec notepad par exemple. Attention, sous Windows 10, le notepad doit être lancé en administrateur.

Trouvez la directive `TTO_WAIT= et mettez la valeur 10` :

**Enregistrez le fichier**. 

:::note

Notez cependant que vous pouvez apporter des modifications à la configuration de l’agent dans ce fichier. Pensez alors à arrêter le service d’abord puis le relancer ensuite pour que les modifications prennent effet. La directive TTO_WAIT indique que l’inventaire s’exécutera 10 secondes après le démarrage du service. Une fois l’inventaire effectué, la valeur TTO_WAIT va changer toute seule. Elle représente alors le délai avant le prochain inventaire.
:::

Retournez dans les services et démarrez **OCS Inventory Service**.

## **ON S’EXERCE ?**

Maintenant que votre infrastructure est en place, à vous d’appliquer différentes configurations et de faire vos tests.

A faire sur votre serveur GLPI : 

:::info Exerice
- Ajouter un profil " client " (interface simplifié)
  - Le profil client devra avoir seulement le droit de créer des tickets
- Ajouter un utilisateur Jean Dupont, avec le profil " client ".
- Créer un profil " Admin_sys " (interface standard)
  - Il devra avoir tous les droits SAUF :
    - L’accès à l’onglet configuration et tous son contenu
    - Les droits sur Fusion Inventory
    - Les droits sur OCSNG
- Créer un utilisateur " George Bush " avec le profil Admin_sys
- Supprimer les profils " Hotliner ", " Observer ", " Read-Only ".
- Créer un gabarit pour les ordinateurs : 
  - Nom du gabarit : LINUX
  - Nom : lnx-
  - Lieu : Paris
  - Responsable technique : George Bush
  - Groupe technique : Admin_linux
  - Utilisateur : Dupont Jean
  - Groupe : Admin_linux
  - Statut : En service
  - Type : Debian
  - Fabricant : DELL
- Créer un gabarit pour les ordinateurs : 
  - Nom du gabarit : WINDOWS
  - Nom : win-
  - Lieu : Marseille
  - Responsable technique : George Bush
  - Groupe technique : Admin_windows
  - Utilisateur : Dupont Jean
  - Groupe : Admin_windows
  - Statut : Hors service
  - Fabricant : ASUS
- Créer un ordinateur avec le gabarit Linux et un avec le Windows 
  - Nom pour l’ordinateur Linux : lnx-machine01
  - Nom pour le Windows : win-machine02
- Créer un ticket " Problème d’imprimante " (depuis l’utilisateur Dupont)
  - Demandeur : Jean Dupont
  - Observateur : GLPI
  - Attribué : tech
  - Statut : En cours (attribué)
  - Urgence : Haute
  - Lieu : Paris
  - Eléments associés : Le pc linux créé précédemment
- Ajouter le plugin TAG dans GLPI : https://plugins.glpi-project.org/#/plugin/tag
- Dans les règles d’imports et de liaison des ordinateurs d’OCS
  - Ajouter Adresse IP n’existe pas
:::