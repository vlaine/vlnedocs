Nagios 
---
## Pré-requis

- Machine : Debian 11 vierge
	- Optionnel : Machine déployée avec Vagrant : https://app.vagrantup.com/generic/boxes/debian11 

## Installation 
Mise à jour et installation des dépendances 

Pensez à utiliser le compte root pour plus de praticité.
Plusieurs commandes à votre disposition : 
```
su - # Switch user root NE PAS OUBLIER LE "-" car cela fourni un nouvel environnement propre, avec les variables d'environnement de root, et donc toutes les commandes nécessaires disponibles
sudo su -
sudo -s #ouvre un shell interactif en tant que superutilisateur (root), en conservant votre environnement actuel.
sudo -i #ouvre un shell interactif en tant que superutilisateur (root), en fournissant un nouvel environnement propre, y compris un répertoire courant différent.
```


```sql
apt update && apt upgrade
```

```rust
apt install vim wget curl build-essential unzip openssl libssl-dev apache2 php libapache2-mod-php php-gd libgd-dev
```

Puis aller chercher les sources :

```bash
NAGIOS_VER=$(curl -s https://api.github.com/repos/NagiosEnterprises/nagioscore/releases/latest|grep tag_name | cut -d '"' -f 4)
```


```bash
wget https://github.com/NagiosEnterprises/nagioscore/releases/download/$NAGIOS_VER/$NAGIOS_VER.tar.gz
```

Extraire l'archive avec tar xvf puis compiler les sources (installer d'abord build-essential):

```bash
tar xvf nagios-4.4.13.tar.gz
cd nagios-4.4.13/
./configure --with-httpd-conf=/etc/apache2/sites-enabled
```

![img/Pasted image 20230712171708.png](img/Pasted%20image%2020230712171708.png)

Création des utilisateurs et groupes
```bash
make install-groups-users
usermod -a -G nagios www-data
```

Puis compiler et installer le programme
```go
make all
make install
```

Installe les fichiers du démon Nagios et les configure également pour qu'ils démarrent au démarrage du système.
```go
make install-daemoninit
```

Installe et configure le fichier de commande externe.
```go
make install-commandmode
```

Dans cette étape, nous installons les fichiers de configuration SAMPLE requis car Nagios a besoin de certains fichiers de configuration pour lui permettre de démarrer.
```go
make install-config
```

Configurer Apache pour les pages Web de Nagios. Les développeurs de Nagios ont facilité la configuration d'Apache. Vous exécutez simplement une commande pour configurer les fichiers de configuration, puis activez des modules apache spécifiques. 
```go
make install-webconf
a2enmod rewrite cgi
systemctl restart apache2
```


### Protéger l'interface

Afin de créer l'authentification Web Nagios, vous devez créer un utilisateur Web pour l'authentification. Avec la commande 'htpasswd'.  Veuillez noter que Nagios utilise l'utilisateur 'nagiosadmin' par défaut. Exécutez la commande ci-dessous et entrez votre mot de passe favorable.

```bash
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
chown www-data:www-data /usr/local/nagios/etc/htpasswd.users
chmod 640 /usr/local/nagios/etc/htpasswd.users
```

## Installation des plugins

Avant de terminer la configuration de Nagios, nous avons besoin de plugins Nagios qui nous aideront à accomplir beaucoup de choses, y compris la surveillance de l'hôte local. 
Nous allons donc récupérer la dernière version stable des plugins, l'extraire et l'installer dans notre système. Vous pouvez trouver les plugins sur Nagios Plugins. 

```bash
VER=$(curl -s https://api.github.com/repos/nagios-plugins/nagios-plugins/releases/latest|grep tag_name | cut -d '"' -f 4|sed 's/release-//')
```
![img/Pasted image 20230712172257.png](img/Pasted%20image%2020230712172257.png)

```bash
wget https://github.com/nagios-plugins/nagios-plugins/releases/download/release-$VER/nagios-plugins-$VER.tar.gz
```

```bash
tar xvf nagios-plugins-$VER.tar.gz
cd nagios-plugins-$VER
./configure --with-nagios-user=nagios --with-nagios-group=nagios
make
make install
```

![img/Pasted image 20230712172513.png](img/Pasted%20image%2020230712172513.png)

## Les fichiers de Nagios 

- Accès à l'interface web
	- `http://<IP Address/FQDN>/nagios`
	- nagiosadmin puis le mot de passe setup à l'étape précédente

![img/Pasted image 20230712172711.png](img/Pasted%20image%2020230712172711.png)
![img/Pasted image 20230712173052.png](img/Pasted%20image%2020230712173052.png)
Répertoires d'installation :
```bash
/usr/local/nagios/ 
- Configurations de base de nagios :
/usr/local/nagios/etc/nagios.cfg
```

```
root@debian11.localdomain /usr/local/nagios # tree -L 1
.
├── bin
├── etc
├── libexec
├── sbin
├── share
└── var
```
![img/Pasted image 20230712173000.png](img/Pasted%20image%2020230712173000.png)


```
ps -ef
```
![img/Pasted image 20230712165743.png](img/Pasted%20image%2020230712165743.png)

###### Syntaxe simple de nagios.cfg
- Toutes les lignes ont la forme `paramètre=valeur`
- Dans certains cas, une valeur peut être répétée plusieurs fois
- Par exemple, spécifier plusieurs fichiers ou répertoires à lire
- La première option est nécessairement celle qui définit le fichier de log

Configuration des objets :	
```bash
/usr/local/nagios/etc/objects
```

Configurations de base de `nagios.cfg` :

/usr/local/nagios/etc/nagios.cfg
```diff
- log_file
- cfg_file
- cfg_dir
- resource_file
- temp_file
- lock_file
- temp_path
- status_file
- status_update_interval
- nagios_user
- nagios_group
- command_file
- use_syslog
- state_retention_file
- retention_update_ interval
- service_check_timeout
- host_check_timeout
- event_handler_timeout
- notification_timeout
- enable_environment_ macros
- interval_length
```

Configurations de base de nagios :
```bash
/usr/local/nagios/etc/nagios.cfg
```

- Le fichier indiqué dans l'option resource_file permet de stocker des variables utilisateurs
- Ce fichier peut être utilisé pour stocker des informations additionnelles qui peuvent être accédées dans toutes les définitions d'objet.
- Il contient généralement les informations sensibles (login /mot de passe).
- Ce fichier ne peut être consulté depuis l'interface Web
- Il peut y avoir jusqu'à 32 macros `($USER1$, $USER2$, ..., $USER32$)`

:::tip 
> - Par exemple `$USER1$` contient le chemin vers les plugins nagios et est utilisé dans la définition des commandes de vérification.
> 	- Donc `$USER1$` = /usr/local/nagios/libexec/
> Cela sera important pour la configuration des commandes plus tard.
> 
:::

## Organisation des répertoires 

- Les objets peuvent être structurés de différentes manières
	- On peut imaginer un répertoire qui contient tous les objets d'un même site
	- On peut imaginer un répertoire qui contient tous les objets d'un même type
- Structure que nous allons utiliser
	- Un répertoire pour chaque type d'objet
	- Les objets similaires seront groupés dans un même fichier

![img/Pasted image 20230712170111.png](img/Pasted%20image%2020230712170111.png)

- Editer le nagios.conf et supprimer toutes les entrées `cfg_file` et `cfg_dir`
- Indiquer les valeurs pour faire la séparation par dossier des tous les objets. Exemple : 

![img/Pasted image 20230712170212.png](img/Pasted%20image%2020230712170212.png)
(Allé je suis sympa)
``` 
cfg_dir=/usr/local/nagios/etc/objects/commands
cfg_dir=/usr/local/nagios/etc/objects/timeperiods
cfg_dir=/usr/local/nagios/etc/objects/contacts
cfg_dir=/usr/local/nagios/etc/objects/contactgroups
cfg_dir=/usr/local/nagios/etc/objects/hosts
cfg_dir=/usr/local/nagios/etc/objects/hostgroups
cfg_dir=/usr/local/nagios/etc/objects/services
cfg_dir=/usr/local/nagios/etc/objects/servicegroups
cfg_dir=/usr/local/nagios/etc/objects/templates
```

- Pour utiliser les plugins par défaut de nagios, il faut copier le fichier de définition de commandes dans le nouveau répertoire prévu à cet effet. 
- Pareil pour les templates et autres fichiers de base fourni à la racine de objects. 
- N'oubliez pas également de créer les dossiers.

![img/Pasted image 20230712174138.png](img/Pasted%20image%2020230712174138.png)

#### Quelques vérifications : 

Vérifier les valeurs suivantes dans `nagios.cfg` pour le bon fonctionnement des plugins

```ini
check_external_commands=1
interval_length=60
accept_passive_service_checks=1
accept_passive_host_checks=1
```

#### Installer SNMP sur le serveur Nagios


```shell
apt install snmp snmpd snmptrapd nagios-snmp-plugins
```

- https://nagios-plugins.org/download/nagios-plugins-2.3.3.tar.gz 
	- le plugin check_snmp est disponible dans cette suite de plugins
- https://exchange.nagios.org/directory/Plugins/Network-Protocols/SNMP/check_snmp/details 
Utilisez la même méthode d'installation que pour les plugins du départ.
:::note 
> - SNMPv1
> 	- Défini dans les RFC 1155, 1156 et 1157
> 	- Fonctionnement très simple
> 	- SNMPv1 se révèle trop limité pour les applications actuelles
> 
>  - SNMPv2c
> 	- Sous-version de SNMPv2
> 	- Est compatible avec SNMPv1
> 	- Ajoute la commande Inform (accusée en réception contrairement aux messages Traps)
> 	- Gestion améliorée des erreurs
> 	- Commandes SET améliorées
> 
> - SNMPv3
> 	- Utilisation d'un Engine-ID pour identifier de façon unique un équipement
> 	- Sécurité (authentification et cryptage)
> 	- Authentification et vérification de l'intégrité par HMAC et clé
> 	- Fonctionnalité anti–replay
> 	- Cryptage DES
:::

Il existe aussi des plugins en Perl pour SNMP
Il faut installer le package Perl Net::SNMP (CPAN)
```sql
apt-get install libnet-snmp-perl
```

- check_ifstatus : vérifie le statut de toutes les interfaces (retourne critical si une seule des interfaces est down).
- check_ifoperstatus : permet de spécifier quelle interface on souhaite vérifier
```undefined
apt install libnet-snmp-perl
```

![img/Pasted image 20230712180029.png](img/Pasted%20image%2020230712180029.png)
![img/Pasted image 20230712180036.png](img/Pasted%20image%2020230712180036.png)

# Installer l'agent SNMP pour les clients Linux

Sur Linux : 
```sql
apt-get install snmpd 
```

Modifier le fichier `/etc/snmp/snmpd.conf`
`agentAddress udp:161` pour écouter sur tous les IP de la machine (et non pas seulement le localhost)
`rocommunity public 0.0.0.0/0` à accès depuis n'importe quelle machine.
![img/Pasted image 20230712180952.png](img/Pasted%20image%2020230712180952.png)

Puis ajouter un host :

`nagios-client.cfg`
```
define host {
    use                     linux-server            ; Name of host template to use
    host_name               nagios-client
    alias                   Nagios Client
    address                 192.168.45.148
}
```

Ajouter un fichier de service : 
`linux-services.cfg`
```
define service {

    use                     local-service           ; Name of service template to use
    host_name               nagios-client
    service_description     PING
    check_command           check_ping!100.0,20%!500.0,60%
}



# Define a service to check the disk space of the root partition
# on the local machine.  Warning if < 20% free, critical if
# < 10% free space on partition.

define service {

    use                     local-service           ; Name of service template to use
    host_name               nagios-client
    service_description     Root Partition
    check_command           check_local_disk!20%!10%!/
}

```

Redémarrez Nagios service puis vérifier sur votre interface web :
![img/Pasted image 20230712182706.png](img/Pasted%20image%2020230712182706.png)

# Installer l'agent SNMP pour les clients Windows
Sur Microsoft Windows - Installer le composant SNMP dans les features.

![img/Pasted image 20230307122551.png](img/Pasted%20image%2020230307122551.png)
![img/Pasted image 20230712185629.png](img/Pasted%20image%2020230712185629.png)
![img/Pasted image 20230712185701.png](img/Pasted%20image%2020230712185701.png)
![img/Pasted image 20230712185645.png](img/Pasted%20image%2020230712185645.png)


## NRPE et check_nt pour Windows

Télécharger : [NSCP-0.5.3.4-x64.msi](https://github.com/mickem/nscp/releases/download/0.5.3.4/NSCP-0.5.3.4-x64.msi)

![img/Pasted image 20230712184448.png](img/Pasted%20image%2020230712184448.png)
Notez que le service NSClient (nscp) sera automatiquement lancé après l'installation. Il établira également une règle de pare-feu pour lui permettre l'accès. Notez également que le service nscp écoute sur le port ==12489.==

Dossier de conf : `C:\Program Files\NSClient++`

Changer le nsclient.ini

```makefile
# If you want to fill this file with all available options run the following command:
#   nscp settings --generate --add-defaults --load-all
# If you want to activate a module and bring in all its options use:
#   nscp settings --activate-module <MODULE NAME> --add-defaults
# For details run: nscp settings --help


; in flight - TODO
[/settings/default]

; Undocumented key
password = eqZhKVDbm36tgBZ9

; Undocumented key
allowed hosts = 127.0.0.1,192.168.10.19,192.168.1.40


; in flight - TODO
[/settings/NRPE/server]
allow arguments = true
allow nasty characters = true
insecure = true
verify mode = none
dh = ${certificate-path}/nrpe_dh_2048.pem

; in flight - TODO
[/modules]

; Undocumented key
CheckExternalScripts = enabled

; Undocumented key
CheckHelpers = enabled

; Undocumented key
CheckNSCP = enabled

; Undocumented key
CheckDisk = enabled

; Undocumented key
WEBServer = enabled

; Undocumented key
CheckSystem = enabled

; Undocumented key
NSClientServer = enabled

; Undocumented key
CheckEventLog = enabled

; Undocumented key
NSCAClient = enabled

; Undocumented key
NRPEServer = enabled

 [/settings/external scripts] 
allow arguments = true
allow nasty characters = true



```


Puis restart de le service :

![img/Pasted image 20230307124338.png](img/Pasted%20image%2020230307124338.png)

En powershell

```powershell
net stop nscp
net start nscp
```

##### Vérifier check_nt : 
```
./check_nt -H 192.168.10.59 -p 12489 -s eqZhKVDbm36tgBZ9 -v CPULOAD -l 5,80,90

Charge CPU 0% (5 moyenne minimale) |   '5 Charge moyenne minimale'=0%;80;90;0;100
```
![img/Pasted image 20230712185121.png](img/Pasted%20image%2020230712185121.png)


#### Vérifier check_nrpe
```
./check_nrpe -H 192.168.10.59
```

:::danger Error 
> CHECK_NRPE: (ssl_err != 5) Error - Could not complete SSL handshake with 192.168.10.59: 1
:::

Probleme de certificat, il faut en régénérer un : 

```
openssl dhparam -C 2048
```

Et garder : 

```
-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEA1HcA9daz1V/N6vRtTECzHYZM3/xObdkvxrkzkmeARGSiQUX4MVv0
6IvyRJ5u4qmBVdxq51ZzqziyCXzXIKbVLkwqJy8BDZQpZZJPIcKxr36PKlG9d5iW
pPGEBoKrsm1UCKXOuoEK4VSebWZDZXChbEgkn0CpvjsnmLngXL7m5NuSrFVZYvYw
ojyzix10+CgLFnb91KGVse3W7tv3kRtftGoW/voiM6aEuHucScmiVB6WJEjnXuVC
8Uy9U8xY82zHd0hQvEnosY5uUf1/Ens/Wm61P6C4jrC64PY/XnAxAeULuyHFr1ur
Qz695KDDvVlqDp8ahAPVS1kgAe3uO+TBuwIBAg==
-----END DH PARAMETERS-----
```

Créer ensuite un fichier `nrpe_dh_2048.pem` et mettez le dans `C:\Program Files\NSClient++\security` sur le client.

Ajouter la ligne dans nsclient.ini : 

```
[/settings/NRPE/server]
dh = ${certificate-path}/nrpe_dh_2048.pem
```

Puis restart de le service :

![img/Pasted image 20230307124338.png](img/Pasted%20image%2020230307124338.png)

Re tester :
:::danger Error
> 
> CHECK_NRPE: Invalid packet version received from server.
> I (0.5.2.41 2018-04-26) seem to be doing fine...
> 
:::

NRPE -c command à tester 
```
2023-03-07 14:29:12: error:c:\source\0.5.2\service\plugin_manager.cpp:475: Unknown command(s): checknet available commands: commands {, alias_cpu, alias_cpu_ex, alias_disk, alias_disk_loose, alias_event_log, alias_file_age, alias_file_size, alias_mem, alias_process, alias_process_count, alias_process_hung, alias_process_stopped, alias_sched_all, alias_sched_long, alias_sched_task, alias_service, alias_service_ex, alias_up, alias_volumes, alias_volumes_loose, check_always_critical, check_always_ok, check_always_warning, check_and_forward, check_counter, check_cpu, check_critical, check_drivesize, check_eventlog, check_files, check_memory, check_multi, check_negate, check_network, check_nscp, check_nscp_version, check_ok, check_os_version, check_pagefile, check_pdh, check_process, check_service, check_timeout, check_uptime, check_version, check_warning, checkalwayscritical, checkalwaysok, checkalwayswarning, checkcounter, checkcpu, checkcritical, checkdrivesize, checkeventlog, checkfiles, checkmem, checkmultiple, checkok, checkprocstate, checkservicestate, checkuptime, checkversion, checkwarning, filter_perf, negate, render_perf, submit_nsca, timeout, xform_perf}, plugins {, 0, 1, 2, 3, 4, 5, 7}

```

Après tests, il pourrait y avoir cette erreur : 
```
./check_nrpe -H 192.168.10.59 -c alias_cpu -3 -D
```

:::danger Error 
> CHECK_NRPE: Invalid packet version received from server.
> OK: CPU load is ok.|'total 5m'=1%;80;90 'total 1m'=0%;80;90 'total 5s'=0%;80;90
> 
:::

On peux tester en rajouteant un payload : 
:::note 
> 
> When NSClient++ has the payload length setting defined, the check_nrpe plugin requires the arguments -2 -P xxxx where xxxx is the value defined for payload length.
:::

```
./check_nrpe -2 -P 1024 -H 192.168.10.59 -c alias_cpu
OK: CPU load is ok.|'total 5m'=1%;80;90 'total 1m'=1%;80;90 'total 5s'=0%;80;90
```

Le message disparait. Pensez à add l'option -2 en tant que workaround.
https://www.claudiokuenzler.com/blog/1115/check_nrpe-4.x-nsclient-5.x-ssl-error-could-not-complete-ssl-handshake


# Les objets - Samples

- commands.cfg
```bash
define command {
    command_name    check_http
    command_line    $USER1$/check_http -I $HOSTADDRESS$ $ARG1$
}
```

- service.cfg
```
define service{
    use                    generic-service
    host_name             web1
    hostgroup_name        web-servers
    service_description     www
    check_command         check_http!opts
    servicegroups           web
 }
```

- host.cfg
```
define host{
    use             linux-server
    host_name        web1
    address         192.168.10.50
    check_command  check-host-alive
    hostgroups     web-servers
}
```

- hostgroup.cfg
```
define hostgroup{
    hostgroup_name     web-servers
    alias                serveursweb
    members             web1,web2,web3
}
```

- servicegroup.cfg
```
define servicegroup{
     servgicegroup_name    web
     alias                  www
     members               web1,www
 }
```

- template_host.cfg
- template_service.cfg
```
define host {

    name                         linux-server
    use                           generic-host
    check_period                  24x7
    check_interval                 5
    retry_interval                 1
    max_check_attempts         10
    check_command                check-host-alive
    notification_period             workhours
    notification_interval           120
    notification_options            d,u,r
    contact_groups                 admins
    register                        0
}
define service {

    name                          generic-service
    check_period                  24x7
    [...]
    notification_options            w,u,c,r
    notification_interval           60
    notification_period            24x7
    contact_groups               admins
}

```

- timeperiod.cfg
```
define timeperiod {

    name                    24x7
    timeperiod_name         24x7
    alias                   24 Hours A Day, 7 Days A Week

    sunday                  00:00-24:00
    monday                  00:00-24:00
    tuesday                 00:00-24:00
    wednesday               00:00-24:00
    thursday                00:00-24:00
    friday                  00:00-24:00
    saturday                00:00-24:00
}

```

contact.cfg
```
define contact {

    contact_name           nagiosadmin
    use                     generic-contact
    alias                   Nagios Admin
    email                   nagios@localhost
}
```

contactgroup.cfg
```
define contactgroup {

    contactgroup_name       admins
    alias                   Nagios Administrators
    members                 nagiosadmin
}
```
