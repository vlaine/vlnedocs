# WDS - MDT 
---
En lient avec [[Active Directory](../Active%20Directory/Active%20Directory.md)
# Troubleshooting

:::danger 
> 
> Le service n'a pas répondu assez vite à la demande de lancement ou de contrôle.
:::


Link : https://social.technet.microsoft.com/Forums/windowsserver/en-US/33a30355-c50d-49fb-a6b3-d55ada75e704/wds-et-dhcp-spars-impossible-de-dmarrer-le-service?forum=windowsserver8fr

:::success 
> 
> - Bug lors de l'installation de Windows Deployment Services 2012.
> 
> - Environnement : 
> 
> - Windows Serveur 2012 R2 Std /  French
> 
> - installé dans une machine virtuelle en environnement ESX
> 
> - Serveur WDS installé sur un serveur différent du Serveur DHCP du domaine.
> 
> - Nouvelle installation de WDS (pas de serveur existant sur le réseau).
> ad-summary
title: Description
:::

- Description du problème : 

- Juste après l'installation du rôle, lors de la configuration initiale du serveur pour être en mode AD integrated, les services WDS ne démarrent pas.

- En utilisant les commandes WDSUTIL pour l'initialisation, le problème est toujours présent.     **CODES ERREURS : 768 - 261 - 264 - 268 - 513 - 257** 
```

- Voici les actions à mener pour résoudre ce problème : 

- *Le problème vient d'une valeur de clef de registre qui ne se configure pas correctement, provoquant une insuffisance de droits pour le démarrage des services WDS.

- **[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WDSServer\Providers\WDSPXE\Providers\BINLSVC\AutoApprove\arm]**

- *Modifier la valeur de la clef User

- *Saisir « **Administrateurs du domaine** ».

- **[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WDSServer\Providers\WDSPXE\Providers\BINLSVC\AutoApprove\ia64]**

- *Modifier la valeur de la clef User

- *Saisir « **Administrateurs du domaine** ».

- **[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WDSServer\Providers\WDSPXE\Providers\BINLSVC\AutoApprove\x64]**

- *Modifier la valeur de la clef User

- *Saisir « **Administrateurs du domaine** ».

- **[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WDSServer\Providers\WDSPXE\Providers\BINLSVC\AutoApprove\x86]**

- *Modifier la valeur de la clef User

- *Saisir « **Administrateurs du domaine** ».

- Redémarrez ensuite votre serveur WDS pour que les autorisations soient prises en compte.
- Le serveur devrait démarrer normalement et les services aussi.


```

Si ce n'est pas efficace, tester en CMD (pour erreur 257):
![Pasted image 20230223111308.png](img/Pasted%20image%2020230223111308.png)

:::note 
> Une recherche (googler) m'a appris que le service wds n'est pas capable de gérer plus de 20 processeurs logiques affectés au serveur hôte. OMG .. Ensuite, j'ai vérifié les spécifications de mon serveur, il a 6 * 4 = 24 processeurs logiques. Condamner!..
:::

```
bcdedit /set {current} numproc 20
```
src: http://delphintipz.blogspot.com/2012/02/service-did-not-respond-to-start-or.html
https://social.technet.microsoft.com/Forums/lync/en-US/5f71f652-ac73-4290-817e-5a251b7852b4/windows-deployment-services-error-the-service-did-not-respond-to-the-start-or-control-request-in-a?forum=winservergen

https://vkarthickeyan.wordpress.com/2013/05/21/wds-service-unable-to-start/
(autres soluces)
Puis redémarrer.

Autres erreurs :

https://rdr-it.com/troubleshooting/wds-boot-pxe-resoudre-lerreur-0xc0000001/#:~:text=Solution%20pour%20résoudre%20l%27erreur%200xc0000001%20sur%20WDS,-Il%20faut%20modifier&text=Ouvrir%20la%20console%20WDS%2C%20faire,Appliquer%204%20et%20OK%205.


## UEFI

Somes links : 
- https://rdr-it.com/wds-configuration-dhcp-uefi/#:~:text=Sur%20la%20console%20DHCP%2C%20faire,et%20cliquer%20sur%20OK%204.
- http://www.itfaq.dk/2016/07/27/use-dhcp-to-detect-uefi-or-legacy-bios-system-and-pxe-boot-to-sccm/
- https://williamlam.com/2015/10/support-for-uefi-pxe-boot-introduced-in-esxi-6-0.html
- https://williamlam.com/2017/10/tip-from-engineering-use-uefi-firmware-for-windows-10-server-2016.html
- https://mediarealm.com.au/articles/wds-uefi-boot-legacy-boot-pxe-dhcp-option/
- http://www.itfaq.dk/2016/07/27/use-dhcp-to-detect-uefi-or-legacy-bios-system-and-pxe-boot-to-sccm/

Following : https://mohamedhassaneg.wordpress.com/2022/06/14/configuring-windows-deployment-services-wds-server/

#### DHCP Option 67: In case of UEFI Boot:

```undefined
boot\x64\wdsmgfw.efi
```


#### DHCP Option 67: In case of Legacy Boot:

```undefined
boot\x64\wdsnbp.com
```


---

## Drivers vmware tools pour ESX

LIENS VERS TOUT LES VMWARES TOOLS : https://packages.vmware.com/tools/esx/index.html

Monter l'iso et extraire les drivers : 
```
E: setup.exe /A /P C:\VMWareDrivers
```

---

# Convertir un fichier ESD en WIM

On récupère les informations du fichier ESD, via un invité de commandes en Powershell. (E est le lecteur où l'ISO se trouve)

```Powershell
dism /Get-WimInfo /WimFile:E:/sources/install.esd
```

![Pasted image 20230311153614.png](img/Pasted%20image%2020230311153614.png)

On souhaite récupérer la version professionnelle de Windows 10, donc la 6ème. 

On se rend dans le dossier /sources/ où install.esd est.

```Powershell
cd E:\sources\
dism /export-image /SourceImageFile:install.esd /SourceIndex:6 /DestinationImageFile:C:\Users\Administrateur.LAB\Desktop\install.wim /Compress:max /CheckIntegrity
```

## Ajout d'un pilote pour le déploiement

![Pasted image 20230311153700.png](img/Pasted%20image%2020230311153700.png)

![Pasted image 20230311153711.png](img/Pasted%20image%2020230311153711.png)

On ajoute un filtre. Ici, le fabricant est VMware (machine virtuelle VMware).

![Pasted image 20230311153725.png](img/Pasted%20image%2020230311153725.png)

On peut ensuite ajouter un filtre selon l'image. Ensuite : 

![Pasted image 20230311153733.png](img/Pasted%20image%2020230311153733.png)

On clique droit sur "Pilotes" et on ajoute un package de pilotes.

> Au préalable, j'ai extrait les pilotes de VMware. Voir : [https://docs.khroners.fr/books/windows-server-2019/page/extraire-les-pilotes-de-vmware-tools](https://docs.khroners.fr/books/windows-server-2019/page/extraire-les-pilotes-de-vmware-tools)

On choisit le chemin suivant dans mon cas : C:\VMwareDrivers\VMware\VMware Tools\VMware\Drivers\vmxnet3\Win8\vmxnet3.inf

![Pasted image 20230311153748.png](img/Pasted%20image%2020230311153748.png)
![Pasted image 20230311153804.png](img/Pasted%20image%2020230311153804.png)

On l'ajoute au groupe de pilotes précédemment créé.

![Pasted image 20230311153814.png](img/Pasted%20image%2020230311153814.png)

Dans notre cas, il faut l'ajouter dès l'étape de Windows PE. 

## Mise à disposition de pilotes pour le démarrage

![Pasted image 20230311153827.png](img/Pasted%20image%2020230311153827.png)

On clique sur "Rechercher des packages".

![Pasted image 20230311153836.png](img/Pasted%20image%2020230311153836.png)


Le pilote est ajouté.

Le PC démarre bien, avec le bon pilote. Cependant, WDS reste limité pour la customisation de l'image. On verra MDT dans un prochain chapitre.