# WDS - MDT 
---
En lient avec [[Active Directory](Active%20Directory.md)
# Troubleshooting
Installation mode core : https://woshub.com/windows-server-core-install-active-directory-domain-controller/

## First

### Commande : 
```powershell
Get-Module -ListAvailable | Import-module
```

### Erreur : 
```powershell
WARNING: The names of some imported commands from the module 'DeliveryOptimization' include unapproved verbs that might
 make them less discoverable. To find the commands with unapproved verbs, run the Import-Module command again with the
Verbose parameter. For a list of approved verbs, type Get-Verb.
Import-Module : The specified module 'Hyper-V' was not loaded because no valid module file was found in any module
directory.
At C:\Windows\system32\WindowsPowerShell\v1.0\Modules\VMDirectStorage\VMDirectStorage.psm1:7 char:1
+ Import-Module Hyper-V
+ ~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : ResourceUnavailable: (Hyper-V:String) [Import-Module], FileNotFoundException
    + FullyQualifiedErrorId : Modules_ModuleNotFound,Microsoft.PowerShell.Commands.ImportModuleCommand

Import-Module : The specified module 'Hyper-V' was not loaded because no valid module file was found in any module
directory.
At C:\Windows\system32\WindowsPowerShell\v1.0\Modules\VMDirectStorage\VMDirectStorage.psm1:7 char:1
+ Import-Module Hyper-V
+ ~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : ResourceUnavailable: (Hyper-V:String) [Import-Module], FileNotFoundException
    + FullyQualifiedErrorId : Modules_ModuleNotFound,Microsoft.PowerShell.Commands.ImportModuleCommand
```

### Résolution : 

- Relancer la commande, pas grave si vous avez un petit warning qui apparait
- Essayer d'ajouter le Path : https://www.sharepointdiary.com/2020/01/import-module-specified-module-not-loaded-because-no-valid-module-file-found-in-any-module-directory.html

--- 

## 2)

### Commande : 

```powershell
PS C:\Users\Administrator> add-computer -domainname vlne.lan
```


### Erreur : 

```powershell

cmdlet Add-Computer at command pipeline position 1
Supply values for the following parameters:
Credential
add-computer : Computer 'sadw02' failed to join domain 'vlne.lan' from its current workgroup 'WORKGROUP' with
following error message: The specified domain either does not exist or could not be contacted.
At line:1 char:1
+ add-computer -domainname vlne.lan
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : OperationStopped: (sadw02:String) [Add-Computer], InvalidOperationException
    + FullyQualifiedErrorId : FailToJoinDomainFromWorkgroup,Microsoft.PowerShell.Commands.AddComputerCommand
    + 
```


### Résolution : 

- Remettez vous sur le même réseau (NAT)
- Essayer de ping le domaine
- Bien vérifier que vous avez ajouté le DNS dans la config IP
- La commande `nslookup vlne.lan` doit fonctionner


---

## 3)

### Commande : 
Ajout controleur de domaine :
```powershell
Install-ADDSDomainController -DomainName vlne.lan -InstallDns:$true -NoGlobalCatalog:$false -NoRebootOnCompletion:$true -SafeModeAdministratorPassword (ConvertTo-SecureString 'Yvettedu95' -AsPlainText -Force) -Credential (get-credential VLNE\Administrator) -verbose
```


### Erreur : 
```powershell
Install-ADDSDomainController : Verification of user credential permissions failed. An Active Directory domain
controller for the domain "vlne.lan" could not be contacted.
Ensure that you supplied the correct DNS domain name.
At line:1 char:1
+ Install-ADDSDomainController -DomainName vlne.lan -InstallDns:$true - ...
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:) [Install-ADDSDomainController], TestFailedException
    + FullyQualifiedErrorId : Test.VerifyUserCredentialPermissions.DCPromo.General.25,Microsoft.DirectoryServices.Depl
   oyment.PowerShell.Commands.InstallADDSDomainControllerCommand
```

Alors que le domaine existe bien et est bon.
```
Verification of user credential permissions failed. An Active Directory domain controller for the domain "vlne.lan" ...
```

### Résolution : 
- Avant de promouvoir un nouveau contrôleur de domaine, assurez-vous que votre domaine Active Directory fonctionne correctement. Vérifiez soigneusement les erreurs sur chaque DC renvoyées par `Dcdiag /v` et vérifiez la réplication AD (`repadmin /showrepl` et `repadmin /replsum`). 
- Essayer de clear le DNS via `sconfig` et re-ajouter le DNS

- Désactiver IPV6

```
Get-NetAdapterBinding (voir les adaptateurs)
```
![Pasted image 20230311115720.png](img/Pasted%20image%2020230311115720.png)
Désactivation IPV6 : 
```
Disable-NetAdapterBinding –InterfaceAlias “Ethernet0” –ComponentID ms_tcpip6
```
![Pasted image 20230311115827.png](img/Pasted%20image%2020230311115827.png)

```powershell
VERBOSE: Active Directory Domain Services Setup
VERBOSE: Validating environment and parameters...
WARNING: Windows Server 2022 domain controllers have a default for the security setting named "Allow cryptography
algorithms compatible with Windows NT 4.0" that prevents weaker cryptography algorithms when establishing security
channel sessions.

For more information about this setting, see Knowledge Base article 942564
(http://go.microsoft.com/fwlink/?LinkId=104751).
                                                                                                                        WARNING: A delegation for this DNS server cannot be created because the authoritative parent zone cannot be found or it  does not run Windows DNS server. If you are integrating with an existing DNS infrastructure, you should manually       create a delegation to this DNS server in the parent zone to ensure reliable name resolution from outside the domain    "vlne.lan". Otherwise, no action is required.                                                                                                                                                                                                   VERBOSE: ----------------------------------------                                                                       VERBOSE: The following actions will be performed:                                                                       VERBOSE: Configure this server as an additional Active Directory domain controller for the domain "vlne.lan".                                                                                                                                   Site:                                                                                                                                                                                                                                           Additional Options:                                                                                                       Read-only domain controller: "No"                                                                                       Global catalog: Yes                                                                                                     DNS Server: Yes                                                                                                                                                                                                                               Update DNS Delegation: No                                                                                                                                                                                                                       Source DC: sadw01.vlne.lan                                                                                                                                                                                                                      Database folder: C:\Windows\NTDS                                                                                        Log file folder: C:\Windows\NTDS                                                                                        SYSVOL folder: C:\Windows\SYSVOL                                                                                                                                                                                                                The DNS Server service will be installed on this computer.                                                              The DNS Server service will be configured on this computer.                                                             This computer will be configured to use this DNS server as its preferred DNS server.
VERBOSE: ----------------------------------------
VERBOSE: Active Directory Domain Services Setup
VERBOSE: Validating environment and parameters...
WARNING: Windows Server 2022 domain controllers have a default for the security setting named "Allow cryptography
algorithms compatible with Windows NT 4.0" that prevents weaker cryptography algorithms when establishing security
channel sessions.

For more information about this setting, see Knowledge Base article 942564
(http://go.microsoft.com/fwlink/?LinkId=104751).
                                                                                                                        WARNING: A delegation for this DNS server cannot be created because the authoritative parent zone cannot be found or it  does not run Windows DNS server. If you are integrating with an existing DNS infrastructure, you should manually       create a delegation to this DNS server in the parent zone to ensure reliable name resolution from outside the domain    "vlne.lan". Otherwise, no action is required.                                                                                                                                                                                                   VERBOSE: ----------------------------------------                                                                       VERBOSE: The following actions will be performed:                                                                       VERBOSE: Configure this server as an additional Active Directory domain controller for the domain "vlne.lan".                                                                                                                                   Site:                                                                                                                                                                                                                                           Additional Options:                                                                                                       Read-only domain controller: "No"                                                                                       Global catalog: Yes                                                                                                     DNS Server: Yes                                                                                                                                                                                                                               Update DNS Delegation: No                                                                                                                                                                                                                       Source DC: sadw01.vlne.lan                                                                                                                                                                                                                      Database folder: C:\Windows\NTDS                                                                                        Log file folder: C:\Windows\NTDS
SYSVOL folder: C:\Windows\SYSVOL

The DNS Server service will be installed on this computer.
The DNS Server service will be configured on this computer.
This computer will be configured to use this DNS server as its preferred DNS server.
VERBOSE: ----------------------------------------
VERBOSE: Press CTRL-C to: Cancel
VERBOSE: Active Directory Domain Services is now installed on this computer for the domain "vlne.lan".

This Active Directory domain controller is assigned to the site "Default-First-Site-Name". You can manage sites with
the Active Directory Sites and Services administrative tool.

Message                                                     Context           RebootRequired  Status
-------                                                     -------           --------------  ------
You must restart this computer to complete the operation... DCPromo.General.4           True Success
```


- https://learn.microsoft.com/fr-fr/windows-server/identity/ad-ds/deploy/troubleshooting-domain-controller-deployment


---
## 4)

### Commande : 


### Erreur : 


### Résolution : 

---


