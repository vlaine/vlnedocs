
![](../img/images/media/image1.png)


# Rapport attendu

## Consignes données par le professeur

-   Les éléments suivants sont attendus suite à votre TP (screenshot obligatoire) : 
-   La procédure d'ajout du serveur en tant que contrôleur de domaine dans le domaine existant (écran par écran)

-   Ajout d'une machine dans le domaine
-   L'OU des contrôleurs de domaines (avec les machines visibles)
-   L'arborescence avec les groupes et les utilisateurs créé
-   Les propriétés d'un utilisateur (avec le mot de passe n'expirant pas)
-   Les propriétés d'un groupe de sécurité
-   Les membres de groupes de sécurité «*Marketing*» et «*Informatique*»
-   La console MMC «Utilisateurs et Ordinateurs Active Directory)

**Veuillez soigner votre rapport et votre rédaction.**

# Consignes d'installation des ADDS

## Consigne Générale

Les serveurs suivants sont à installer (les 2 machines seront membres du
même domaine) :
-   L'installation d'un 1er Windows Server 2012 en version Core nommé «
    NomPrenom01 » (sans majuscules) **Ici « sadw02 »**
-   L'installation d'un 2e Windows Server 2012 en version Standard «
    NomPrenom02 » (sans majuscules) **Ici « sadw01 »**

**J'ai d'abord installé la version graphique.**

Les 2 machines seront contrôleur de domaine du même domaine
nomprenom.local. **Ici juste nom.local**

L\'installation de Active Directory se fait \...
-   en ajoutant le rôle Service de Domaine Active Directory
-   puis en allant dans le rôle, en cliquant sur promouvoir le serveur en contrôleur de domaine
-   Cocher l\'installation en mode avancé.
-   Domaine dans une nouvelle forêt
-   Le domaine sera votre « laine.local » (sans les majuscules)
-   Nom NetBios : LAINE
-   Niveau fonctionnel : Windows Server 2022
-   Les autres options seront laissées par défaut.

# Installation du contrôleur de domaine (Version GUI)


## Infrastructure 

Dans mon cas, l'active directory sera installé :

-   Sur une machine virtuelle, dans un hyperviseur VMWare ESXi 5.5
    Update 3.
-   Sur un Windows Server 2022
-   Avec 4-8Go de RAM
-   60 Go de disque dur
-   Une IP fixe

## Adressage IP et Hostname

J'ai attribué à mon serveur une adresse IP fixe avec les configurations
suivantes :

- Adresse : 192.168.1.101
- Masque de sous réseaux : 255.255.255.0
- Gateway : 192.168.1.1
- Le nom de mon serveur est **sadw01**.
- **s** = serveur
- **ad** = active directory
- **w** = windows
- **01** = Premier serveur de ce type

## Installation du contrôleur de domaine

Une fois que notre système d'exploitation est installé. Rendez-vous dans
Server **Manager -\> Manage -\> Add Roles and Features.**

Puis sélectionnez **Role-based or feature-based installation.**

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.46.36.png](../img/images/media/image3.png)

Puis cochez la case **Select a server from a server pool**.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.47.12.png](../img/images/media/image4.png)

Ensuite sélectionnez les cases **Active Directory Domain Services et DNS
Server**.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.47.45.png](../img/images/media/image5.png)

Au moment où vous cocherez ces cases, le serveur vous demandera si vous voulez ajouter les fonctionnalités liées aux rôles. Cochez **Include
management tools** puis cliquez sur **Add Features**.

  ![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.47.27.png](../img/images/media/image6.png)   
  
  ![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.47.37.png](../img/images/media/image7.png)

La prochaine étape nous demande si on veut installer des fonctionnalités
supplémentaires. Dans notre cas, nous laissons par défaut. Cliquez sur
**Next**.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.48.05.png](../img/images/media/image8.png)
Le résumé de notre configuration apparaît. Cochez **Restart the
destination server automatically if required** pour redémarrer le
serveur automatiquement si besoin, puis **Install**.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.48.29.png](../img/images/media/image9.png)

Nous pouvons ensuite fermer la fenêtre sans que l'installation ne soit
interrompue. Nous pouvons suivre l'avancement de l'installation depuis
le **Server Manager.** Cliquez sur le petit drapeau avec le point
d'exclamation pour avoir le visuel.

Nous pouvons voir que le serveur nous propose de promouvoir ce serveur
comme un contrôleur de domaine. C'est ce que nous allons faire ici.
Cliquez donc sur le lien **Promote this server to a domain controller**.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.50.54.png](../img/images/media/image10.png)

Une fois cela fait, nous arrivons sur une fenêtre de configuration. Nous
n'avons dans notre cas, aucune forêt, nous allons donc cocher la case
**Add a new forest**. Puis nous définissons notre domaine, ici
**laine.local.** Dans le cadre d'un labo comme celui-ci, il ne faut pas
ajouter de domaine en .fr où .com sous peine d'avoir des problèmes de
DNS par la suite.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.52.08.png](../img/images/media/image11.png)

Laissez ensuite la configuration de la forêt et du domaine en Windows Server 2012 R2, et choisissez un mot de passe DSRM à conserver précieusement en cas de future panne.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.53.10.png](../img/images/media/image12.png)
Choisissez ensuite le nom NetBIOS du domaine, ici LAINE.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.54.35.png](../img/images/media/image13.png)
Laissez ensuite la configuration par défaut. Cliquez sur **Next**.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.54.46.png](../img/images/media/image14.png)
Encore **Next**.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.55.04.png](../img/images/media/image15.png)
Ensuite, attendez que le Wizard ai fini de vérifier les prérequis,
ignorez les avertissements, et cliquez sur **Install**.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-02%20à%2023.55.58.png](../img/images/media/image16.png)

Le serveur va ensuite redémarrer. Nous pouvons nous connecter avec nos identifiants Administrator.

**Login** : LAINE\\Administrator

**Password** : \*\*\*\*\*\*\*\*

Nous pouvons voir dans le serveur manager que le serveur est dans le
domaine et correctement installé.

![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-03%20à%2012.43.08.png](../img/images/media/image17.png)
![Screen%20install%20controleur%20de%20domaine/Capture%20d'écran%202017-02-03%20à%2012.43.19.png](../img/images/media/image18.png)
Le contrôleur est fin prêt. Nous pouvons maintenant ajouter des
utilisateurs, groupes, machines, et profitez de toutes les autres
fonctionnalités disponibles comme l'ajout de machines serveurs ou
clientes dans le domaine.

# Installation du second contrôleur de domaine (Core)

## Installation et Configuration basique du serveur

L'installation d'un Windows Serveur 2012/2016 R2 se fait de la même
manière qu'un Windows classique mais en sélectionnant sa version
minimale lors de l'installation.

Une fois l'OS installé. Nous devons définir en premier lieu le mot de
passe administrateur.
  ![](../img/images/media/image19.png)  
  
  ![](../img/images/media/image20.png)  
  
  
Une fois logger pour la première fois, nous passons en PowerShell et
donnons les premières config au serveur.

Tapez d'abord **powershell** puis **sconfig** pour accéder au « server
manager » de la version core.

![](../img/images/media/image21.png)
Résumé des options du server manager de la version Core :

1)  **Domain/Workgroup** : Option de liaison de machine au domaine

2)  **Computer** **Name** : Option du hostname de la machine

3)  **Add Local Administrator** : Ajout d'un utilisateur au groupe
    Administrateur local

4)  **Configure Remote Management** : Configuration de l'accès à
    distance par powershell

5)  **Windows Update Settings** : Activer ou désactiver les mises à
    jours Windows

6)  **Download or install updates** : Télécharger et installé les mises
    à jours Windows disponible

7)  **Remote Desktop** : Activer le RDP

8)  **Network Settings** : Paramètres réseau

9)  **Date and time** : Changement de la date et de l'heure

10) **Help Improve the product with CEIP** : Envoyer des données
    d'utilisation à Microsoft

11) **Windows Activation** : Activation de Windows (en ligne, avec
    licence...)

12) **Log off user** : Se déconnecter

13) **Restart Server** : Redémarrer

14) **Shut Down Server** : Eteindre

15) **Exit Command Line** : Revenir en ligne de commande

Nous allons tout d'abord changer le nom de la machine en SADW02. Option
(2). Acceptez le redémarrage de la machine.
  ![](../img/images/media/image23.png)
  
Nous activons ensuite le Remote Mangement (3). Indiquez le nom d'utilisateur et le mot de passe du compte.
![](../img/images/media/image25.png)
  
![](../img/images/media/image26.png)


Nous allons ensuite configurer le réseau (8). Nous lui attribuons une adresse IP fixe et les DNS adéquat. Ici les DNS seront, le serveur DC et lui-même, afin d'avoir une bonne communication pour la liaison à l'AD.

La configuration sera :

**Adresse IP** : 192.168.1.105

**Masque** : 255.255.255.0

**Gateway** : 192.168.1.1

**DNS** **Primaire** : 192.168.1.101 (Adresse du premier DC)

**DNS** **Secondaire** : 192.168.1.105 (Lui-même, futur DC)

Paramètre réseau de base.

![](../img/images/media/image27.png)

Option (1) pour mettre une IP statique.

![](../img/images/media/image28.png)

Puis option (2) pour définir les serveurs DNS.

![](../img/images/media/image29.png)

Nous pouvons voir dans le résumé que les adresses sont bien entrées.

![](../img/images/media/image30.png)

Après ces configurations de base, nous chargeons tous les modules PowerShell. Tapez la commande : **Get-Module -ListAvailable \| Import-Module**

![](../img/images/media/image31.png)

Puis, pour plus de simplicité ici, nous désactivons le pare-feu, mode cmd : **netsh advfirewall set AllProfiles state off**

![](../img/images/media/image32.png)

Nous installons ensuite le rôle serveur Active directory et ses composants : **Install-WindowsFeature --name AD-Domain-Services -IncludeManagementTools**

![](../img/images/media/image33.png)

L'installation s'est bien déroulée.

![](../img/images/media/image34.png)

Nous pouvons ensuite ajouter notre machine au domaine que nous avons configuré précédemment.

Pour cela, retour dans le panneau « sconfig », puis choisir l'option (1)
ou tapez la commande **add-computer --domainname laine.local**

Pour ajouter la machine, après avoir choisie l'option (1), tapez **D**
pour lui indiquer que vous voulez joindre un domaine, tapez ensuite le
nom du domaine à joindre, ici, **laine.local**.

Spécifiez ensuite l'utilisateur qui vous servira à vous connecter au
domaine. Nous utiliserons le compte administrateur,
**LAINE\\Administrator**. Tapez ensuite votre mot de passe quand cela
vous est demandé.

![](../img/images/media/image35.png)

Redémarrez ensuite la machine quand demandée. Vous pouvez voir dans
**sconfig** que vous êtes bien lié au domaine.

![](../img/images/media/image36.png)

Nous pouvons également le vérifier sur DC, dans le DNS et les computers
de l'Active Directory.

  ![](../img/images/media/image37.png)  
  

  ![](../img/images/media/image38.png)  
Pour plus de simplicité de management, nous allons ajouter ce serveur
aux serveurs managé du DC. Pour cela, rendez-vous dans votre Server
Manager du DC, puis cliquez sur Manage en haut à droite, puis **Add**
**Servers**.

Cherchez ensuite votre serveur fraîchement ajouté à l'AD, puis ajoutez-le à la sélection. Puis **Ok**.

![](../img/images/media/image39.png)
  
![](../img/images/media/image40.png)  

Nous pouvons voir que le serveur est disponible dans la liste.

![](../img/images/media/image41.png)

## Administration à distance

Nous avons précédemment dans **sconfig**, activé l'administration à
distance. Nous aurions aussi pu passer par la commande PowerShell,
**Enable-PSRemoting**.

![](../img/images/media/image42.png)

Grâce à ça, depuis notre liste de serveur, nous pouvons par exemple accéder au serveur en PowerShell depuis le DC.

![](../img/images/media/image43.png)

## Ajout du serveur core en tant que contrôleur de domaine

Il y a plusieurs façons de faire pour définir ce serveur en tant que
contrôleur de domaine. Soit en PowerShell, soit en graphique depuis le
premier DC, car nous pouvons le manager à distance.

### En PowerShell

Il suffit de taper en powershell :

**POUR 2012 et 2012 R2 : 

```bash
Install-ADDSDomainController -domainname laine.local -installdns :\$true -noglobalcatalog :\$false
```

**POUR 2016 : 
```bash
Install-ADDSDomainController -domainname laine.local -installdns -noglobalcatalog
```

pour installer le contrôleur de domaine **laine.local**, installer les
composants DNS et le définir en tant que Catalogue Global. Il ne faut
pas oublier de se connecter avec le compte **administrateur du
domaine.**

Dans notre cas, nous avons privilégié l'installation en graphique depuis
le premier DC pour des raisons de simplicités et parce que le résultat
sera le même. L'avancement de l'installation est le même qu'en graphique
mais adapté pour un mode console.

### En Graphique

Comme pour la première installation du DC, nous avons le petit panneau
jaune (notification) dans le server manager pour promouvoir le serveur
en tant que contrôleur de domaine. Cette notification apparaît car nous
avons installé les rôles et les services précédemment.
(**Install-WindowsFeature --name AD-Domain-Services
--IncludeManagementTools**)

***N:B** Avant de pouvoir lancer la procédure, nous devons ajouter notre
serveur (SADW02) au hôtes de confiance. Via PowerShell sur le DC1 :*
**Set-Item WSMAN:\\localhost\\Client\\TrustedHosts --value sadw02
--force**

![](../img/images/media/image44.png)

Nous pouvons maintenant passer à la configuration du nouveau contrôleur
de domaine.

![](../img/images/media/image45.png)

Cochez, **Add a Domain controller to an existing domain** dans
**laine.local**. Puis cliquez sur Change pour ajouter vos informations
d'identification : **LAINE\\Administrator et mot de passe :
\*\*\*\*\*\*\*\***

![](../img/images/media/image46.png)

Les Credentials :

![](../img/images/media/image47.png)

Cochez ensuite les deux premières cases et tapez votre mot de passe
DSRIM.

![](../img/images/media/image48.png)

Laissez l'option **Any Domain controller** et ne cochez pas la case
**Install from media**. **Next**.

![](../img/images/media/image49.png)
Laissez par défaut.

![](../img/images/media/image50.png)

Le Review, vérifiez votre configuration et cliquez sur **Next**.

![](../img/images/media/image51.png)

Une fois les prérequis check, cliquez sur **Install**.

![](../img/images/media/image52.png)

L'installation s'est bien déroulée. Cliquez sur **Close**. Le serveur
SADW02 va redémarrer automatiquement.

![](../img/images/media/image53.png)

Nous pouvons maintenant vérifier que le serveur est bien défini en tant
que contrôleur de domaine. Allez dans le Server Manager -\> Tools -\>
Active Directory Sites and Services. Le visuel vous montre les deux
serveurs contrôleur de domaine.

![](../img/images/media/image54.png)

Toujours dans le serveur manager également.

![](../img/images/media/image55.png)

Notre second contrôleur de domaine est maintenant opérationnel.

# Ajout d'une machine cliente dans le domaine

## Prérequis

Afin d'ajouter une machine dans notre domaine, celle-ci doit être sur le
même réseau (ici 192.168.1.X). Ici, nous avons une machine cliente sous
Windows 7. L'ajout de la machine nous servira pour utiliser les outils
d'administration par la suite.

Nous partons du principe que la machine est sur une installation
classique, ici nous lui avons attribué une IP fixe car nous n'avons pour
le moment aucun DHCP d'installé. Et en DNS l'adresse du contrôleur de
domaine. (Nous aurions pu mettre le DC secondaire en secondaire
également)

![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.30.26.png](../img/images/media/image56.png)

## Ajout de la machine au domaine

Pour ce faire, nous devons nous rendre dans les paramètres de
l'ordinateur. Cliquez droit sur le poste de travail, puis propriétés.

Les performances de la machine y sont décrites. Cliquez sur **Modifier
les paramètres**, en face du nom du poste.

![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.17.31.png](../img/images/media/image57.png)

Puis ensuite sur le bouton **Modifier**. Cochez la case Domaine. Puis
tapez le nom NetBIOS du contrôleur de domaine. Ici **LAINE**.

![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.22.54.png](../img/images/media/image58.png)

L'application va ensuite vous demander un nom d'utilisateur et un mot de
passe. Mettez les identifiants admin défini plus tôt.

![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.23.10.png](../img/images/media/image59.png)

La machine est maintenant ajoutée au domaine. Vous devez redémarrer la
machine pour appliquer les changements.

![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.34.12.png](../img/images/media/image60.png)

  
![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.34.19.png](../img/images/media/image61.png)


![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.34.28.png](../img/images/media/image62.png)


![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2013.25.38.png](../img/images/media/image63.png)

### Troubleshooting 

Il peut arriver parfois que la machine cliente ai des problèmes pour
résoudre le nom du serveur DNS. Si c'est le cas, pour pallier ce
problème, il faut ajouter des clefs de registre à la machine. Cette
procédure fonctionne également pour les serveurs.

Rendez-vous donc dans le regedit.

Dans
KEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\LanmanWorkstation\\Parameters
et ajouter ces deux clés de registre DWORD 32 bits.\
\
DomainCompatibilityMode avec la valeur 1 \
DNSNameResolutionRequired avec la valeur 0

![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.31.43.png](../img/images/media/image64.png)

![Screen%20ajout%20d\'un%20client%20dans%20l\'AD/Capture%20d'écran%202017-02-03%20à%2001.32.53.png](../img/images/media/image65.png)

# Installation des RSAT

## Consigne

-   Installer les RSAT sur votre hôtes (Windows de votre machine locale)

-   Connectez-vous avec 2 MMC « Utilisateurs et Ordinateurs Active
    Directory » sur vos 2 contrôleurs de domaines séparément

-   Fournissant une impression écran des 2 consoles MMC en même temps

## Installation des RSAT

Téléchargez le KB et l'installer. 


Pour Windows 10 ou 7 rendez-vous sur [ce lien](https://www.microsoft.com/fr-FR/download/details.aspx?id=45520) et installez le package.

![RSAT/Capture%20d'écran%202017-02-03%20à%2015.46.45.png](../img/images/media/image66.png) 
  
  
![RSAT/Capture%20d'écran%202017-02-03%20à%2015.47.15.png](../img/images/media/image67.png)


  
Ajoutez ensuite les fonctionnalités suivantes à votre machine Windows
(Windows 7)

![RSAT/Capture%20d'écran%202017-02-03%20à%2016.05.17.png](../img/images/media/image68.png)

Une fois les fonctionnalités ajoutées, vous pouvez les retrouver dans
les Outils d'administration.

![RSAT/Capture%20d'écran%202017-02-03%20à%2016.34.43.png](../img/images/media/image69.png)

Pour Windows 10, même principe, exécuter le logiciel précédemment
téléchargé :

![](../img/images/media/image70.png)

![](../img/images/media/image71.png)

Puis redémarrer :

![](../img/images/media/image72.png)

Aller ensuite dans paramètres, puis applications pour installer les
RSAT, dans la recherche, tapez RSAT puis installez les :

![](../img/images/media/image73.png)

![](../img/images/media/image74.png)

![](../img/images/media/image75.png)

## Connexion avec les MMC des contrôleurs de domaine

Tout d'abord, connectez-vous avec votre machine liée à l'AD avec les
identifiants administrateur du domaine (LAINE\\Administrator).

Dans les outils d'administration, cliquez sur **Utilisateurs et
ordinateurs Active Directory.** Ouvrez deux consoles afin d'avoir le
visuel des deux contrôleurs de domaine.

Une fois les consoles ouvertes, dans l'une d'elle, cliquez droit sur
**Utilisateurs et ordinateur Active Directory \[ sadw01.laine.local \]**
et faite changer de contrôleur de domaine. Sélectionnez **sadw02** puis
Ok.

![](../img/images/media/image76.png)

Nous pouvons maintenant administrer nos deux Contrôleurs de domaine en
même temps.

![](../img/images/media/image77.png)

# Administration du contrôleur de domaine

## Consigne

Créer un OU « Holding » à la racine et créez les trois OU suivantes :

-   « Compta »

-   « Marketing »

-   « Informatique »

Dans l'OU « Compta », créez les groupes de sécurité « assistants » et

« chefs comptables ».

Dans l'OU « Marketing », créez le groupe « accueil » et le groupe «
assistantes ».

Dans l'OU « Informatique », créez le groupe « developpeurs » et le
groupe

« techniciens ».

Créer les utilisateurs suivants avec les caractéristiques suivantes :

-   Julien DROUET

> Membre du groupe : développeurs
>
> Placé dans l'OU : Informatique
>
> Mot de passe : Espoir15 (avec la lettre E en majuscules)
>
> Le mot de passe n'expire pas

-   Jessica MOTAF

> Membre du groupe : assistantes
>
> Mot de passe : Espoir15 (avec la lettre E en majuscules)
>
> Placé dans l'OU : marketing

-   Jean-Philippe PIAGOT

> Membre du groupe : assistant
>
> Mot de passe : Espoir15 (avec la lettre E en majuscules)
>
> Placé dans l'OU : Compta

-   Marie-Claire CUBILLE

> Membre du groupe : Techniciens
>
> Mot de passe : Espoir15 (avec la lettre E en majuscules)
>
> Le mot de passe n'expire pas
>
> Placé dans l'OU : Informatique

## Création des OU

### Création des OU

Rendez-vous dans **Utilisateur et ordinateurs Active Directory** depuis
un MMC ou sur le DC.

Cliquez droit dans le vide de l'arborescence, puis New, **Organization
Unit**. Nommez la « **Holding **» puis Ok.

![](../img/images/media/image78.png)

L'OU Holding est créée à la racine.

![](../img/images/media/image79.png)

Cliquez droit ensuite sur l'OU Holding, est New -\> Organization Unit.
Nommez les Marketing, Informatique, Compta.

![](../img/images/media/image80.png)

![](../img/images/media/image81.png)

## OU des contrôleurs de domaine

Dans le dossier Domain Controllers, à la racine, nous pouvons voir les
machines contrôleur de domaine. Nous avons ici les deux DC configuré
précédemment.

![](../img/images/media/image82.png)

## Création des groupes 

### Création des groupes

Pour créer un groupe, cliquez droit sur l'OU dans laquelle vous voulez
créer votre groupe, puis New -\> Group.

Ici nous créons les groupes de sécurité « assistants » et « chefs
comptables » dans l'OU Compta.

![](../img/images/media/image83.png)
  

![](../img/images/media/image84.png)  
Ils y sont bien présents.

![](../img/images/media/image85.png)

Maintenant, nous créons les groupes de distribution « accueil » et
« assistantes » dans l'OU Marketing.

  ![](../img/images/media/image86.png)
  
  
  ![](../img/images/media/image87.png)  

Ils y sont présents.

![](../img/images/media/image88.png)

Exactement de la même manière mais dans l'OU Informatique, les groupes
de distribution « développeurs » et « technicien ».

![](../img/images/media/image89.png)

### Exemple de propriétés d'un groupe de sécurité

Ici, nous voyons le groupe de sécurité « chefs comptables ». La case
Security est bien coché dans le cadre « Group type ».

  ![](../img/images/media/image90.png)

  
  ![](../img/images/media/image91.png)  
## Création des comptes utilisateurs

### Création des comptes

Pour créer un utilisateur, cliquez droit sur l'OU dans laquelle vous
voulez créer un utilisateur, New -\> User.

![](../img/images/media/image92.png)

Puis renseignez les paramètres du compte, Nom, Prénom, mot de passe..
Cochez « Password never expires » pour que le mot de passe n'expire
jamais.
  ![](../img/images/media/image93.png)
  
  
  ![](../img/images/media/image94.png)  

De la même manière et dans les bon OU, créez les utilisateurs Jessica
MOTAF, Jean-Philippe PIAGOT, et Marie-Claire CUBILLE. (Les mots de
passes expireront pour Mme. MOTAF et Mr. PIAGOT.)

Il nous faut, une fois les utilisateurs créés, les ajouter aux bons
groupes.

Pour cela, clic droit sur un utilisateur -\> Propriétés. Dans l'onglet
**Member Of**, cliquez sur **Add**... pour ajouter un groupe.

  ![](../img/images/media/image95.png)
  
  
  ![](../img/images/media/image96.png)  

Tapez le nom du groupe puis OK. Faite ensuite Apply puis OK pour valider
les nouveaux paramètres. Les groupes ont été ajoutés selon la consigne.

### Exemple de propriétés d'un utilisateur

Ici Julien DROUET, son mot de passe n'expire jamais et il est membre du
groupe « développeurs »

  ![](../img/images/media/image97.png)
  
  
  ![](../img/images/media/image98.png)  

## Configuration finale

### Arborescence des groupes et des utilisateurs créés 

![](../img/images/media/image99.png)

### OU Informatique

![](../img/images/media/image100.png)

### OU Marketing

![](../img/images/media/image101.png)

# Création des réplications

## Consigne

-   Créer 1 groupes de réplication (à nommer selon votre convenance)

-   Créer 1 plan d'adressage IP

-   Affecter chacun de vos contrôleurs de domaine au groupe de
    réplication

-   Définir le contrôleur en version Core en tant que « Catalogue Global
    »

-   Définir la planification de la réplication toutes les heures sauf
    mercredi et samedi

## Création d'un groupe de réplication

Pour créer une réplication il faut se rendre dans le Server Manager -\>
Tools -\> Active Direcotry Sites and Services.

Commençons par créer un **Subnet**, cliquez droit sur **Subnets** et
**New** **Subnet**. Tapez l'adresse de votre réseau/CIDR puis faite
**OK**.

![](../img/images/media/image102.png)

J'ai nommé mon groupe de réplication « Siege-Social ».

Cliquez droit ensuite SADW01, sélectionnez **IP** et **SMTP** et **Add
\>\>**.

![](../img/images/media/image103.png)

Puis dans Sites -\> Inter-Site Transport -\> IP, clique droit,
propriétés.

  ![](../img/images/media/image104.png)
  
  
  ![](../img/images/media/image105.png)  

Changez la valeur « Replicate every » à 60, puis dans « Change
Schedule » désactiver la réplication le mercredi et vendredi.

## Planification de la réplication

Dans les NTDS Settings du serveur, cliquez droit --\> Propriétés sur
**\<automatically generated\>**.

![](../img/images/media/image106.png)

Changez la planification de la réplication selon la consigne. (Ici from
SADW02, on configure selon quel serveur sera votre serveur primaire.)

![](../img/images/media/image107.png)

La réplication est configurée. Nous pouvons maintenant la lancer en mode
manuel et vérifier son bon fonctionnement.

Clic droit sur la tâche qui viens d'être modifié **-\> Replicate Now**

  ![](../img/images/media/image108.png)
  
  
  ![](../img/images/media/image109.png) 
  
La réplication a été effectué. Nous pouvons maintenant vérifier sur
SADW02 si nous avons nos utilisateurs et nos groupes créés précédemment.
Je vois que tout y est. La réplication s'est bien déroulée.

![](../img/images/media/image110.png)

## Affectation des contrôleurs de domaine au groupe de réplication

Mes deux serveurs sont bien présents dans le groupe de réplication.

![](../img/images/media/image111.png)

## Définition du contrôleur version Core en tant que Catalogue Global

Pour définir un serveur en mode Catalogue Global, clique droit sur NTDS
Settings du serveur en question, puis Propreties. Et cochez la case
**Global Catalog** puis **OK**.

![](../img/images/media/image112.png)

Dans les propriétés de SADW02, nous pouvons voir qu'il est défini en
tant que Catalogue Global. Dans le champ « DC Type »

![](../img/images/media/image113.png)

## Sécurité 

[[Découverte réseau Eternal Blue](../S%C3%A9curit%C3%A9%20Windows/D%C3%A9couverte%20r%C3%A9seau%20Eternal%20Blue.md) & [[Exploitation Eternal Blue](../S%C3%A9curit%C3%A9%20Windows/Exploitation%20Eternal%20Blue.md)