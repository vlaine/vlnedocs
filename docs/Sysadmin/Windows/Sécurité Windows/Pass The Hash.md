Pass The Hash
---

Un exemple de POC en cybersécurité lié à l'Active Directory pourrait être la démonstration d'une attaque de type "Pass-the-Hash" (PtH). Cette attaque consiste à utiliser les hachages de mots de passe volés pour se connecter à d'autres systèmes sans avoir besoin de connaître le mot de passe en clair.

Voici une procédure simplifiée pour effectuer un POC sur une attaque Pass-the-Hash sur l'Active Directory :

1.  Préparation de l'environnement de test : Configurez un environnement de test isolé avec un contrôleur de domaine Active Directory, des ordinateurs clients et des comptes d'utilisateurs. Assurez-vous que votre environnement de test ne soit pas connecté à votre réseau de production pour éviter tout risque de compromission.
    
2.  Extraction des hachages : Utilisez un outil tel que Mimikatz pour extraire les hachages de mots de passe des comptes d'utilisateurs à partir d'un ordinateur compromis. Cela peut être réalisé en exécutant Mimikatz avec des privilèges administratifs et en utilisant la commande "sekurlsa::logonpasswords" pour récupérer les hachages.
    
3.  Utilisation de l'attaque Pass-the-Hash : Utilisez un outil comme PsExec ou un autre outil d'administration à distance pour vous connecter à un autre ordinateur du réseau en utilisant les hachages de mots de passe extraits à l'étape précédente. Par exemple, avec PsExec, vous pouvez utiliser la commande suivante :
    

```xml
psexec.exe \\target_computer -u domain\username -p <password_hash> cmd.exe
```


4.  Vérification de l'accès : Si l'attaque Pass-the-Hash réussit, vous devriez avoir accès à l'ordinateur cible avec les privilèges du compte utilisateur dont vous avez utilisé le hachage. Vous pouvez alors explorer les ressources du système pour démontrer la portée de l'attaque.
    
5.  Mise en place de mesures de sécurité : Une fois le POC terminé, identifiez et mettez en œuvre des mesures de sécurité pour protéger l'Active Directory contre ce type d'attaque. Par exemple, appliquez des politiques de sécurité strictes, mettez en œuvre la limitation des privilèges et utilisez des solutions de protection des identités, comme l'authentification à deux facteurs (2FA).
    

Ce POC permet de démontrer la vulnérabilité d'un environnement Active Directory à une attaque Pass-the-Hash et la nécessité de mettre en place des mesures de sécurité appropriées pour protéger les systèmes et les données.