# Authentification Windows
Link to : [[Active Directory](../Active%20Directory/Active%20Directory.md)

---
Password VM AD Security :  ParamountDefenses!
1.  Download this free Active Directory Security Virtual Machine from [here](https://www.paramountdefenses.com/vm/AD-Security.zip).
2.  Download and install the free version of VMWare Workstation Player from [here](https://www.vmware.com/go/downloadplayer).
3.  Unzip the VM to extract the "_AD Security_" folder
4.  Create a "_Virtual Machines_" folder in "_My Documents_"
5.  Move the unzipped "_AD Security_" folder into the "_Virtual Machines_" folder
  
7.  Launch VM Workstation Player and select "_Open a Virtual Machine_"
8.  Point it to the "_AD Security.vmx_" file in the "_My Documents\Virtual Machines\AD Security_" folder
9.  Then select the "_AD Security VM_" and click the play button to start it.
10.  At the logon screen, login as "CORP\_Administrator_"  (The password is provided below.)
11.  Open a command-prompt, and enter "_slmgr /rearm_" to rearm the Windows license, then restart the VM.

https://blog.paramountdefenses.com/2020/06/active-directory-security-lab-virtual-machine.html

## Lab

![Pasted image 20230327154119.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327154119.png)

## Processus d'authentification de base


1. L'utilisateur saisit son nom d'utilisateur et son mot de passe sur son poste de travail.
2. Le serveur génère un nombre aléatoire (défi) et le fourni au client
3. La client dispose du mot de passe, génère le hash de son mot de passe et utilise le hash et le défi fourni par le serveur pour dégenrer une fonction de hachage
4. Avec, il génère un nouveau hash qu'il fourni au serveur
5. le serveur qui détient le hash et le mot de passe va générer la même fonction de hachage et calculer le hash qui en découle
6. si le hash calculé correspond au hash du client, cela dis que chacun à le bon mot de passe sans divulgation de mot de passe sur le réseau 

:::info Important 
> 
> Ce type d'authentification repose sur 3 étapes: 
> 1) Négociation : utilisateur
> 2) Challenge : defi
> 3) Authentification : bon hash grace à la fonction hashage
:::


![Pasted image 20230523120324.png](img/Pasted%20image%2020230523120324.png)
![Pasted image 20230327155015.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327155015.png)

![Pasted image 20230523120334.png](img/Pasted%20image%2020230523120334.png)


:::note 
> 
> - hash = empreinte numérique
> 	- Permet de calculer l'emprunte numérique d'une donnée, en théorie unique (si pas unique = collision de hash)
> 	- ![Pasted image 20230327140127.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327140127.png)
:::

### NTLM 
**NTLM désigne le protocole d’authentification.** `LM (pour Lan Manager hash)` et `NTLM (pour NT Lan Manager)` désignent eux le format du hash. 

*L’appellation « Hash NTLM » est un abus de langage puisque Microsoft l’appelle « NTHash ».*

- `LM` est le format historique. Les mots de passe sont limités à 14 caractères, sur un alphabet restreint. 
	- Il repose sur un algorithme DES ancien et faible. Le calcul du hash ne se fait même pas sur toute la longueur du mot de passe. 
	- Ce dernier est décomposé en deux portions de 7 caractères (voire une portion plus courte pour les mots de passe qui ne font pas 14 caractères), ce qui facilite le travail d'un attaquant. 

- `NTHash` est un format plus récent. 
	- Les mots de passe peuvent aller jusqu'à 255 caractères Unicode. 
	- L'algorithme sur lequel repose le calcul du hash NT est MD4 et il est plus robuste. 
	- L’algorithme de hachage complet : MD4(UTF-16-LE(password)) NTLM désigne donc à la fois un format de hash (par abus de langage) mais également un protocole d’authentification !

Filtre dans Wireshark pour voir le processus d'authentification : `smb2`

![Pasted image 20230327140818.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327140818.png)

## Authentification dans l'active directory

Dans un environnement Active Directory, le processus d'authentification se déroule en plusieurs étapes :

1. L'utilisateur saisit son nom d'utilisateur et son mot de passe sur son poste de travail.
2. Le poste de travail envoie une demande d'authentification au KDC (Key Distribution Center), qui est généralement le contrôleur de domaine.
3. Le KDC vérifie les informations d'authentification et, si elles sont correctes, génère un ticket d'authentification (TGT) pour l'utilisateur. Ce ticket est chiffré avec le secret partagé entre le KDC et l'utilisateur (généralement basé sur le mot de passe de l'utilisateur).
4. Le poste de travail reçoit le TGT et le stocke localement. L'utilisateur est maintenant authentificationentifié auprès du domaine.
5. Lorsque l'utilisateur souhaite accéder à une ressource spécifique (par exemple, un partage de fichiers sur un serveur), le poste de travail envoie une demande de ticket de service (TGS) au KDC, en incluant le TGT précédemment obtenu.
6. Le KDC vérifie le TGT et, s'il est valide, génère un ticket de service (TGS) pour la ressource demandée. Ce ticket est chiffré avec le secret partagé entre le KDC et la ressource (généralement basé sur le mot de passe du compte de la ressource).
7. Le poste de travail reçoit le TGS et l'envoie à la ressource demandée.
8. La ressource déchiffre le TGS à l'aide de son secret partagé, vérifie les informations contenues dans le ticket, et autorise ou refuse l'accès en conséquence.

![Pasted image 20230327160432.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327160432.png)

## Avec Kerberos

Protocole d'authentification au même titre que le NTLM mais protocole utilisé par l'AD. Permet de gérer les services d'annuaire type AD.

Ici on fait intervenir le KDC (DC), centre de distribution des clefs ==(Key Distribution Center)==, qui va garantir l'authentification de l'utilisateur auprès du serveur.
Kerberos repose sur le principe de non divulgation de mot de passe.

Schéma : 
Client qui s'authentifie sur le KDC :
- -> KDC vérifie si l'authentification est OK 
- -> SI OK, il fourni un jeton au client 
:::note 
> 
> - Le KDC renvoi ensuite au client une réponse sous la forme d’un paquet KRB_AS_REP, contenant la clé de session chiffrée avec le hash du mot de passe du compte spécifié par le ==client, ainsi qu’un jeton TGT qui contient le nom d’utilisateur, la période de validité, la clé de session et d’autres informations concernant des informations spécifiques du compte (son identifiant, les groupes auxquels il appartient… etc)==, qu’on appelle le ==**PAC.**==
![Pasted image 20230327170725.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327170725.png)
- -> Le client redonne le jeton pour demander l'accès a une ressource (file ?) 
- -> KDC renvoie un second jetons que le client va présenter au serveur de ressource 
- -> le serveur ressource vérifie que le utilisateur à accès a la ressources (groupes, droits..) 
- -> SI c'est le cas il fourni la ressource
:::

![[Drawing 2023-03-27 15.35.13.excalidraw.png](../../S%C3%A9curit%C3%A9/Authentification/img/Drawing%202023-03-27%2015.35.13.excalidraw.png)

:::info Important 
> Le DC gère L'authentification ET LE SERVEUR DE RESSOURCE GERE L'ACCèS VIA LES PACS AUXQUELS IL APARTIENT.
:::


:::note 
> - Si le utilisateur veut aller sur une ressource en spécifiant l'IP du serveur, c'est le protocole NTLM qui sera utilisé.
> - Si on veux utiliser Kerberos, il faudrat accéder aux ressources via les noms DNS afin que le DC puisse résoudre le **SPN (Service principal name)** et trouver la clef associé pour chiffrer le jetons TGS.
:::

## LSASS & Mimikatz

Pour éviter qu'un utilisateur ne retape son mot de passe a chaque fois, son hash NT ou LM sont stocké dans des fournisseurs de supports de sécurité appelé **SSP.** (Security Support Provider)
Ce sont en des dll, chargés dans le processus LSASS. Entièrement dédié à l'authentification, avec les fournisseurs tel que kerberos ntlm etc...

Le processus système LSASS est exécuté avec le privilège système sur la machine, donc un seul process pour tout le utilisateurs.
Et LSASS stocke les 10 derniers hash de mot de passe par défaut.

Mimikatz permet de récupérer stocké en mémoire.

Le principe de Mimikatz fonctionne sur les privilège `SE debug privilège`, qui est un privilège dont les comptes importants du domaine dispose. 
Permet d'attacher un débuggeur au niveau du noyau ou sur un processus système.
Donc c'est notre cas, le débogueur affectera le processus système LSASS et récupèrerera toute les infos qui s'y trouve en cache 
Donc potentiellement récupérer tout les hash des utilisateurs du système.

:::note 
> Depuis XP jusqu'à Server 2012, WDIGEST a été conçu pour gérer l'authentification sur http.
> Sur ces système le fourniseeur est activé par defaut et tout les mot de passe sont en clair (soucis de retro-comptabilité)
> Donc mimikatz est capable de récupérer des mots de passe en clair le cas échéant.
:::

### NTLM
Dans mimikatz 

```makefile
privilege::debug
sekurlsa::logonpasswords
```

### Kerberos et AD

#### DCSync

En cas de domaine répliqué, on peut se faire passer pour un DC, demander une réplication et récupérer des utilisateurs + hash du mot de passe = DcSync
Il faut avoir les privilège de réplication pour effectuer une attaque DCSync, l’utilisateur doit disposer des privilèges **Replicating Directory Changes All** et **Replicating Directory Changes.**
Les membres des groupes Administrateurs, Administrateurs de domaine, Administrateurs d'entreprise et Contrôleurs de domaine ont ces privilèges par défaut.

#### Pré-authentification Kerberos

- Afin d’obtenir un Jeton TGT, le client fourni l’heure précise de la demande, appelée timestamp, chiffrée avec le hash de son mot de passe personnel, son nom d’utilisateur ainsi que d’autres informations. 
- Malheureusement certaines applications ne prennent pas en charge la préauthentification Kerberos. 
- Il est possible de désactiver cette pré-authentification dans les paramètres du compte dans l’annuaire Active Directory. 
- Sans pré-authentification Kerberos, il n’est pas nécessaire de fournir ces informations. Un attaquant malveillant peut donc envoyer directement une demande fictive d'authentification. 
- Le KDC renverra un TGT chiffré et l'attaquant pourra tenter de trouver le mot de passe de l’utilisateur par force brute de manière hors ligne (puisque la clé utilisée pour chiffrer le jeton TGT est le hash de l’utilisateur).

```bash
cd /impacket/examples
python3 GetNPUsers.py domain.local/Vincent -no-pass -dc-ip 192.168.10.10
```

On récupère ensuite un jetons TGT pour le utilisateur Vincent.
Puis on garde que la ligne sur KR5ASREP (donc jetons TGT)

```bash
Puis bruteforce avec john 
john vincent.hash --wordlist/root/rockyou.txt
```

Le jetons à pu être déchiffré.

#### Responder 

- Lorsque vous tentez d'accéder à un partage réseau, Windows envoi le nom d’utilisateur du compte courant avec son hash ``Net-NTLM vers le serveur.

- Le protocole LLMNR (Link-Local Multicast Name Resolution) viens de vista, son but est que lorsqu'un utilisateur veut aller sur une ressource d'un serveur avec un nom DNS, et que son nom est pas trouvé, le poste va demander à toute les machines du réseau, donc en broadcast, ou se trouve le serveur ? 

- Il est donc possible d'écouter le requêtes LLMNR via une attaque par responder, le but est d'écouter les requêtes LLMNR sur le réseau et d'usurper l'identité d'un serveur dont un utilisateur cherche à accéder à une ressource
- Donc si on se fait passer pour un serveur légitime, la personne voudra s'authentifier auprès de nous et nous pourrons récupérer des informations tel que des hash.
- Puis ensuite bruteforce le hash.
- 
```css
responder -I eth0
```

#### Amélioration de l'attaque par responder, le relai NTLM.



Le principe n'est pas de récupérer comme précédemment mais de relayer pour s'authentifier sur le serveur légitime en se faisant passer pour le client qui voulait s'authentifier.
Fonctionne en deux temps :
1) Ecouter la requête LLMNR avec responder et y répondre pour usurper l'identité d'un serveur légitime et récupérer les info d'ID d'un utilisateur.
2) Ensuite avec `ntlmrelayx.py` (impacket) permettra de relayer les infos du utilisateur vers un vrai serveur sur le domaine pour accéder aux ressources en tant que le utilisateur dont on a pris les accès.

```vbnet
vim /etc/responder/responder.conf
	SMB = OFF
	HTTP = OFF
```

```css
responder -I eth0
# En même temps :
python3 ntlmrelayx.py -t 192.168.1.10 -smbport 
python3 ntlmrelayx.py -smb2support -t 192.168.1.10
```

link :
https://ethicalhackingguru.com/the-complete-ntlm-relay-attack-tutorial/ 
https://beta.hackndo.com/ntlm-relay/

Une fois les scripts lancées, lancer un client Windows 10 et tenter d'accéder à un partage qui n'existe pas.

La requête est maintenant empoisonné.

Nous avons ensuite utilisé les informations d'authentification pour s'authentifier sur le DC.
Ainsi que les IDs enregistrés dans la table SAM du client.

*TGT(cli->srv_kdc) -> TGS(cli->srv_ressource)*

:::info Important 
> Il faut se connecter directement sur le serveur de ressource, pas via un autre serveur de ressource pour que cette méthode fonctionne.
:::

:::note  Notion de délégation sans contrainte.
> - Microsoft a introduit les délégations Kerberos dans l’objectif de permettre à une application de réutiliser l’identité d’un utilisateur pour accéder à une ressource hébergée sur un serveur différent. 
> - Un cas d’usage est par exemple l’accès à des documents hébergés sur un serveur de fichier depuis un serveur SharePoint. 
> - L’utilisateur n’ayant pas d’accès direct au serveur de fichiers, il s’authentificationentifie sur la plateforme SharePoint qui doit alors transmettre l’identité de l’utilisateur au serveur de fichiers. 
> - Lors de cet échange, le jeton TGS transmis au serveur SharePoint contient une copie du jeton TGT de l’utilisateur. 
> - Le serveur SharePoint transmet donc au serveur de fichier le ticket TGT de l’utilisateur. 
> - De cette façon, ce sont les droits de l’utilisateur qui s’appliquent lors de l’accès aux fichiers du serveur de fichiers. 
> - Mais si le serveur SharePoint décide de conserver le jeton TGT, qui identifie l’utilisateur, il peut alors générer des jetons TGS à l’infini, simplement en communiquant avec le KDC, et ainsi accéder à toutes les ressources de tous les serveurs disponibles pour cet utilisateur
:::

![Pasted image 20230523120305.png](img/Pasted%20image%2020230523120305.png)

##### Génération de ticket TGT via impacket :
```bash
python3 getTGT.py domain.local/Vincent -dc-ip 192.168.1.10
cat Vincent.ccache #non affichable
export KRB5CCNAME=vincent.ccache #Utilisation de -k pour utiliser cette variable
python3 psexec.py -k -no-pass srvdc.domain.local #Donc on use la variable d'ENV
# Nous sommes normalement identifié
```

:::info Important 
> - ANSSI recommande de changer le mot de passe du compte KRBTGT du domaine tout les 40 jours
> - Le hash du mot de passe de se compte sert à chiffré et signer tout les jetons TGT
> - Le mot de passe est généré àaléatoirement à la création d'un domaine
> - WARNING : Changer le deux fois pour la synchro pour ne pas conserver d'historique valide
:::

#### Silver Ticket

- Un attaquant qui dispose du hash du compte KRBTGT peut récupérer l'ensemble des hash des comptes du domaine, y compris des comptes machines, et grâce à ces comptes machines que l'ont peut créé des TGS.
- Quand un utilisateur veut accéder à une ressource, il demande TGT au KDC puis demande le TGS pour avoir le droit d'accéder à une ressource précise, le TGS contiendra aussi le PAC de l'utilisateur.
- Ce jetons TGS est chiffré avec le hash du compte machine du serveur qui contient la ressource
- Le  serveur qui contient sa ressource peut donc déchiffrer le TGS et voir si les permissions ou groupes sont ok pour aller à la ressource.
- Si on dispose du hash du compte machine du serveur qui propose la ressource ALORS
- On peut forger notre propre jetons TGS avec toutes les information du PAC
- Donc on peux s'octroyer la totalité des droits des ressources proposé par le serveur si on dispose du hash de son compte machine
- Permet donc d'avoir tout les accès a la ressource
- Elle est appelé génération de **Silver ticket ou Ticket d'argent**
- 
![Pasted image 20230327170531.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327170531.png)

#### Golden Ticket

- Une "amélioration" du ticket d'argent est le **Golden ticket**
- Consiste à récupérer le hash du compte KRBTGT
- Les jetons TGT permettent de créer le PAC de départ qui contient les groupes du utilisateurs.
- Etant donné que nous avons le hash du compte KRBTGT
- Il est possible de créer nos propres jetons TGT avec nos PAC personnalisés.
- Comme par exemple créer un utilisateur qui n'existe pas dans l'annuaire, mais appartiens au *Domain admin* par exemple.
- De cette façon **nous pouvons créer un jetons TGT universel**, avec tout les droits sur toutes les ressources de tout les serveurs du domaine.
- Puis demander de façon légitime au KDC un jetons TGS pour cet utilisateur
- Le KDC ne nous le refusera pas, et donc on a un passe partout sur le domaine.
- ==Donc un **ticket d'o**r==

![Pasted image 20230327170600.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327170600.png)
```
cd /impacket/example
python3 lookupsid.py domaine.local/Administrateur:P@ss0rd@192.168.1.10
```

On récupère avec ceci le SID du domaine, de type : 
```
Domain SID is: S-1-XXXXXXXXXXXXX-XXXXXX-XXXXXXXXXXXX
```

Puis pour récupérer le hash de KRBTGT : 
```
cd /impacket/example
python3 secretsdump.py domaine.local/Administrateur:P@ss0rd@192.168.1.10
```

Récupération du hash LM et NT de KRBTGT, ici on récup le hash NT, puis : 

![Pasted image 20230327152803.png](../../S%C3%A9curit%C3%A9/Authentification/img/Pasted%20image%2020230327152803.png)

```
cd /impacket/example
python3 ticketer.py -nthash XXXXXXXXXXXXXXXXXXX -domain-sid XXXXXXXXXXXXXXXXXXXXXX -domain domaine.local utilisateurhacker
```

Un ticket TGT à été créé pour cet utilisateur. Il n'éxiste pas dans l'annuaire.

```bash
export KRB5CCNAME=utilisateurhacker.ccache
python3 symclient.py -k -no-pass srv-dc.domain.local
```

On s'authentifie en tant que utilisateurhacker sur le serveur DC.
Ce jetons passe partout s'appelle donc un ticket d'or ou golden ticket.

### KRBTGT

Le hash du mot de passe krbtgt peut être obtenu en utilisant un dump des informations d’identification du système d’exploitation pour avoir un accès privilégié à un contrôleur de domaine. Cette capacité est à la fois extrêmement puissante et difficile à détecter.

Plusieurs outils peuvent être utilisés à la fois par des attaquants et les auditeurs de sécurité. Des outils développés spécifiquement mais non publics existent probablement également.
Pour créer et utiliser un Golden Ticket, l’attaquant doit trouver un moyen d’accéder au réseau :
1. Compromettre un ordinateur cible avec un malware qui lui permettra d’utiliser des comptes utilisateur pour accéder à d’autres ressources réseau (souvent à partir d’un e-mail de phishing et l’exploitation d’une vulnérabilité)
2. Compromettre un utilisateur qui dispose des privilèges de réplication ou d’accès d’administrateurs à un contrôleur de domaine. Les membres des groupes Administrateurs, Administrateurs de Contrôleurs de domaine ont ces privilèges par défaut.
3. Se connecter au DC et obtenir via un dump le hash du mot de passe du compte Krbtgt afin de créer le Golden Ticket.

L’attaquant peut utiliser mimikatz, Impacket ou un autre outil d’exploitation de vulnérabilités similaire afin d’extraire le hash du mot de passe à partir d’un dump.

Mimikatz de base est un outil de sécurité offensive incroyablement efficace développée par Benjamin Delpy, cet outil a été créé pour démontrer les vulnérabilités de l’Active Directory de Microsoft. Ses fonctionnalités offrent aux auditeurs et aux attaquants un moyen facile de récupérer les informations d’identification d’un réseau cible. Depuis quelques années Delpy à ajouter une nouvelle fonctionnalité dans Mimikatz pour forger les golden ticket Kerberos, ce qui a donné plus d’avantages aux attaquants.

En utilisant Mimikatz, il est possible d’exploiter les informations de mot de passe du compte krbtgt pour créer des tickets Kerberos falsifiés (TGT) qui peuvent être utilisés pour demander des tickets TGS pour n’importe quel service sur n’importe quel ordinateur du domaine.

Pour créer un TGT particulier du compte admin du domaine, l’attaquant doit spécifier certaines informations pour _mimikatz kerberos::golden_

**Utilisateur** : Le nom du compte utilisateur pour lequel le ticket sera créé. Cela peut être un vrai nom de compte.  
**ID** : Le RID du compte qui sera emprunté. Il peut s’agir d’un véritable ID de compte, tel que l’ID administrateur par défaut de 500, ou un faux ID.  
**Rc4** : le hash NTLM du mot de passe  
**Groupes** : Une liste de groupes auxquels appartiendra le compte du ticket. Cela inclura les administrateurs de domaine par défaut, de sorte que le ticket soit créé avec les privilèges maximum.  
**SID** : le SID du domaine. Ceci est utile pour s’authentifier sur plusieurs domaines  
**Ptt (optionnel)** : pour injecter immédiatement le faux ticket dans la session en cours  
NB : Le golden ticket peut être généré depuis n’importe quelle machine, même hors du domaine

4. Il ne reste qu’à charger le ticket Kerberos.


## Source : 
- https://beta.hackndo.com/ntlm-relay/
- https://processus.site/
- https://learn.microsoft.com/fr-fr/windows-server/identity/ad-ds/manage/how-to-configure-protected-accounts
- https://beta.hackndo.com/
- https://www.youtube.com/watch?v=Z32UhxE29LU
- https://www.conix.fr/kerberos-attaque-golden-ticket/
- https://beta.hackndo.com/kerberos-silver-golden-tickets/
- https://blog.gentilkiwi.com/tag/krbtgt
- https://blog.paramountdefenses.com/2020/05/active-directory-security-for-cyber-security-experts.html
- https://blog.paramountdefenses.com/2020/06/active-directory-security-lab-virtual-machine.html
- https://happycamper84.medium.com/generating-a-golden-ticket-in-the-lab-4740efa22f34
- https://shamsher-khan-404.medium.com/attacking-kerberos-tryhackme-writeup-97cec6b563d2
- https://cybergladius.com/ad-hardening-against-kerberos-golden-ticket-attack/
- https://happycamper84.medium.com/kerberoasting-over-an-open-fire-3b604e4c52f2