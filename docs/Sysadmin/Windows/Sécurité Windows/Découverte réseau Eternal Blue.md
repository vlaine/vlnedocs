# Découverte Eternal Blue 

##### Auteur : Vincent Lainé 

---

:::tip  Introduction
> 
> Les fuites publiques d'outils et d'informations de la NSA ont conduit à la publication d' exploits zero-day auparavant secrets tels que EternalBlue, qui a été utilisé dans la célèbre attaque de rançongiciel WannaCry . Malgré la publication de plusieurs correctifs, de nombreux utilisateurs n'ont pas réussi à mettre à jour leurs systèmes, de sorte que de nombreux appareils sont toujours vulnérables à ces attaques désormais publiques.
> 
> EternalBlue ( [CVE-2017-0144](https://cve.mitre.org/cgi-bin/cvename.cgi?name=cve-2017-0144) ) et EternalRomance ( [CVE-2017-0145](https://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0145) ) ont été révélés à l'origine à la suite de fuites d'outils compromis par la NSA par les Shadow Brokers . Ces exploits ciblaient des erreurs dans l'implémentation du protocole Microsoft Server Message Block (SMB).
> 
> Ces vulnérabilités sont particulièrement précieuses pour les attaquants, car un paquet conçu de manière malveillante permet l'exécution de code à distance, qui pourrait transporter des charges utiles de logiciels malveillants tels que des ransomwares ou une boîte à outils d'accès à distance. Ces exploits ont déjà été utilisés pour distribuer des rançongiciels tels que WannaCry, [Petya/NotPetya](https://www.theregister.co.uk/2017/06/28/petya_notpetya_ransomware/) et [Bad Rabbit](https://arstechnica.com/information-technology/2017/10/bad-rabbit-used-nsa-eternalromance-exploit-to-spread-researchers-say/) .
:::


---

Alors que la plupart des utilisateurs ont déjà mis à jour et que de nombreux appareils connectés à Internet vulnérables à ces exploits ont déjà été attaqués, de nombreux appareils sur des réseaux locaux non directement exposés à Internet peuvent encore être à risque. Tout appareil infecté rejoignant le réseau peut propager l'infection à des appareils vulnérables sur le même réseau, donc être capable d'analyser un réseau ou une plage d'adresses IP pour ces vulnérabilités permet de s'assurer que son propre réseau interne est actuellement protégé contre ces attaques.

Dans ce guide, nous utiliserons [Eternal Scanner](https://github.com/peterpt/eternal_scanner) pour aider à automatiser le processus de recherche des appareils vulnérables. De plus, cet exercice vous aidera à mieux comprendre la méthodologie utilisée par un pirate informatique pour rechercher et attaquer les appareils vulnérables.

---

## 1 - Exigences d'installation

Eternal Scanner utilise un certain nombre d'outils pour rechercher les appareils vulnérables. [Masscan](https://github.com/robertdavidgraham/masscan) est utilisé pour rechercher des appareils dans une plage d'adresses IP, et **Metasploit Framework** est utilisé pour vérifier les vulnérabilités. De plus, [Wget](https://www.gnu.org/software/wget/) est utilisé pour mettre à jour l'outil lui-même. Un certain nombre de modules Python sont également nécessaires pour détecter des vulnérabilités supplémentaires. Pour installer les prérequis sur un système basé sur Debian tel que Kali ou Ubuntu, exécutez la commande ci-dessous dans une fenêtre de ligne de commande.


```bash
sudo apt-get install masscan metasploit-framework wget python3-pip
```

Si les modules Python sont disponibles dans vos référentiels système, ils peuvent être installés en exécutant la commande ci-dessous.

```bash
sudo apt-get install python-crypto python-impacket python-pyasn1-modules
```

:::note 
> 
> Si les modules ne sont pas disponibles dans vos référentiels système, ils peuvent également être installés à l'aide de **[pip](https://pip.pypa.io/en/stable/)** .
```shell
pip install h5py  
pip install typing-extensions  
pip install wheel
pip install crypto impacket pyasn1-modules 
```
:::

Si vous avez ce genre de message d'erreur :

```bash
Installing collected packages: shellescape, Naked, dsinternals, crypto
  WARNING: The script naked is installed in '/home/vlaine/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  WARNING: The scripts crypto and decrypto are installed in '/home/vlaine/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
```

Utilisez cette commande en adaptant à votre chemin : 

```bash
export PATH=$PATH:/home/vlaine/.local/bin
```

:::caution 
> 
> Vous pourriez retrouver cela également :
:::

```shell
ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
crackmapexec 5.2.2 requires bs4<0.0.2,>=0.0.1, which is not installed.
crackmapexec 5.2.2 requires impacket<0.10.0,>=0.9.23, but you have impacket 0.10.0 which is incompatible.
crackmapexec 5.2.2 requires neo4j<5.0.0,>=4.1.1, but you have neo4j 1.7.0.dev0 which is incompatible.
crackmapexec 5.2.2 requires pylnk3<0.4.0,>=0.3.0, but you have pylnk3 0.4.2 which is incompatible.
crackmapexec 5.2.2 requires xmltodict<0.13.0,>=0.12.0, but you have xmltodict 0.13.0 which is incompatible
```
.


Si c'est le cas, cela n'a normalement pas d'incidence, tenter de relancer les commandes d'installation.

![Pasted image 20230125193805.png](../img/images/kali/Pasted%20image%2020230125193805.png)

Une fois les conditions requises installées, nous pouvons maintenant télécharger Eternal Scanner à partir de GitHub.

```bash
git clone https://github.com/peterpt/eternal_scanner
```


```bash
┌──(vlaine㉿kali2)-[~]
└─$ git clone https://github.com/peterpt/eternal_scanner
Cloning into 'eternal_scanner'...
remote: Enumerating objects: 323, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 323 (delta 0), reused 0 (delta 0), pack-reused 320
Receiving objects: 100% (323/323), 107.95 KiB | 2.40 MiB/s, done.
Resolving deltas: 100% (184/184), done.
```


Maintenant, Eternal Scanner est prêt à être installé et exécuté.

## 2 - Utiliser Eternal Scanner

Pour installer Eternal Scanner, changez d'abord de répertoire dans le répertoire cloné Git en utilisant **[cd](https://null-byte.wonderhowto.com/how-to/hack-like-pro-linux-basics-for-aspiring-hacker-part-2-creating-directories-files-0147234/)** .

```bash
cd eternal_scanner
```


Une fois dans le répertoire, Eternal Scanner peut être exécuté à l'aide de la commande suivante.

```bash
sudo ./escan
```


Lors de la première exécution du script, l'outil sera automatiquement installé et peut maintenant être exécuté en entrant simplement **escan** . Pour continuer à utiliser l'outil lors de la première exécution, appuyez simplement sur la touche _Entrée_ .

![Pasted image 20230125194212.png](../img/images/kali/Pasted%20image%2020230125194212.png)

À partir de là, l'écran de démarrage Eternal Scanner devrait s'ouvrir.

![Pasted image 20230125194257.png](../img/images/kali/Pasted%20image%2020230125194257.png)

Afin de scanner votre réseau local, saisissez les trois premiers octets de l'adresse IP de votre réseau local suivis de la plage souhaitée. Pour analyser chaque adresse du sous-réseau, une chaîne telle que **1/24** peut être utilisée. Si vous n'êtes pas certain du format IP de votre sous-réseau, vous pouvez exécuter **[netstat](https://null-byte.wonderhowto.com/how-to/advice-from-real-hacker-know-if-youve-been-hacked-0157336/) -rn** dans une nouvelle fenêtre de terminal et consulter l' adresse IP de la _passerelle affichée._

![Pasted image 20230125202746.png](../img/images/kali/Pasted%20image%2020230125202746.png)

Si tous les systèmes de votre réseau local sont mis à jour, vous ne recevrez probablement pas de cibles vulnérables, ce qui est probablement une bonne chose. Si cela devait être exécuté contre un plus grand ranger IP sur Internet, quelque chose qui touchait plus des 4 294 967 296 adresses IP théoriquement possibles, plus de résultats seraient beaucoup plus susceptibles d'être trouvés, et ces résultats pourraient toujours être très précieux pour un attaquant, même si les appareils avaient déjà été attaqués.

---

Comme le script shell lui-même effectue un certain nombre d'appels directs aux programmes qui ont été installés comme prérequis, nous pouvons également répliquer manuellement un processus similaire en faisant directement des requêtes similaires à ces mêmes outils, y compris Masscan et Metasploit Framework.

##  3 - Faire des requêtes Masscan directement

Pour examiner la requête Masscan, nous pouvons jeter un œil au script shell lui-même. Pour l'ouvrir dans nano, exécutez **nano escan** à partir du dossier qui a été cloné à partir de Git. Pour trouver spécifiquement la requête Masscan, appuyez sur _Ctrl+W_ et appuyez sur _Entrée_ jusqu'à ce que vous atteigniez la chaîne de requête Masscan.

![Pasted image 20230125195338.png](../img/images/kali/Pasted%20image%2020230125195338.png)

La ligne en surbrillance dans l'image ci-dessus applique plusieurs variables définies dans le script.

```bash
masscan "$ip" -p "$port" --rate "$rt" --exclude 255.255.255.255 --output-filename "$mass"
```

Certaines variables, telles que "$port", sont définies au début du script, comme illustré ci-dessous, où la variable "port" est définie sur "455".

![Pasted image 20230125195501.png](../img/images/kali/Pasted%20image%2020230125195501.png)

```
Nous pouvons également remplir manuellement cette même chaîne directement en utilisant nos propres paramètres. La même adresse IP ou plage utilisée précédemment, telle que 192.168.0.0/24, peut remplacer « $ip ». Le ou les ports à analyser, tels que 455, peuvent remplacer directement '$port'. Le taux, ou '$rt,' peut être 
remplacé par 500. Enfin, '$mass' peut être remplacé par le nom de fichier souhaité de la sortie de Masscan. Cette sortie sera formatée en tant que fichier XML, il peut donc être utile de supprimer le paramètre --output-filename de la commande. La chaîne complète peut ressembler à celle ci-dessous.
```


```bash
masscan 192.168.1.0/24 -p 455 --rate 500 --exclude 255.255.255.255 --output-filename out.txt
```


Le paramètre **--exclude** existe à titre préventif dans le script d'origine afin d'établir une confirmation si la plage est jugée trop grande par Masscan. Ceci n'est pas nécessaire pour un balayage à petite portée, mais peut être utile pour des ensembles plus importants.

[!![Pasted image 20230125200516.png](../img/images/kali/Pasted%20image%2020230125200516.png)

Si aucun périphérique n'a de port 455 ouvert, cette analyse ne renverra aucun résultat. Pour tester des ports plus courants, nous pouvons utiliser une chaîne beaucoup plus simple, comme celle ci-dessous.

```bash
masscan 192.168.0.0/24 -p80,23
```


Cette analyse teste également toutes les adresses IP du sous-réseau **192.168.1** et teste spécifiquement le port **80** , utilisé pour HTTP, et le port **23** , utilisé pour Telnet.

![Pasted image 20230125200617.png](../img/images/kali/Pasted%20image%2020230125200617.png)

Une chaîne telle que celle ci-dessous tentera la même analyse que celle illustrée ci-dessus, mais sur l'ensemble d'Internet plutôt que sur un seul sous-réseau. Cela prendra, sur la plupart des appareils et des connexions Internet, beaucoup de temps.

```bash
masscan 0.0.0.0/0 -p80,23 --exclude 255.255.255.255
```


Nous pouvons comparer les résultats de Masscan à la sortie d'un scanner de réseau similaire à portée plus étroite, [Nmap](https://null-byte.wonderhowto.com/how-to/hack-like-pro-advanced-nmap-for-reconnaissance-0151619/) . Cet outil est disponible dans la plupart des référentiels Linux, et sur les systèmes basés sur Debian, il peut être installé à l'aide de la commande ci-dessous.

```bash
sudo apt-get install nmap
```


L'exécution d'une analyse telle que celle définie dans la chaîne ci-dessous exécutera une analyse TCP SYN sur le sous-réseau local et renverra pratiquement tous les ports communs qui peuvent être ouverts sur n'importe quel périphérique connecté au réseau.

```bash
sudo nmap -sS 192.168.1.0/24
```

Ou pour chercher directement avec le script des vulnérabilités : 
```bash
sudo nmap -sC -sV --script vuln 192.168.155.0/24 -O
```

Des exemples de résultats pour une telle analyse sont présentés ci-dessous. Ce résultat d'analyse fournit beaucoup plus de détails que le rapport spécifique au port de Masscan, mais est beaucoup moins efficace sur des plages de réseau plus larges, car il n'a pas été conçu pour analyser l'intégralité d'Internet.

```
┌──(vlaine㉿kali2)-[~/eternal_scanner]
└─$ nmap -A 192.168.1.198
Starting Nmap 7.93 ( https://nmap.org ) at 2023-01-25 19:55 GMT
Nmap scan report for 192.168.1.198
Host is up (0.00067s latency).
Not shown: 989 closed tcp ports (conn-refused)
PORT      STATE SERVICE            VERSION
135/tcp   open  msrpc              Microsoft Windows RPC
139/tcp   open  netbios-ssn        Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds       Windows Server 2008 R2 Enterprise 7601 Service Pack 1 microsoft-ds
3389/tcp  open  ssl/ms-wbt-server?
| rdp-ntlm-info:
|   Target_Name: WIN-TUQ7V1077LH
|   NetBIOS_Domain_Name: WIN-TUQ7V1077LH
|   NetBIOS_Computer_Name: WIN-TUQ7V1077LH
|   DNS_Domain_Name: WIN-TUQ7V1077LH
|   DNS_Computer_Name: WIN-TUQ7V1077LH
|   Product_Version: 6.1.7601
|_  System_Time: 2023-01-26T03:57:04+00:00
|_ssl-date: 2023-01-26T03:57:11+00:00; +7h59m59s from scanner time.
| ssl-cert: Subject: commonName=WIN-TUQ7V1077LH
| Not valid before: 2023-01-25T03:22:36
|_Not valid after:  2023-07-27T03:22:36
49152/tcp open  msrpc              Microsoft Windows RPC
49153/tcp open  msrpc              Microsoft Windows RPC
49154/tcp open  msrpc              Microsoft Windows RPC
49155/tcp open  msrpc              Microsoft Windows RPC
49156/tcp open  msrpc              Microsoft Windows RPC
49157/tcp open  msrpc              Microsoft Windows RPC
49158/tcp open  msrpc              Microsoft Windows RPC
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode:
|   210:
|_    Message signing enabled but not required
|_nbstat: NetBIOS name: WIN-TUQ7V1077LH, NetBIOS user: <unknown>, NetBIOS MAC: 080027bd5185 (Oracle VirtualBox virtual NIC)
| smb2-time:
|   date: 2023-01-26T03:57:04
|_  start_date: 2023-01-26T03:22:33
| smb-os-discovery:
|   OS: Windows Server 2008 R2 Enterprise 7601 Service Pack 1 (Windows Server 2008 R2 Enterprise 6.1)
|   OS CPE: cpe:/o:microsoft:windows_server_2008::sp1
|   Computer name: WIN-TUQ7V1077LH
|   NetBIOS computer name: WIN-TUQ7V1077LH\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2023-01-25T19:57:04-08:00
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_clock-skew: mean: 9h35m58s, deviation: 3h34m39s, median: 7h59m57s

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 89.03 seconds
```


En comprenant comment le script utilise Masscan pour découvrir des adresses IP potentiellement vulnérables, nous pouvons également examiner le fonctionnement du processus de test réel.

## 4 - Utilisation de Metasploit pour tester la vulnérabilité d'EternalBlue

Pour voir comment le script appelle le Metasploit Framework, nous pouvons à nouveau ouvrir le fichier directement.
Pour ce faire à l'aide de nano, exécutez **nano escan** à partir du dossier Eternal Scanner. Pour rechercher la demande msfconsole, appuyez sur _Ctrl+W_ , tapez **msfconsole** et appuyez sur _Entrée_ jusqu'à ce que vous atteigniez le code affiché dans l'image ci-dessous.

![Pasted image 20230125201237.png](../img/images/kali/Pasted%20image%2020230125201237.png)

Chacune des commandes "echo" qui se terminent par ">"$defdir/msf.rc"" sont des commandes qui sont envoyées à la console Metasploit Framework. Bien que ces commandes incluent un certain nombre de variables définies par le script, elles peuvent également être exécutées manuellement par nous. Pour lancer la console Metasploit, entrez simplement **[msfconsole](https://null-byte.wonderhowto.com/how-to/hack-like-pro-metasploit-for-aspiring-hacker-part-1-primer-overview-0155986/)** sur une ligne de commande.

![Pasted image 20230125202049.png](../img/images/kali/Pasted%20image%2020230125202049.png)

Le lancement de Metasploit peut initialement imprimer un certain nombre d'erreurs de connexion à la base de données. Bien que cela ne limite pas nécessairement la fonction requise pour ce didacticiel, l'utilisation de la base de données accélérera l'utilisation de Metasploit lors de la recherche de certains modules. Si vous avez déjà configuré la base de données, elle peut être lancée avec **service start postgresql** ou **systemctl start postgresql** . 

Une fois que Metasploit est en cours d'exécution, nous pouvons continuer à utiliser les commandes telles que définies dans le script d'analyse. La saisie de la commande ci-dessous dans la console Metasploit chargera le module qui peut tester la vulnérabilité EternalBlue.

```bash
use auxiliary/scanner/smb/smb_ms17_010
```


Une fois ce module chargé, nous pouvons afficher plus d'informations à son sujet en tapant **info** et en appuyant sur _Entrée_ .

![Pasted image 20230125202319.png](../img/images/kali/Pasted%20image%2020230125202319.png)

Cela montre qu'il s'agit bien du module de détection recherché, vérifiant la vulnérabilité "SMB RCE", ou EternalBlue. Pour afficher les options de configuration du module, nous pouvons entrer **des options** et appuyer sur _Entrée_ .

![Pasted image 20230125202410.png](../img/images/kali/Pasted%20image%2020230125202410.png)
Cela montre un certain nombre de paramètres de configuration qui ont été automatiquement définis dans le script Eternal Scanner, tels que "RHOSTS", "RPORT" et "THREADS". Ces paramètres de configuration peuvent également être définis manuellement par nos soins. Certaines d'entre elles peuvent déjà avoir des valeurs définies, mais la plus importante, l'adresse cible, n'a probablement pas été définie. Pour définir ce paramètre, on peut utiliser la commande **set** , puis le nom du paramètre, tel que **RHOSTS** , suivi de l'IP que l'on souhaite tester. Il peut s'agir de l'une des mêmes adresses IP découvertes précédemment par Masscan.

```bash
set RHOSTS 192.168.1.198
```


Une fois cette valeur définie, nous pouvons nous assurer que les modifications ont été écrites en exécutant à nouveau **options** .

![Pasted image 20230125202847.png](../img/images/kali/Pasted%20image%2020230125202847.png)

Une fois toutes les options souhaitées définies, la vulnérabilité peut être testée en tapant simplement **exploit** et en appuyant sur _Entrée_ .

![Pasted image 20230125203012.png](../img/images/kali/Pasted%20image%2020230125203012.png)

Si le périphérique n'est pas vulnérable à l'exploit, le module terminera son exécution mais le scanner ne renverra aucun résultat. 

:::info  Info Importante
> 
> Cette même méthodologie peut être appliquée à de nombreux modules Metasploit supplémentaires. De plus, la méthode dans laquelle Eternal Scanner a appelé le Metasploit Framework pour tester ces vulnérabilités pourrait également être appliquée à des modules supplémentaires.
:::


:::info 
> 
> ## Défense contre les attaques basées sur l'analyse
> 
> La première ligne de défense consiste à minimiser le nombre de systèmes vulnérables qui pourraient être découverts en maintenant des systèmes à jour. La numérisation est une épée à double tranchant, et limiter votre exposition implique d'être conscient du type d'empreintes digitales et de la présence sur Internet de vos appareils.
> 
> Cela peut impliquer de vérifier les paramètres de confidentialité et de sécurité de vos appareils, de rechercher vos propres adresses IP sortantes sur des moteurs de recherche tels que [Shodan](https://null-byte.wonderhowto.com/collection/shodan-guides/) , ou même d'utiliser des scanners de ports tels que Nmap et Masscan, comme détaillé dans ce tutoriel, pour être mieux conscient de ce que votre réseau ressemble à un hacker.
> 
> La majorité des attaques automatisées sont dérivées de vulnérabilités publiées publiquement, ce qui incite les pirates à se précipiter pour trouver et exploiter tous les appareils non corrigés. Ces vulnérabilités sont rarement connues du public avant qu'un correctif ne soit disponible, donc la meilleure défense est de bons paramètres de sécurité avec des mises à jour automatiques activées lorsque cela est possible.
:::


:::tip Check 
> 
> ## Analysez votre réseau pour être le premier informé des problèmes
> 
> Bien que notre guide se concentre sur la manière dont un attaquant pourrait utiliser ces outils, les mêmes principes s'appliqueront à toute personne essayant de sécuriser son réseau. Les pirates utilisant des vulnérabilités publiées publiquement ont besoin que les propriétaires du réseau échouent à appliquer les correctifs ou ignorent qu'il y a un problème à résoudre en premier lieu, afin que vous puissiez reprendre ce pouvoir en apprenant à analyser votre réseau. Découvrir une vulnérabilité grave sur votre réseau vous donne d'abord le dessus pour y répondre, donc apprendre à être conscient est la première clé pour vous défendre.
:::

##### Auteur : Vincent Lainé 