Mimikatz
---
Link to [[Authentification Windows - NTLM & Kerberos](Authentification%20Windows%20-%20NTLM%20&%20Kerberos.md) [[Pass The Hash](Pass%20The%20Hash.md) [[Découverte réseau Eternal Blue](D%C3%A9couverte%20r%C3%A9seau%20Eternal%20Blue.md) [[Exploitation Eternal Blue](Exploitation%20Eternal%20Blue.md) [[Active Directory](../Active%20Directory/Active%20Directory.md)

Aide mimikatz : https://adsecurity.org/?page_id=1821 

---

- Commande : `privilege::debug` 
- But : Obtenir des privilèges de débogage sur le système. 
- Procédure : Exécutez cette commande pour vous assurer que Mimikatz fonctionne avec des privilèges de débogage suffisants pour effectuer d'autres actions. 
- Exemple :

```powerhsell
mimikatz # privilege::debug
```

- Commande : `sekurlsa::logonpasswords` 
- But : Extraire les hachages de mots de passe et les informations d'identification stockées en mémoire. 
- Procédure : Exécutez cette commande après avoir obtenu des privilèges de débogage pour récupérer les hachages de mots de passe et les tickets Kerberos. 
- Exemple :

```powerhsell
mimikatz # sekurlsa::logonpasswords
```


- Commande : `lsadump::lsa /inject` 
- But : Extraire les hachages de mots de passe des comptes locaux stockés dans la base de données Security Account Manager (SAM). 
- Procédure : Exécutez cette commande avec des privilèges administratifs pour extraire les hachages des comptes locaux. 
- Exemple :

```powerhsell
mimikatz # lsadump::lsa /inject
```


- Commande : `lsadump::dcsync` 
- But : Extraire les hachages de mots de passe des comptes de domaine à partir d'un contrôleur de domaine Active Directory. 
- Procédure : Exécutez cette commande avec des privilèges de domaine élevés (par exemple, un compte de domaine avec des droits d'administrateur) pour demander les hachages des mots de passe des comptes de domaine. 
- Exemple :

```powerhsell
mimikatz # lsadump::dcsync /user:domain\username
```


- Commande : `kerberos::list` 
- But : Lister les tickets Kerberos actuellement en mémoire. 
- Procédure : Exécutez cette commande pour afficher les tickets Kerberos en mémoire et obtenir des informations sur les sessions d'authentification. 
- Exemple :

```powerhsell
mimikatz # kerberos::list
```


- Commande : `kerberos::ptt` 
- But : Passer un ticket Kerberos (fichier .kirbi) pour accéder à des ressources dans un environnement Active Directory. 
- Procédure : Utilisez cette commande pour charger un ticket Kerberos précédemment volé ou falsifié afin de se faire passer pour un utilisateur et accéder aux ressources du domaine. 
- Exemple :

```powerhsell
mimikatz # kerberos::ptt ticket.kirbi
```


- Commande : `crypto::capi` 
- But : Extraire les clés de chiffrement privées des certificats stockés dans le magasin de certificats Windows. 
- Procédure : Exécutez cette commande pour récupérer les clés privées des certificats installés sur le système. 
- Exemple :
```powerhsell
mimikatz # crypto::capi
```

- 8.  Commande : `dpapi::masterkey` 
- But : Extraire les clés maîtresses DPAPI (Data Protection API) stockées sur le système. 
- Procédure : Exécutez cette commande pour récupérer les clés maîtresses DPAPI, qui sont utilisées pour protéger les données sensibles, telles que les mots de passe et les clés de chiffrement, stockées par les applications Windows. 
- Exemple : 

```powershell
mimikatz # dpapi::masterkey /in:"%APPDATA%\Microsoft\Protect\{SID}\MasterKeyFile"
```

- Commande : `dpapi::cred` 
- But : Déchiffrer les informations d'identification Windows stockées dans les fichiers de données d'identification. 
- Procédure : Exécutez cette commande en utilisant les clés maîtresses DPAPI extraites précédemment pour déchiffrer les informations d'identification stockées dans les fichiers de données d'identification. 
- Exemple : 

```powerhsell
mimikatz # dpapi::cred /in:"%APPDATA%\Microsoft\Credentials\CredentialFile" /masterkey:MasterKeyFile
```

- Commande : `ts::multirdp` 
- But : Activer la fonctionnalité Multi-RDP (Remote Desktop Protocol) sur le système. 
- Procédure : Exécutez cette commande pour modifier la base de registre Windows et permettre à plusieurs sessions RDP de se connecter simultanément au même système. 
- Exemple : 

```powerhsell
mimikatz # ts::multirdp
```
