Powershell Notes
---

Créer un user dans l'AD avec mot de passe en input : 
```powershell
New-ADUser -Name "Jack Robinson" -GivenName "Jack" -Surname "Robinson" -SamAccountName "J.Robinson" -UserPrincipalName "J.Robinson@enterprise.com" -Path "OU=Managers,DC=enterprise,DC=com" -AccountPassword(Read-Host -AsSecureString "Input Password") -Enabled $true
```

Créer un user avec mot de passe dans le script
```powershell
New-ADUser -Name "Jack Robinson" -GivenName "Jack" -Surname "Robinson" -SamAccountName "J.Robinson" -UserPrincipalName "J.Robinson@vlne.lan" -Path "OU=Managers,DC=vlne,DC=lan" -AccountPassword(ConvertTo-SecureString "P@ssw0rd" -AsPlainText -force) -Enabled $true 
```
