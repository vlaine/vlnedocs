GPO - Troubleshooting
---

![Pasted image 20230227105130.png](../../S%C3%A9curit%C3%A9/img/Pasted%20image%2020230227105130.png)

8007071a error
https://stefanos.cloud/kb/how-to-resolve-group-policy-error-codes-8007071a-and-800706ba/
- Désactiver firewall
- Désactiver le protections Windows Defender


## confirmGC was not recognized

`The specified argument 'ConfirmGc' was not recognized.`
https://rdr-it.com/en/active-directory-add-a-domain-controller-to-powershell/

A tester : 
```powershell
- Install-WindowsFeature AD-Domain-Services -IncludeManagementTools
- Install-ADDSDomainController -DomainName 'xxxx' -SafeModeAdministratorPassword $Secure_String_Pwd -Debug
- Install-ADDSDomainController -domainname laine.local -installdns -noglobalcatalog
- Install-ADDSDomainController -domainname laine.local -installdns 
- Install-ADDSDomainController -DomainName "domain.tld" -InstallDns:$true -Credential (Get-Credential "DOMAIN\administratreur")
```

Soucis avec [[Active Directory](../Active%20Directory/Active%20Directory.md)


## 80070005 Access Denied

- https://social.technet.microsoft.com/Forums/en-US/b9580d92-1a29-4639-9d57-cb41d14ca2c4/gpo-files-access-denied-0x80070005?forum=winserverGP
- https://www.mysysadmintips.com/windows/servers/400-error-0x80070005-trying-to-push-files-via-group-policy

1) Partage avec all rights (a la racine du C dans un dossier dédiée)
2) Ne pas avoir de dossier parents qui aient des permissions spécifique
3) Dans le GPO files (pour le wallpaper)
	1) Hiiden, Archive, Create
		1) Source : Dossier partagé
		2) Dest : C:\wallapperclient\image.png
4) GPO (dans users) Desktop Wallpaper : Mettre un dossier partagé

```
EVERYONE: read AUTHENTICATED USERS: read DOMAIN COMPUTERS: read MYDOMAIN\MYUSERNAME: read and write other users and groups which is not significant to report here.

**SOLVED**: I understood what the problem was.  

The script is at this level: \\<servername>\Share1\Subfolder\Subfolder2\Script.cmd

I set the **AUTHENTICATED USERS** read permission only at **Subfolder2**, but the top-level parent (Share1) had not it. Now it is working, but I must say this solution (create a read permission for all authenticated users for that top-level directory) does not convince me much.
```
