Windows Server 2022
---
# Installation

Voir ISO : SW_DVD9_Win_Server_STD_CORE_2022_64Bit_English_DC_STD_MLF_X22-74290.iso 



![Pasted image 20230223084529.png](../img/Pasted%20image%2020230223084529.png)

![Pasted image 20230223084629.png](../img/Pasted%20image%2020230223084629.png)
Desktop Experience : Avec interface graphique
Sinon c'est version Core (PowerSHell)

![Pasted image 20230223084731.png](../img/Pasted%20image%2020230223084731.png)


![Pasted image 20230223084752.png](../img/Pasted%20image%2020230223084752.png)
Sélectionner : "Custom"

![Pasted image 20230223084817.png](../img/Pasted%20image%2020230223084817.png)

![Pasted image 20230223084835.png](../img/Pasted%20image%2020230223084835.png)

## Premières configurations

Mot de passe Admin : 8 caractères, CHIFFRES, MAJ, MIN
![Pasted image 20230223085713.png](../img/Pasted%20image%2020230223085713.png)

Installer les VMWARES TOOLS
![Pasted image 20230223085912.png](../img/Pasted%20image%2020230223085912.png)

![Pasted image 20230223090000.png](../img/Pasted%20image%2020230223090000.png)

![Pasted image 20230223090040.png](../img/Pasted%20image%2020230223090040.png)

![Pasted image 20230223090056.png](../img/Pasted%20image%2020230223090056.png)

![Pasted image 20230223090145.png](../img/Pasted%20image%2020230223090145.png)

### Configuration initiale
Après redémarrage, reconnectez vous puis aller dans Local Server. Les configurations suivantes seront à faire sur TOUTES LES MACHINES.

![Pasted image 20230223090521.png](../img/Pasted%20image%2020230223090521.png)

#### Changement de nom de la machine

![Pasted image 20230223090658.png](../img/Pasted%20image%2020230223090658.png)

![Pasted image 20230223090713.png](../img/Pasted%20image%2020230223090713.png)

![Pasted image 20230223090723.png](../img/Pasted%20image%2020230223090723.png)
Redémarrer la machine directement.

#### Désactiver le firewall

![Pasted image 20230223091004.png](../img/Pasted%20image%2020230223091004.png)

![Pasted image 20230223091019.png](../img/Pasted%20image%2020230223091019.png)

![Pasted image 20230223091035.png](../img/Pasted%20image%2020230223091035.png)

![Pasted image 20230223091046.png](../img/Pasted%20image%2020230223091046.png)

![Pasted image 20230223091057.png](../img/Pasted%20image%2020230223091057.png)

#### Activer Remote management et Remote desktop

![Pasted image 20230223091149.png](../img/Pasted%20image%2020230223091149.png)

#### Désactiver l'installation de mises à jours automatiques

Cela devrait être comme ceci par défault.
![Pasted image 20230223091302.png](../img/Pasted%20image%2020230223091302.png)

#### Configuration de l'IP fixe
![Pasted image 20230223091433.png](../img/Pasted%20image%2020230223091433.png)

![Pasted image 20230223091529.png](../img/Pasted%20image%2020230223091529.png)

Nous pouvons maintenant passer aux installations.

Voir [[Active Directory](../Active%20Directory/Active%20Directory.md) ou d'autres services Windows.

Activation Windows :

```Powsershell
slmgr /skms kms.digiboy.ir
slmgr /ato
```


:::caution 
> 
> Vérifier que votre heure est bien la même sur toutes les machines.
:::


:::tip To Do
> Créer la zone reverse dans le DNS.
:::


#### Troubleshooting

Il peut arriver parfois que la machine cliente ai des problèmes pour résoudre le nom du serveur DNS. Si c’est le cas, pour pallier ce problème, il faut ajouter des clefs de registre à la machine. Cette procédure fonctionne également pour les serveurs.

Rendez-vous donc dans le regedit.

Dans KEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters et ajouter ces deux clés de registre DWORD 32 bits.  
  
DomainCompatibilityMode avec la valeur 1   
DNSNameResolutionRequired avec la valeur 0

![Pasted image 20230223101415.png](../img/Pasted%20image%2020230223101415.png)

![Pasted image 20230223101421.png](../img/Pasted%20image%2020230223101421.png)


Voir : [[Active Directory](../Active%20Directory/Active%20Directory.md)