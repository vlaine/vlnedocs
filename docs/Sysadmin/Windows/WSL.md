WSL
---

### Installation WSL

https://learn.microsoft.com/fr-fr/windows/wsl/install
https://korben.info/installer-wsl2-windows-linux.html
https://korben.info/linux-wsl-gui-interface-graphique-windows-10.html

### All tools kali for WSL2
https://simontaplin.net/2019/03/29/how-to-install-all-the-penetration-testing-tools-in-kali-linux-for-windows-subsystem-for-linux/ 

```bash
sudo apt-get install kali-linux-default
```

https://tools.kali.org/kali-metapackages
https://tools.kali.org/tools-listing

Avoir le bureau : https://www.kali.org/docs/wsl/win-kex/ 

##### Montage BTRFS (ou autre) WSL

https://learn.microsoft.com/en-us/windows/wsl/wsl2-mount-disk

### Réactiver hyper-v pour WSL
```powershell
 Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

## Régler problème internet WSL

https://github.com/microsoft/WSL/issues/4731
https://gist.github.com/mandeepsmagh/f1d062fc59e4e6115385c2609b5f0448
https://www.tecklyfe.com/how-to-fix-no-internet-connection-in-wsl/

- Désactiver toutes les interfaces réseau (Wi-Fi, VPN) à l'exception de l'interface que je veux qu'Hyper-V utilise (Ethernet)
- `netsh int ip reset`, `netsh winsock reset`
- Désinstaller Hyper-V, WSL et tous les composants associés, redémarrer, réinstaller les composants.
- Arrêter les services `cmdservice`, `hns`, `vmcompute`, `lxssmanager` et redémarrer ces services.

Restart les services de containers : 
```powershell
sc stop cmservice
sc stop hns
sc stop vmcompute
sc stop lxssmanager
netsh winsock reset
netsh int ip reset all
netsh winhttp reset proxy
ipconfig /flushdns
sc start cmservice
sc start hns
sc start vmcompute
sc start lxssmanager
```

Etaindre WSL : 
```
wsl --terminate Ubuntu
wsl --shutdown
```

Relancer une machine.

Script pour toute relancer : 
```powershell
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process (Get-Process -Id $PID | Select-Object -ExpandProperty Path) "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

Write-Output "Shutting down WSL..."
wsl --shutdown

Write-Output "Stopping services..."
Stop-Service cmservice
Stop-Service hns
Stop-Service vmcompute
Stop-Service lxssmanager

Write-Output "Starting services..."
Start-Service lxssmanager
Start-Service vmcompute
Start-Service hns
Start-Service cmservice

Write-Output "DONE!"
Read-Host "Press ENTER to continue..."
```

Dans windows, créer %USERPROFILE%\.wslconfig
Désactiver le cryptage et la compression des fichiers :  
pour `%TEMP%` - le fichier d'échange qui s'y trouve par défaut  
Ou spécifier où sauvegarder dans `%USERPROFILE%\N- wslconfig` et désactiver à cet endroit :
```css
[wsl2]
...
swapFile = E:\\Temp\\swap.vhdx
...
```


Disable encryption and compression on directories with distributions:

```
%appdatalocal%\Packages\CanonicalGroupLimited.Ubuntu*
%appdatalocal%\Packages\TheDebianProject.DebianGNULinux* 
```

### Solution 2

#### Le resolvonf
- Effectuez les étapes ci-dessus pour modifier le serveur de noms dans le fichier /etc/resolv.conf.
- Exécutez ces deux commandes qui créeront un fichier /etc/wsl.conf et ajouteront quelques paramètres :  
    **sudo bash -c 'echo "[network]" > /etc/wsl.conf'**  
    **sudo bash -c 'echo "generateResolvConf = false" >> /etc/wsl.conf'**
- Dans une invite de commande administrateur, exécutez **wsl -shutdown**.
- Redémarrez votre image WSL Linux et vous devriez toujours avoir accès à Internet.
- 
1.  Inside WSL2, create or append file: /etc/wsl.conf
    
2.  Put the following lines in the file in order to ensure the your DNS changes do not get blown away
    
    ```
    sudo tee /etc/wsl.conf << EOF
    [network]
    generateResolvConf = false
    EOF
    ```
    
3.  **In a** `cmd` **window** (!!), run `wsl --shutdown`
    
4.  Start WSL2
    
5.  Run the following inside WSL2 (line with `search` is optional)
    
    ```
    sudo rm -rf /etc/resolv.conf
    sudo tee /etc/resolv.conf << EOF
    search yourbase.domain.local
    nameserver 8.8.8.8
    nameserver 1.1.1.1
    EOF
    ```

## Solution 3

Fix github script test : 
```
Assuming you have followed all the steps to install / setup WSL2 -> https://docs.microsoft.com/en-us/windows/wsl/install-win10

**Tested on Ubuntu 20.04**

Step 1 - Find out default gateway and DNS servers
- Navigate to `Control Panel\Network and Internet\Network Connections`
- Right click on relevant connection type WiFi or Ethernet and select `Status`
- Status screen will be displayed, click on `Details` button
- Network Connection details screen will be displayed
- Note down `IPv4 default gateway` and `IPv4 DNS Servers` if available

                       **OR**
- Run following command from powershell or cmd
  `ipconfig /all`
- Note down `Default Gateway` and `DNS Servers` for relevant connection type WiFi or Ethernet

Step 2 - Create `wsl.conf` file
- Login to WSL2 and create sudo vi `/etc/wsl.conf` with following content
``
[network]
generateResolvConf = false

``
- Save file.

Step 3 - Delete current `resolv.conf` file
`sudo rm /etc/resolv.conf`

Step 4 - Create new custom `resolv.conf` file
- Use below command to create new file
`sudo vi /etc/resolv.conf`
- enter default gateway and any `IPv4 DNS servers` like below example

``
nameserver 192.168.1.254
nameserver 192.165.1.251

``
- Save file

Step 5 - Restart WSL2
- Open powershell and run following command
`wsl --shutdown`

Step 6 - Test internet connection in WSL2 
- ping google.com, it should work now.
```