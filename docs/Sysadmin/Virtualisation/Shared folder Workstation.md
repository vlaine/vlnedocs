Shared folder
---

# Workstation 

- Avoir créé le dossier dans Windows
- L'ajouter dans les settings de la VM

![Pasted image 20230322162342.png](img/Pasted%20image%2020230322162342.png)

- Créer le dossier /mnt/test dans la VM puis lui donner tout les droits.
```bash
mkdir /mnt/test
chmod 777 /mnt/test
```

Puis monter le dossier dans la VM : 

```bash
sudo /usr/bin/vmhgfs-fuse .host:/TESTDOSSIER /mnt/test -o subtype=vmhgfs-fus
e,allow_other
```

Vérifier avec `df` :

```
vmhgfs-fuse                        976117436  761556060 214561376  79% /mnt/test
```

# VirtualBox

- Avoir créé le dossier dans Windows
- L'ajouter dans les settings de la VM

![Pasted image 20230322162437.png](img/Pasted%20image%2020230322162437.png)

![Pasted image 20230322162543.png](img/Pasted%20image%2020230322162543.png)