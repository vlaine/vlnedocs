OVF - OVA
---

---


### Convertir OVF to OVA : 
```
ovftool /ovfs/my_vapp.ovf /ovfs/my_vapp.ova
```

- Corriger l'erreur : `This issue occurs because the vSphere Client does not support the SHA256 hashing algorithm, which the latest VIC OVA was made of (from 1.1.1).`

```
ovftool.exe --shaAlgorithm=SHA1 \path\to\the\original\ova_file.ova \path\to\the\new\ova\file-SHA1.ova
```

Ceci permet de regénérer la signature pour faire fonctionner l'import d'un OVF to OVA via Workstation --> Vers un ESXI

![Pasted image 20230328173457.png](img/Pasted%20image%2020230328173457.png)
Appuyer sur OUI. Ca devrait de lancer.

##### OVF Tools install

https://docs.vmware.com/en/VMware-Telco-Cloud-Operations/1.4.0/deployment-guide-140/GUID-95301A42-F6F6-4BA9-B3A0-A86A268754B6.html
```undefined
sudo apt-get install libfontconfig1 fontconfig libfontconfig1-dev
sudo ./VMware-ovftool-4.3.0-7948156-lin.x86_64.bundle
```

1.  Add the following line to your .bashrc file:
    
    export PATH=$PATH:/home/worker/ovftool
    
    Notice the added .bashrc file line at the bottom.
```bash
    $ cat .bashrc
    # .bashrc
     
     
    # Source global definitions
    if [ -f /etc/bashrc ]; then
        . /etc/bashrc
    fi
     
     
    # Uncomment the following line if you don't like systemctl's auto-paging feature:
    # export SYSTEMD_PAGER=
     
     
    # User specific aliases and functions
    export PATH=$PATH:/home/worker/ovftool
```

```bash
ans-vag:~# ovftool --version
VMware ovftool 4.3.0 (build-7948156)
ans-vag:~#
```

##### Troubleshooting

Error :
```
./xxx: symbol lookup error: /lib64/libfontconfig.so.1: undefined symbol: FT_Done_MM_Var
```

same issue,  
work for me:

```
if [ -L /usr/lib64/libfreetype.so ];then
    export 'LD_PRELOAD=/usr/lib64/libfreetype.so' 
fi
exec my_c++_bin
```

```
LD_PRELOAD=/usr/lib/libfreetype.so ./AppRun
```


Il faut qu'une fenetre puisse s'afficher, donc si la commande sudo ./VMware[...].bundle ne fonctionne pas, il faut activer le X11 forwarding (en ssh) : https://www.cyberciti.biz/faq/x11-connection-rejected-because-of-wrong-authentication/

```shell
apt install xauth
sudo mkdir /root/.Xauthority
mkdir /home/vagrant/.Xauthority

export DISPLAY=localhost:0.0

sudo ./VMware-ovftool-4.3.0-7948156-lin.x86_64.bundle
Extracting VMware Installer...done.
```

![Pasted image 20230512153017.png](img/Pasted%20image%2020230512153017.png)

![Pasted image 20230512153032.png](img/Pasted%20image%2020230512153032.png)