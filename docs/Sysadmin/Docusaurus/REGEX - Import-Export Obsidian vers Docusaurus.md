REGEX - Import-Export Obsidian vers Docusaurus
----

#### Example 1
:::warning
:::

```diff
A remplacer dans VSCODE avec 
- :::(\w+)"*
	- vers
- :::$1
```

Puis pour la fermeture :
La fermeture ne nécessaires pour obsidian, mieux vaut prévoir juste un saut de ligne a la place de :::

```diff
A remplacer dans VSCODE avec 
- :::
	- vers
- ```
```

Voir [[Test admonition]]

---

Convertion depuis le fichier original d'obsidian, vers la syntaxe Docusaurus.
1) Remplacer les admonitions

Dans Obsidian, plugin admonition, convertir tout en callout
Donc : 
```
:::warning
**ATTENTION AU GUILLEMET SIMPLE, SI VOUS FAITE UN COPIER COLLER, N’OUBLIEZ PAS DE VERIFIER LES GUILLEMETS, ILS DOIVENT ETRE CORRECTEMENT ENTREES.**
:::

Deviendra : 

```
[!warning]
```

Attention aussi a remplacer toutes les tabulations

```
 > 
```
vers rien si nécessaire.

Ensuite convertir le 
[!warning] 
en
:::warning
avec le fin en ::: aussi

vers rien si nécessaire.

Puis dans vscode :

Exemple : 

```
> :::note
>
>En gros, les collections Ansible ressemblent à des rôles Ansible, mais beaucoup plus que cela. Dans un rôle Ansible, vous avez des éléments tels que des variables, des gestionnaires, des tâches, des modèles de fichiers, etc.
>
>Mais dans une collection Ansible, vous avez plus d'éléments tels que des modules, des plugins, des filtres et des rôles Ansible. Il n'est pas garanti que chaque collection aura tous les éléments que vous avez expliqués précédemment, cela dépend de l'objectif de la collection et du type de contenu.
:::


Remplacer : 
A test : 
```regex
(\[!hint\])\n([^>]*>[^]*?)\n\n
(\[!hint\])\n\n([^>]*)\n(>[^]*)
```
 
 Good one :
```
>\s*\[!hint\]([\s\S]*?)(?=\n\n)
>\s*\[!hint\]([\s\S]*?)(?=\n\n)
>\s*\[!(\w+)\]([\s\S]*?)(?=\n\n) #OK
```

 Remplacer par : 
 ```
:::hint\n\n> $1\n\n:::
:::hint\n> $1\n:::
:::hint $1\n:::
:::$1\n\n> $2\n\n:::
:::$1 $2\n::: #OK
```

##### Format des images pour Docusaurus
Obsidian initial : 
```
![[Draw Nagios Object Diagram.png]]
```
Pour Docusaurus : 
```
![img/c3b9313faaa3a226b7ebb8ce762913a3_MD5.png](img/c3b9313faaa3a226b7ebb8ce762913a3_MD5.png)#lien vers image final
```

REGEX dans VSCODE pour intégrer : 
```
!\[\[(.*?)\]\]
![$1]($1)
```
