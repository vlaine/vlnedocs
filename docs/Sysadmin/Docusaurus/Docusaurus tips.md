  ---
  sidebar_position: 1
  ---

  # Ceci est un H1

  ## Ceci est un H2

  ### Ceci est un H3

  **Texte en gras**

  *Texte en italique*

  Ceci est une liste :

  * Lorem
  * ipsum

  > Ceci est un citation
```
  # Ceci est un H1

  ## Ceci est un H2

  ### Ceci est un H3

  **Texte en gras**

  *Texte en italique*

  Ceci est une liste :

  * Lorem
  * ipsum

  > Ceci est un citation

```


:::danger Ceci est le titre du bloc !
Ceci est un blog de **danger**
:::

:::success
Ceci est un blog de **success**
:::
  
:::note

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::

:::tip

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::

:::info

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::

:::caution

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::

:::danger

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::  

```
:::danger Ceci est le titre du bloc !
Ceci est un blog de **danger**
:::

:::success
Ceci est un blog de **success**
:::
  
:::note

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::

:::tip

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::

:::info

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::

:::caution

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::

:::danger

Un peu de **contenu** avec la `syntaxe` _Markdown_. Consultez [cette `api`](#).

:::  
```

##Langage pris en charge de base :

```

// @flow

// These are the languages that'll be included in the generated

// prism/index.js file

module.exports = {

markup: true,

bash: true,

clike: true,

c: true,

cpp: true,

css: true,

"css-extras": true,

javascript: true,

jsx: true,

"js-extras": true,

"js-templates": true,

coffeescript: true,

diff: true,

git: true,

go: true,

graphql: true,

"markup-templating": true,

handlebars: true,

json: true,

less: true,

makefile: true,

markdown: true,

objectivec: true,

ocaml: true,

python: true,

reason: true,

sass: true,

scss: true,

sql: true,

stylus: true,

tsx: true,

typescript: true,

wasm: true,

yaml: true,

};```
```

###Adtionnal language highlight

```js docusaurus.config.js
    prism: {

        theme: lightCodeTheme,

        darkTheme: darkCodeTheme,

        additionalLanguages: ['php','bash','powershell','nginx','markdown','makefile','json','http','git','docker','css','yaml','sql'],
```

```css src/css/docusaurus-admonition.css
/**
 Docusaurus-like styling for `remarkable-admonitions` blocks
 */

.admonition {
  margin-bottom: 1em;
  padding: 15px 30px 15px 15px;
}

.admonition h5 {
  margin-top: 0;
  margin-bottom: 8px;
  text-transform: uppercase;
}

.admonition-icon {
  display: inline-block;
  vertical-align: middle;
  margin-right: 0.2em;
}

.admonition-icon svg {
  display: inline-block;
  width: 22px;
  height: 22px;
  stroke-width: 0;
}

.admonition-content > :last-child {
  margin-bottom: 0;
}

/** Customization */
.admonition-warning {
  background-color: rgba(230, 126, 34, 0.1);
  border-left: 8px solid #e67e22;
}

.admonition-warning h5 {
  color: #e67e22;
}

.admonition-warning .admonition-icon svg {
  stroke: #e67e22;
  fill: #e67e22;
}

.admonition-tip {
  background-color: rgba(46, 204, 113, 0.1);
  border-left: 8px solid #2ecc71;
}

.admonition-tip h5 {
  color: #2ecc71;
}

.admonition-tip .admonition-icon svg {
  stroke: #2ecc71;
  fill: #2ecc71;
}

.admonition-caution {
  background-color: rgba(231, 76, 60, 0.1);
  border-left: 8px solid #e74c3c;
}

.admonition-caution h5 {
  color: #e74c3c;
}

.admonition-caution .admonition-icon svg {
  stroke: #e74c3c;
  fill: #e74c3c;
}

.admonition-important {
  background-color: rgba(52, 152, 219, 0.1);
  border-left: 8px solid #3498db;
}

.admonition-important h5 {
  color: #3498db;
}

.admonition-important .admonition-icon svg {
  stroke: #3498db;
  fill: #3498db;
}

.admonition-note {
  background-color: rgba(241, 196, 15, 0.1);
  border-left: 8px solid #f1c40f;
}

.admonition-note h5 {
  color: #f1c40f;
}

.admonition-note .admonition-icon svg {
  stroke: #f1c40f;
  fill: #f1c40f;
}
```