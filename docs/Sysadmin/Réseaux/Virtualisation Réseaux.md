# Travaux pratique - Virtualisation Réseaux

---


![Pasted image 20230218141736.png](img/Pasted%20image%2020230218141736.png)

- Cours : Virtualisation des réseaux

# Objectif

- A l'issue de ce document, l'objectif est d'obtenir une infrastructure similaire à celle ci dessous.  Elle pourra être mise en œuvre au choix sur ESXi, Workstation, ou VirtualBox. **Il est fortement recommandé d'utiliser Workstation.** Car les [Réseaux dans Virtualbox](R%C3%A9seaux%20dans%20Virtualbox.md) ne sont pas très adaptés.
- Les ranges IP des réseaux sont des exemples.

![Pasted image 20230218142155.png](img/Pasted%20image%2020230218142155.png)

## Partie 1 - Mise en œuvre initiale

![Pasted image 20230218142400.png](img/Pasted%20image%2020230218142400.png)

### Configuration des réseaux dans VMware Workstation

Le réseau ADMIN va servir pour se connecter à l’interface du PFsense à partir de votre PC hôte.

A noter qu’en entreprise, il faut privilégier un réseau d’administration séparé du réseau utilisateur.

Modifier les réseaux dans VMware Workstation

![Pasted image 20230218142428.png](img/Pasted%20image%2020230218142428.png)

Cliquer sur"Change Settings" pour avoir accès à toutes les fonctions.

![Pasted image 20230218142437.png](img/Pasted%20image%2020230218142437.png)

Cliquer sur"Add Network".

![Pasted image 20230218142454.png](img/Pasted%20image%2020230218142454.png)

Sélectionner une carte réseau virtuelle disponible.

![Pasted image 20230218142506.png](img/Pasted%20image%2020230218142506.png)

Désactiver le DHCP et configurer le réseau du LAN du site1.

![Pasted image 20230218143542.png](img/Pasted%20image%2020230218143542.png)

Ajouter une autre carte virtuelle et configurer là pour le réseau de la DMZ du site1 sans DHCP.

![Pasted image 20230218143617.png](img/Pasted%20image%2020230218143617.png)

Il faut bien noter la correspondance des réseaux avec les cartes virtuelles :

- DMZ : 192.168.201.0/24 : vmnet3

- LAN : 192.168.200.0/24 : vmnet2

- ADMIN : 192.168.248.0/24 : vmnet1

Vous pouvez aussi renommer les cartes virtuelles.

Supprimer les IP configurées sur votre machine hôte pour les cartes correspondant au réseau DMZ(vmnet3) et LAN(vmnet2) :

![Pasted image 20230218143639.png](img/Pasted%20image%2020230218143639.png)

Passer les en DHCP pour qu’aucune IP ne leur soit allouée.

### Configuration des machines virtuelles

Remplacer les cartes réseaux virtuelles des machines Ubuntu pour les mettre dans les bons réseaux.

![Pasted image 20230218143657.png](img/Pasted%20image%2020230218143657.png)

Par exemple, la machine en DMZ doit être avoir la carte réseau vmnet3 :

![Pasted image 20230218143708.png](img/Pasted%20image%2020230218143708.png)

Configurer la machine dans le LAN.

Connecter vous sur les machines pour changer les adresses IP.

Ne pas oublier de faire"`sudo -s`"

Editer le fichier *.yaml situé dans `/etc/netplan`

network:
```vim
network:
  ethernets:
    ens33:
      addresses:
        - 192.168.200.1/24
      nameservers:
        addresses: [1.1.1.1, 8.8.8.8]
      routes:
        - to: default
        via: 192.168.200.254            
  version: 2
```


:::caution 
> -  Ce fichier ne supporte pas les tabulations !!
> - Le nom de la carte Ethernet ne doit pas être changée, elle doit correspondre à votre configuration (ici elle s’appelle"ens33")
> - Respecter les espaces (après"addresses:"...)
:::


Enregistrer le fichier et appliquer la configuration réseau en tapant :

`netplan apply`

Vérifier que le système ne vous renvoie pas un message d’erreur. Si c’est le cas, corrigé votre fichier.

Redémarrer le réseau :

- `systemctl restart systemd-networkd`
- ou 
- `systemctl restart networking`

Un redémarrage complet de la machine remplace cette dernière commande.

### Installation de Pfsense

Créer une nouvelle machine dans VMware Workstation.

![Pasted image 20230218144323.png](img/Pasted%20image%2020230218144323.png)

Sélectionner le fichier ISO de Pfsense :

![Pasted image 20230218144340.png](img/Pasted%20image%2020230218144340.png)

Workstation détecte automatiquement l’OS

Sélectionner un disque de 10G et 512M de mémoire et ajouter une 2ème carte réseau configurée sur le réseau ADMIN, la première étant par défaut en NAT.

Dérouler l’installation de Pfsense en sélectionnant le clavier Français.

![Pasted image 20230218144347.png](img/Pasted%20image%2020230218144347.png)

Pendant l’installation, n’oublier pas de sélectionner le disque avec la barre d’espace

A la fin de l’installation, sélectionner NO (pas d’ouverture de Shell) et reboot

La configuration se lance après le démarrage :

![Pasted image 20230218144359.png](img/Pasted%20image%2020230218144359.png)
Taper N  (pas de VLAN)

![Pasted image 20230218144408.png](img/Pasted%20image%2020230218144408.png)
Taper a (autodetection)

![Pasted image 20230218144435.png](img/Pasted%20image%2020230218144435.png)
Le système vous dit de connecter le câble réseau.

Aller dans les paramètres de la machine virtuelle et assigner les cartes réseau.

Par défaut, il y a qu’une seule carte réseau.

![Pasted image 20230218144457.png](img/Pasted%20image%2020230218144457.png)

Ajouter 3 cartes.

:::caution 
> 
> **Pour faciliter l’identification des cartes, ajoutez les cartes une par une !**
:::

![Pasted image 20230218144523.png](img/Pasted%20image%2020230218144523.png)

La première servira de WAN (NAT avec votre PC pour l’accès internet)

- La 2ème au LAN (VMNET2)
- La 3ème à la DMZ (VMNET3)
- La 4ème à l’admin (VMNET1)

Retourner dans l’installation de Pfsense. Redémarrer la machine pour qu’elle détecte les nouvelles cartes

La configuration se relance.

![Pasted image 20230218144547.png](img/Pasted%20image%2020230218144547.png)

Appuyer sur Enter et taper em0 pour le nom de l’interface WAN

Redémarrer la machine pour qu’elle détecte les nouvelles cartes

La configuration se relance.

Sélectionner 1 pour assigner les interfaces.

![Pasted image 20230218144612.png](img/Pasted%20image%2020230218144612.png)
Pas de VLAN.

![Pasted image 20230218144632.png](img/Pasted%20image%2020230218144632.png)
- WAN : em0
- LAN : em1
- OPT1 : em2
- OPT2 : em3
![Pasted image 20230218144702.png](img/Pasted%20image%2020230218144702.png)

La configuration a été appliquée.
![Pasted image 20230218144717.png](img/Pasted%20image%2020230218144717.png)

Configurer une adresse IP au LAN. A noter que ce LAN ne correspond pas au LAN du TP, il s’agit seulement de l’appellation par défaut de Pfsense.

Cette adresse IP doit être dans le range de la vmnet partagé avec votre PC

Dans cette exemple, l’adressage du vmnet1 est 192.168.248.0/24

![Pasted image 20230218144731.png](img/Pasted%20image%2020230218144731.png)

En vérifiant les IP du PC host, on remarque bien que la carte réseau sur vmnet1 a bien une adresse dans le bon range :
![Pasted image 20230218144742.png](img/Pasted%20image%2020230218144742.png)

Nous allons donc assigner l’adresse IP 192.168.248.2 au firewall

Dans Pfsense, sélectionner l’option 2 "Set Interface Ip address".

![Pasted image 20230218144818.png](img/Pasted%20image%2020230218144818.png)

Entrer l’adresse IP : 

![Pasted image 20230218144833.png](img/Pasted%20image%2020230218144833.png)

Puis le masque :
![Pasted image 20230218144844.png](img/Pasted%20image%2020230218144844.png)

Pas de gateway, pas d’IP V6 et pas de DHCP :
![Pasted image 20230218144909.png](img/Pasted%20image%2020230218144909.png)

L’interface de Pfsense est maintenant accessible :
![Pasted image 20230218144918.png](img/Pasted%20image%2020230218144918.png)

Accéder à l’interface via un navigateur à l’adresse 192.168.248.2
![Pasted image 20230218144928.png](img/Pasted%20image%2020230218144928.png)

:::tip 
> Bienvenue dans PfSense ! 
:::


Paramétrer les stations Ubuntu :

Assigner les bonnes adresses IP aux stations Ubuntu et configurer les bonnes cartes réseau virtuelles.

Dans cet exemple :
- Ubuntu LAN : 192.168.200.1 vmnet2
- Ubuntu DMZ : 192.168.201.1 vmnet3

Connecter vous sur l’interface du Pfsense (Username : admin , password : pfsense)

Vérifier le nom des interfaces par rapport aux cartes réseaux :
![Pasted image 20230218145006.png](img/Pasted%20image%2020230218145006.png)

Configurer les interfaces du firewall.
Menu "interfaces" , "LAN"
![Pasted image 20230218145015.png](img/Pasted%20image%2020230218145015.png)

Renommer l’interface LAN en Admin
![Pasted image 20230218145023.png](img/Pasted%20image%2020230218145023.png)

Cliquer sur "Save" puis "Apply Change" pour sauvegarder.

Cette interface servira uniquement à l’accès au firewall depuis votre machine hôte.

Configurer les interfaces OPT1 et OPT2 :
OPT1 deviendra LAN et OPT2 DMZ (à adapter en fonction de votre configuration des cartes)

:::tip To Do 
> Configurer les adresses IP de chaque réseau pour qu’elles correspondent à votre architecture.
:::


N’oubliez pas d’activer vos interfaces.

**LAN (OPT1) :**
![Pasted image 20230218145117.png](img/Pasted%20image%2020230218145117.png)

**DMZ (OPT2) :**
![Pasted image 20230218145145.png](img/Pasted%20image%2020230218145145.png)

Allez dans le menu "Rules" et naviguer sur les différentes interfaces.

Autoriser, pour les interfaces LAN et DMZ, les 2 réseaux avec tous les protocoles pour toutes les destinations en activant les traces.

Exemple pour l’interface LAN :
![Pasted image 20230218145157.png](img/Pasted%20image%2020230218145157.png)

Une fois les règles effectuées, essayer de "pinguer" le firewall depuis les machines Ubuntu LAN et DMZ.

Cela devrait fonctionner si vos règles ont bien été effectuées et si les cartes réseaux des interface LAN et DMZ sont les bonnes. Vous pouvez changer les cartes réseaux des interfaces dans le menu "Interfaces" "Assignments" (attention, un reboot est nécessaire).

:::tip To Do 
> - Essayer de "pinguer" internet (1.1.1.1 par exemple)
> - Aller explorer le menu "Interfaces", "NAT", "outbound"
> - Essayer de pinguer votre machine en DMZ depuis votre machine du LAN.
> - Faire un traceroute depuis votre Ubuntu LAN vers votre Ubuntu DMZ
> - Si la comment traceroute n’est pas installée, taper "apt-get install traceroute"
:::

:::note Question 
> Que remarquez-vous ?
:::

Arrêter vos machines virtuelles et configurer un autre site complet comme sur ce schéma :
![Pasted image 20230218145357.png](img/Pasted%20image%2020230218145357.png)

Vous devez pouvoir « pinguer » internet depuis l’Ubuntu.

## Partie 2 : Configurer un tunnel IPSEC site à site entre le Site1 et le Site 2

![Pasted image 20230218145557.png](img/Pasted%20image%2020230218145557.png)

Configurer les phase 1 et phase 2 de chaque site.


:::note Défintion
> - Phase 1 : AH (Authentification Header)
> - Permet de s’assurer l’intégrité des données et authentifier la provenance de ces données.
> - 
> - Phase 2 :ESP (Encapsulation Security Payload)
> - Assure la confidentialité des données et leur protection.
:::

Les paramètres importants sont :

:::info Important  Phase 1
> - Identification des sites : adresses IP extérieurs (WAN) des firewalls
> - Authentification : comment les 2 firewalls vont s’authentifier entre eux
> - Algorithme de chiffrement de l’authentification : il doit être identique sur les différents firewalls
:::

:::tip  Phase 2
> - Identification des réseaux à joindre : Range IP des réseaux situés à chaque extrémité
> - Algorithme de chiffrement des données :  il doit être identique à chaque extrémité
> - Sur l’interface d’administration de vos Pfsense, vous trouverez tous les paramètres dans l’onglet VPN puis IPSEC.
> - Il est conseillé de faire les 2 sites en même temps.
> - Cliquer sur ADD P1, configurer vos phases 1, cliquer sur SAVE et faites de même pour la phase 2.
:::

Phase1 site 1 et site 2 :
![Pasted image 20230218145909.png](img/Pasted%20image%2020230218145909.png)

Phase 2 site 1 et site 2 :
![Pasted image 20230218145925.png](img/Pasted%20image%2020230218145925.png)![Pasted image 20230218145939.png](img/Pasted%20image%2020230218145939.png)

Si votre configuration est correcte, la connexion VPN est établie :
![Pasted image 20230218145954.png](img/Pasted%20image%2020230218145954.png)

Il faut maintenant autoriser le trafic dans le tunnel VPN.

:::tip To Do 
> Créer les règles dans les interfaces VPN de Pfsense.
> Vous devez pouvoir joindre vos machines entre le LAN du Site1 et le LAN du Site2.
> Ajouter une phase2 pour joindre la DMZ du site1 à partir du LAN du site2.
:::

# Travaux pratiques Docker

## TP 1 - Création d’un container

- Lancez un premier premier container docker, l’image à utiliser devra être « nginx »
- Vérifier si votre container est bien fonctionnel à l’aide de la commande « docker ps »
- Stoppez votre container
- Supprimez votre container et se rassurer qu’il a bien été supprimé.
- Supprimer également les volumes qui auront été créé

## TP 2 - Les images

- Combien d’images docker existent-ils dans votre machine à ce stade ?
- Relancer à nouveau un container docker à l’aide de l’image nginx
- Combien de temps cela prend ? Est-ce le même temps que précédemment?
- Supprimer l’image nginx de votre machine
- Avez-vous pu supprimer la dite images? Pourquoi?
- Supprimer le container nginx
- Supprimer à nouveau l’image nginx de votre machine

## TP 3 - Interaction avec un container

- Lancer le container Ubuntu en mode détaché et interactive avec un tag différent de latest
- Créez une nouvelle machine dans l’environnement de labs
- Dans cette machine, lancez à nouveau le container ubuntu en mode interactive + nouveau terminal
- Supprimez les différents containers présent dans votre machine.
- Lancez le container Ubuntu en mode détaché avec la commande « sleep 4500 »
- Utiliser la commande « docker exec » afin d’exécuter une commande à l’intérieur du container (commande de création d’un dossier portant votre prénom »
- Utiliser docker exec afin d’afficher l’ensemble des fichier et répertoire à l’aide de la commande « ls »
- Utiliser docker exec et passez la commande « /bin/bash »
- Observez et expliquez le comportement de ces différentes commandes puis supprimez vos environnements

## TP 3 - Commandes

- Lancer le container ubuntu
- Vérifier que ce container soit bien lancé et qu’il fonctionne correctement
- Pourquoi le container s’est-il arreté
- Supprimer le container ubuntu
- Lancer à nouveau le container ubuntu en mode attach et en passant en paramètre la commande « sleep 100 » 
- Lancez à nouveau le container ubuntu en mode attach passant la commande « ls » 
- Lancez le container ubuntu en mode détaché avec la commande « sleep 4500 » Observez et expliquez le comportement de ces différentes commandes
- Supprimez l’ensemble des containers présents (fonctionnels ou arrêtés) sur votre machine . Supprimez également les images existantes.

## TP - 4 Interaction avec le container

- Créez un fichier « index.html » en local sur votre machine.
- Lancez le container nginx en l’exposant sur le port 8080 et en ajoutant l’option (-v  fichier_index.html:/usr/share/nginx/html//index.html)
- Et changer la phrase «welcome to nginx »
- Vérifier que le container soit bien lancé
- Vérifiez que l’application de notre container nginx est bien consommable à partir du port 8080 de notre hôte.
- Que constatez vous? Quel est le contenu de la page web affichée?
- Vous remarquerez que les conteneurs sont très limités, vous n’avez pas d’éditeur de fichier installé.
- Installez votre éditeur préféré : apt-get update , puis apt-get install vim , par exemple
- Supprimer les containers, images et votre environnement.