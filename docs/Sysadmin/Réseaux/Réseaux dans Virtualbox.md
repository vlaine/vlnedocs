# Réseaux dans Virtualbox

---

## Network

### A. Le mode "NAT"

Commençons par étudier le mode "NAT", car lorsqu'une machine virtuelle est créée dans VirtualBox, elle hérite de ce mode de fonctionnement. L'avantage de ce mode, c'est que **votre VM accède à Internet**, via la même connexion que l'hôte physique, **sans pour autant obtenir une adresse IP** sur [votre réseau local](https://www.it-connect.fr/les-types-de-reseaux-lan-man-wan-et-pan-pour-les-debutants/ "Les types de réseaux : LAN, MAN, WAN, et PAN pour les débutants"). 

:::note 
> Effectivement, c'est **le serveur DHCP de VirtualBox qui va attribuer une adresse IP à votre machine virtuelle**, et non le serveur DHCP de votre réseau local. Lorsque ce mode est utilisé, VirtualBox crée un réseau local virtuel et isolé du reste de vos machines physiques.
> 
:::

:::caution 
> 
> L'inconvénient de ce mode, c'est que vous ne pouvez pas accéder aux services de votre machine virtuelle (RDP, site Web, etc...) à partir d'une autre machine de votre réseau local. **C'est compréhensible, car c'est le [principe du NAT](https://www.it-connect.fr/le-nat-et-le-pat-pour-les-debutants/) et VirtualBox**, au sein de ce réseau virtuel, va **jouer le rôle de routeur pour que la VM accède au réseau local** puis à Internet. Néanmoins, **VirtualBox intègre un assistant qui permet de créer une règle de redirection de port** pour contourner cette restriction.
:::


Au sein d'une machine virtuelle, la création d'une règle de redirection de port est une opération assez simple. Dans les paramètres de la VM, vous devez cliquer sur "**Réseau**" (1), choisir le mode d'accès réseau "**NAT**" (2) puis cliquer sur le bouton "**Redirection de ports**" (3). Une fenêtre va s'ouvrir afin d'afficher les différentes règles. Cliquez sur le bouton avec un "**+**" pour ajouter une règle (4) et **configurez** la règle (5). Ensuite, validez (6).

Dans l'exemple ci-dessous, on **redirige le port 443 (source et destination)** vers la VM pour faire du HTTPS. Ainsi, **si quelqu'un se connecte sur l'adresse IP correspondante à l'ordinateur physique, sur le port 443 (HTTPS) alors ce flux sera redirigé vers la machine virtuelle !** Il n'est pas forcément nécessaire de configurer les champs "**IP hôte**" et "**IP invité**" sauf si vous souhaitez pointer vers des adresses IP spécifiques, dans le cas où vous avez plusieurs adresses IP.

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-03-800x490.png)

Lorsque le mode NAT est utilisé, la VM hérite toujours de la même adresse IP (10.0.2.15/24) tandis que VirtualBox utilise 10.0.2.2. Ce mode est pratique pour connecter une seule VM à Internet, sans exploiter le réseau local sur lequel est connecté l'hôte physique. Néanmoins, pour profiter de ce mécanisme sur plusieurs machines virtuelles, il est recommandé de créer un "**Réseau NAT**".

### B. Le mode "Réseau NAT"

:::info 
> VirtualBox permet de créer plusieurs réseaux NAT : **les VM connectées à un même réseau NAT peuvent communiquer entre elles, et elles peuvent aussi accéder à Internet grâce au NAT** (comme dans le mode précédent). De la même manière, elles ne sont pas joignables directement depuis l'hôte physique ou depuis une autre machine connectée à votre réseau local (à moins de créer des règles de redirection de ports).
:::


![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/virtualbox-schema-reseau-nat-2022.png)

Pour **créer un nouveau réseau NAT**, car par défaut il n'y en a pas, il faut cliquer sur "**Fichier**" puis "**Préférences**" dans le menu de VirtualBox. Ensuite, dans la partie "**Réseau**", il suffit de cliquer sur le bouton avec un "**+**" pour créer un nouveau réseau NAT. Ensuite, **ce réseau NAT est personnalisable** notamment pour définir une adresse réseau, un nom, et activer la prise en charge du DHCP. Le réseau par défaut est "10.0.2.0/24".

C'est également à cet endroit que l'on crée **les règles de redirection de ports**, sauf que dans ce cas, **il faudra forcément préciser l'adresse IP "IP invité", car sur ce réseau NAT il peut y avoir plusieurs VMs donc VirtualBox doit savoir vers quelle adresse IP il doit rediriger le flux**.

![VirtualBox réseau NAT](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-16.png)

Si je prends l'exemple de deux machines virtuelles connectées à mon nouveau réseau NAT, c'est-à-dire avec la configuration réseau suivante :

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-18.png)

Et bien, je peux constater qu'**elles ont toutes les deux un accès à Internet, avec une adresse IP différente, en étant connectées sur le même réseau**.

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-17-800x465.jpg)

![Pasted image 20230221160105.png](img/Pasted%20image%2020230221160105.png)

```
ssh root@127.0.0.1 -p 2222
```


## III. VirtualBox et l'accès par pont

:::info 
> 
> En sélectionnant le mode "**Accès par pont**" que l'on peut appeler le mode "**Bridge**", la machine virtuelle aura un accès à votre réseau, au même titre que votre ordinateur physique. De ce fait, si elle est configurée en DHCP, elle va solliciter le serveur DHCP de votre réseau local pour obtenir une adresse IP et accéder au réseau local.
> 
> Avec un accès par pont, **la VM pourra contacter les autres machines connectées au réseau et elle pourra être contactée par les autres machines de ce réseau**, contrairement au mode NAT où ce n'était pas possible (à moins de créer une ou plusieurs règles de redirection de ports).
:::


![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/virtualbox-schema-bridge-2022.png)

:::info 
> 
> Si l'on configure l'accès par pont sur une machine virtuelle, il faut **sélectionner la carte réseau de l'ordinateur physique avec laquelle le pont doit être établi**. Dans cet exemple, si je sélectionne la carte "_MediaTek Wi-Fi 6 MT7921 Wireless LAN Card_", cela va me permettre de me **connecter au travers de ma carte Wi-Fi et donc d'accéder au réseau Wi-Fi auquel est connectée ma machine physique**.
:::


![VirtualBox et l'accès par pont](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-04-800x594.png)

Le mode "Accès par pont" est avantageux pour connecter la VM au réseau dans les mêmes conditions que l'ordinateur physique où est installé VirtualBox.

## IV. VirtualBox et le mode Host-Only

:::note 
> 
> Continuons à explorer les différents modes d'accès au réseau, en s'intéressant au mode "**Réseau privé hôte**" que l'on appelle généralement le mode "[**Host-Only**](https://www.it-connect.fr/gestion-des-reseaux-host-only-sous-virtualbox/)". Une **VM connectée en mode "réseau privé hôte" peut contacter l'hôte physique** où est installé VirtualBox et **elle peut contacter également contacter les autres machines virtuelles connectées sur ce même réseau "Host-only"**. Cependant, **la machine virtuelle ne peut pas accéder à votre réseau local, ni même à Internet**.
:::


Ce qui donne la représentation suivante, où j'attire votre attention sur l'absence de lien entre la carte réseau host-only et la carte physique de la machine :

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/virtualbox-schema-host-only-2022.png)

Dans les paramètres d'une machine virtuelle, le mode "réseau privé hôte" doit être sélectionné pour utiliser ce mode, comme ceci :

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-06.png)

Lorsque ce mode est sélectionné, nous avons la possibilité de choisir l'interface sur laquelle se connecter. Par défaut, VirtualBox est livré avec une interface nommée "**VirtualBox Host-Only Ethernet Adapter**" qui est un adapteur réseau virtuel. Cette carte est visible à partir de l'hôte VirtualBox, à côté des cartes physiques :

![VirtualBox - Carte Host-only](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-05.png)

En accédant aux paramètres de VirtualBox, il est envisageable de **créer des interfaces Host-Only supplémentaires**. Voyons comment faire pour créer une interface host-only... À partir de l'interface VirtualBox, cliquez sur "**Fichier**" puis "**Gestionnaire de réseau hôte**".

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-07.png)

La liste des interfaces Host-Only apparaît. Une seule carte est présente, ce qui est normal. Cliquez sur le bouton "**Créer**" pour ajouter une nouvelle interface : cela va nécessiter une élévation de privilèges (droit admin) pour créer la carte donc cliquez sur "**Oui**" lorsque Windows demande une confirmation.

![VirtualBox ajouter interface host-only](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-08-800x476.png)

Voilà, la deuxième carte est créée ! On peut voir que la première carte utilise le réseau "**192.168.56.0/24**" tandis que cette seconde carte utilise le réseau "**192.168.183.0/24**". Donc, c'est bien deux réseaux Host-Only distincts. **Une machine virtuelle connectée au premier réseau Host-Only ne pourra pas communiquer avec une machine virtuelle connectée dans le second réseau Host-Only, mais dans les deux cas il sera possible de communiquer avec l'hôte physique VirtualBox.**

À chaque fois, il est possible d'activer ou non le serveur DHCP de VirtualBox sur l'interface host-only. De ce fait, la VM connectée à un réseau host-only obtiendra directement une adresse IP. Le serveur DHCP de chaque interface host-only se gère dans l'onglet "Serveur DHCP", après avoir sélectionné l'interface dans la liste.

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-09-800x474.png)

## V. VirtualBox et le réseau interne

:::caution 
> 
> **Le mode d'accès "réseau interne" va permettre de créer un réseau virtuel isolé**, où les communications seront possibles uniquement entre les machines virtuelles connectées sur un même réseau interne. De ce fait, une VM connectée à un réseau interne ne peut pas communiquer avec l'hôte physique VirtualBox, ni avec le reste du réseau où est connecté cet hôte physique, ni même avec Internet.
:::


**Ce mode d'accès réseau est intéressant pour reproduire un vrai réseau puisque l'on isole totalement ce réseau virtuel des autres réseaux.** Par exemple, on peut installer et tester son propre serveur DHCP sans risquer de perturber le réseau local de production, ni même être perturbé par le serveur DHCP de votre box, de votre entreprise, ou celui de VirtualBox. **Avec ce mode, c'est à vous de gérer le plan d'adresse IP au sein du réseau virtuel, soit par l'intermédiaire d'un serveur DHCP ou via des adresses IP fixes.** Autrement dit, le serveur DHCP de VirtualBox ne pourra pas vous être utile.

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/virtualbox-schema-reseau-interne-exemple-2022-800x422.png)

:::tip 
> 
> VirtualBox autorise la création de plusieurs réseaux internes, simplement en donnant des noms différents. Par défaut, le réseau interne est nommé "intnet" donc toutes les VMs associées à ce réseau interne pourront communiquer entre elles. Si un autre réseau interne nommé "intnet2" est créé, les VMs de ce nouveau réseau ne pourront pas communiquer avec celles du réseau "intnet".
:::


![VirtualBox réseau interne](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-10.png)

Afin de permettre aux machines virtuelles connectées au réseau interne "intnet" d'accéder à Internet malgré tout, on peut imaginer le scénario ci-dessous. C'est un scénario très intéressant à reproduire dans un lab puisque l'**on positionne son propre routeur/pare-feu (VM 3) pour permettre les communications vers l'extérieur**, de la même façon qu'on le fait avec un réseau réel.

Dans cet exemple, **la VM 3 a une carte réseau configurée dans le réseau interne "intnet" ainsi qu'une carte réseau configurée en mode NAT  afin de permettre aux machines du réseau interne d'accéder à Internet.** La VM 3 peut être un pare-feu PfSense ou OPNsense, mais aussi sous [Linux](https://www.it-connect.fr/cours-tutoriels/administration-systemes/linux/) ou Windows en activant les fonctions de routage.

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/virtualbox-schema-reseau-interne-2022-800x422.png)

## VI. VirtualBox et le Cloud Network

Depuis la version 6.0 de VirtualBox, il est possible d'interagir avec Oracle Cloud Infrastructure (OCI) depuis VirtualBox. De ce fait, à partir de VirtualBox on peut créer une VM sur son environnement OCI et donc il est possible de connecter une VM au réseau du Cloud Oracle. Pour le moment, il s'agit d'une fonctionnalité expérimentale.

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-11.png)

Afin d'utiliser ce mode d'accès réseau, il faut déclarer son profil OCI dans les paramètres de VirtualBox, via "**Fichier**" puis "**Gestionnaire de profils Cloud**".

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-12-800x494.png)

## VII. VirtualBox et le mode "Generic Driver"

Le mode d'accès réseau "**Generic Driver**" est très spécifique et il est probable qu'il soit utilisé assez rarement. Avec ce mode, on peut spécifier un pilote spécifique à utiliser et que l'on a pu obtenir par l'intermédiaire d'un pack d'extensions. D'après la documentation de VirtualBox, il fonctionne selon deux sous-modes :

-   Tunnel UDP
-   Réseau VDE (Virtual Distributed Networking)

![VirtualBox Generic Driver](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-14.png)

Personnellement, je n'ai pas de retour d'expérience à vous partager quant à l'utilisation de ce mode.

## VIII. VirtualBox et le mode "Aucune connexion"

Terminons par le mode "**aucune connexion**" qui peut sembler étonnant au premier abord, mais qui est pratique pour simuler une action très simple : un câble réseau débranché. En effet, avec un adapteur physique, si l'on souhaite simuler une coupure réseau, on peut débrancher le câble RJ45 physiquement, mais comment faire avec une VM ? Et bien, avec VirtualBox, il y a cette option nommée "**Aucune connexion**" et qui permet d'avoir l'**interface réseau virtuelle présente et visible dans la VM,** mais avec le statut "**Câble réseau non connecté**".

![VirtualBox Aucune connexion](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-13.png)

Néanmoins, lorsque l'on souhaite effectuer des [tests](https://www.it-connect.fr/actualites/tests-materiel-logiciel/) et simuler l'action de déconnecter un câble réseau, il y a une autre alternative que l'on peut utiliser : l'option "**Câble branché**" qui est incluse avec les autres modes tels que NAT, le mode pont, etc...

Si la VM est connectée à un réseau NAT, avec une connexion active, il suffit d'accéder aux paramètres de la VM, de décocher l'option "**Câble branché**" pour faire comme si on débranchait le câble. Pour valider le changement, il ne faut pas oublier de cliquer sur "**OK**". Cette opération est faisable à tout moment, y compris lorsque la VM est allumée : ce sera pris en compte en direct.

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-15.png)

## IX. Choisir le type de cartes réseau

**VirtualBox propose plusieurs types de cartes réseau, mais est-ce nécessaire de choisir une autre carte que celle par défaut ?** Tout d'abord, la carte réseau sélectionnée par VirtualBox peut changer en fonction du type d'OS que vous affectez à la VM lors de la création. La première réponse est non, mais dans certains cas, le fait de choisir une autre carte réseau permettra de corriger des problèmes de compatibilité avec un système d'exploitation spécifique. Par exemple, la carte "**Intel PRO/1000 MDT Desktop**" est prise en charge par Windows nativement depuis Windows Vista, tandis que pour les versions précédentes, il faut utiliser le type "**Intel PRO/1000 T Server**".

![Types de cartes réseau VirtualBox](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/tuto-virtualbox-types-de-reseaux-02.png)

Pour conclure, voici un tableau récapitulatif qui, pour chaque mode d'accès réseau, vous montre quels sont les flux possibles :

![](https://www.it-connect.fr/wp-content-itc/uploads/2022/05/virtualbox-synthese-modes-reseau.png)

