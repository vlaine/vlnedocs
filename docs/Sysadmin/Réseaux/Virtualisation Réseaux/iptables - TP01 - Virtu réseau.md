iptables - TP01 - Virtu réseau
---
Related to [[Sysadmin/Réseaux/Virtualisation Réseaux](../Virtualisation%20R%C3%A9seaux.md)

![Pasted image 20230404151511.png](../img/Pasted%20image%2020230404151511.png)

### Configuration initiale : 

VM1 :
	NAT --> ens33
		IP : 192.168.45.200/24
		GT : 192.168.45.2
		DNS : 1.1.1.1
	Host-only (vmnet1) --> ens36
		IP : 192.168.46.254/24
VM2 :
	Host-only (vmnet1) --> ens33
		192.168.46.200/24
		DNS : 1.1.1.1

BUT : ping google.fr depuis VM2
Etapes : 
	VM2(ens33) --> ping --> VM1(192.168.46.254)
	VM2(ens33) --> ping --> VM1(192.168.45.200)
	VM2(ens33) --> ping google.fr 

1) Vérifier son Virtual network editor et y configurer les réseaux
2) Vérifier les settings de vos VM
3) Puis configurer les ips
	1) `nano /etc/network/interfaces`
VM1 : 

![Pasted image 20230404152642.png](../img/Pasted%20image%2020230404152642.png)

VM2: 

![Pasted image 20230404152917.png](../img/Pasted%20image%2020230404152917.png)

A ce stade nous devrions pouvoir pinger l'interface eth1 de VM1 depuis VM2.

![Pasted image 20230404153028.png](../img/Pasted%20image%2020230404153028.png)

#### Ajout de a gateway sur VM1 

Etape 2 : Ping l'interface NAT de VM1 via VM2
Initialement : *
![Pasted image 20230404153124.png](../img/Pasted%20image%2020230404153124.png)

Donc --> Ajout de la gateway sur VM2
![Pasted image 20230404153428.png](../img/Pasted%20image%2020230404153428.png)

Puis test de ping depuis VM2 vers carte NAT de VM1 : 

![Pasted image 20230404153414.png](../img/Pasted%20image%2020230404153414.png)

---
#### Configuration de la règle de firewall
##### A FAIRE SUR VM1
Dernière étape : ping google.fr depuis VM2.

Ajout de la règle de NAT avec iptables.

D'abord autoriser le forwarding dans nos machines : 

```
vim /etc/sysctl.conf
	net.ipv4.ip_forward=1
```

![Pasted image 20230404153841.png](../img/Pasted%20image%2020230404153841.png)

Nous pouvons ensuite ajouter la règle iptables : 

```css
iptables -t nat -A POSTROUTING -s 192.168.92.15 -o ens33 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 192.168.50.44 -o eth0 -j MASQUERADE
```
- -t = type NAT
- -A = Ajout dans la table POSTROUTING
- -s = SOURCE --> IP DE VM2
- -o = out --> ens33 = interface NAT de VM1

Vérifier que la règle est bien ajoutée :
```bash
iptables -L -t nat
```
```
Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination         


MASQUERADE  all  --  172.17.0.0/16        anywhere            
MASQUERADE  all  --  172.18.0.0/16        anywhere            
MASQUERADE  tcp  --  172.18.0.3           172.18.0.3           tcp dpt:http
MASQUERADE  all  --  192.168.50.44        anywhere            

```
Normalement le `ping google.fr` devrait fonctionner.