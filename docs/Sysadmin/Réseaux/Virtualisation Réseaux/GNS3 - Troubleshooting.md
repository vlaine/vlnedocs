GNS3
---

#### Troubleshooting

##### Error
```
Virtualized Intel VT-x/EPT is not supported on this platform.
Continue without virtualized Intel VT-x/EPT?

VMware Workstation does not support nested virtualization on this host.

Module 'HV' power-on failed.

Failed to start the virtual machine
```

###### Resolution

Désactiver l'isolation du noyau Windows : https://gns3.com/virtualized-intel-vt-x-ept-is-not-supported-on-this-platform
On peux tester de désactiver hyper-V : 
```
bcdedit /set hypervisorlaunchtype off
```

Voir aussi ici :
- MEILLEUR SOURCE ICI : https://gns3.com/community/featured/fixing-vt-x-or-amd-v-not-available-in-windows-11-with-vmware-ws-pro-and-player
- https://www.gns3.com/community/discussions/virtualized-intel-vt-x-ept-is-not-supported-on-this-platform-gns3-vm
- https://communities.vmware.com/t5/VMware-Workstation-Pro/Workstation-16-Pro-Error-quot-Virtualized-Intel-VT-x-EPT-is-not/m-p/2866101
- https://kb.vmware.com/s/article/2146361

Etapes clefs :
- Désactivation du "core isolation" de Windows
- Désactiver hyper-V
- Redémarrer le PC
- Désactiver Device Guard :
```
**Disable Device Guard:**

NOTE 1: This portion may or may not apply depending on Windows version

NOTE 2: MS seems to have disabled the group policy editor in Windows 11 Home, to reenable follow these instructions: [Link](https://www.ghacks.net/2021/11/29/how-to-enable-the-group-policy-editor-on-windows-11-home/) (not affiliated with, sponsored by, or validated by GNS3)

1.  Click: [START]
2.  Type (without quotes): "gpedit.msc"
3.  Press: [ENTER]
4.  Navigate in the left-hand tree: Local Computer Policy > Computer Configuration > Administrative Templates > System > Device Guard
5.  Double-Click on: "Turn on Virtualization Based Security"
6.  Select: "Disable"
7.  Click: "OK"
```
- Redémarrer le PC
- Réinstaller Workstation 17.0 (parfois nécessaire)
	- Un "Repair" est possible
- Rédémarrer le PC
- Importer VM GNS3
- L'allumer
Cela devrait fonctionner

### Ajout d'une VM Workstation dans GNS3

https://docs.gns3.com/docs/emulators/adding-vmware-vms-to-gns3-topologies/ 

###### Troubleshooting ajout VM

```
Error while creating link: vnet ethernet0.vnet not in VMX file
```
Ajouter dans le VMX : 
