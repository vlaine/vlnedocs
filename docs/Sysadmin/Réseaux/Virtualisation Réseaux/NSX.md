VMWARE NSX
---
- https://vblog.io/nsx-t-pour-les-nuls-partie-1-42-installation-et-decouverte/ NSX-T
- http://vmwareinsight.com/Articles/2020/5/5803008/NSX-V-Part-8-Exclude-from-Firewall NSX-V
- Download eval: https://customerconnect.vmware.com/fr/evalcenter?p=nsx-t-eval
- VMware NSX-T	2023-05-04	==HN425-MKJ46-U8TH4-053A2-95GM4==
- https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/products/nsx/vmw-nsx-network-virtualization-design-guide.pdf

NSX-T VS NSX-T
https://www.vmwarearena.com/difference-between-vmware-nsx-v-and-nsx-t/

Associé à [[Sysadmin/Réseaux/Virtualisation Réseaux](../Virtualisation%20R%C3%A9seaux.md) et [[Sysadmin/Réseaux/Virtualisation Réseaux/Réseaux dans Virtualbox](R%C3%A9seaux%20dans%20Virtualbox.md)


## Pré requis important
- Mettre un mot de passe directement au déploiement : 12 caractères maj min chiffre symb
- Optionnel : Serveur DNS
- Heure : pool.ntp.org
- LICENCE TRES IMPORTANT : EVAL dispo de 60 jours sinon on peux pas installer sur un esxi
- Les vibs installé sur les ESXi du cluster ou installera nsx
	- A faire sur les esxi distants : 
```sql
esxcli software vib get --vibname esx-vxlan
esxcli software vib get --vibname esx-dvfilter-switch-security
esxcli software vib get --vibname esx-vsip

 esxcli software vib install -v VMware_bootbank_esx-nsxv_6.5.0-0.0.20506747.vib --force
  esxcli software vib install -d vxlan.zip --force
```
Les mibs pour 6.5 sont dans le dossier du NAS (vxlan.zip)

Voir les vibs dispo : 
```shell
https://192.168.1.16/bin/vdn/nwfabric.properties
```

Erreur bizarre et correction :

```shell
[root@esxi02:/vmfs/volumes/6335fd44-04c6f731-a4a3-000c299a23df/vibs] esxcli software vib install -v VMware_bootbank_esx-nsxv_6.5.0-0.0.20506747.vib -
-force
 [VibDownloadError]
 ('VMware_bootbank_esx-nsxv_6.5.0-0.0.20506747.vib', '/tmp/vibtransaction/tmp.vib', '[Errno 2] Local file does not exist: /var/log/vmware/VMware_bootbank_esx-nsxv_6.5.0-0.0.20506747.vib')
        url = VMware_bootbank_esx-nsxv_6.5.0-0.0.20506747.vib
 Please refer to the log file for more details.
[root@esxi02:/vmfs/volumes/6335fd44-04c6f731-a4a3-000c299a23df/vibs] cp esxcli software vib install -v VMware_bootbank_esx-nsxv_6.5.0-0.0.20506747.vi
b --force
[root@esxi02:/vmfs/volumes/6335fd44-04c6f731-a4a3-000c299a23df/vibs] cp VMware_bootbank_esx-nsxv_6.5.0-0.0.20506747.vib /var/log/
[root@esxi02:/vmfs/volumes/6335fd44-04c6f731-a4a3-000c299a23df/vibs] cp VMware_bootbank_esx-nsxv_6.5.0-0.0.20506747.vib /var/log/vmware
```

Une fois la vibs installé, go to vcenter et faire Resoudre pour tester :
![Pasted image 20230305171633.png](../img/Pasted%20image%2020230305171633.png)
https://kb.vmware.com/s/article/78898

![Pasted image 20230305171645.png](../img/Pasted%20image%2020230305171645.png)

![Pasted image 20230305171744.png](../img/Pasted%20image%2020230305171744.png)


https://www.vmwarearena.com/vmware-nsx-installation-part-10-create-nsx-logical-switch/


---
Le NSX de VMware est une solution de mise en réseau définie par logiciel qui offre une mise en réseau et une sécurité virtualisées entièrement dans le logiciel. En ce qui concerne VMware NSX, nous avons maintenant deux offres NSX-V (NSX pour vSphere) et NSX-T (NSX Transformers). Les deux ont une grande capacité et des ensembles de fonctionnalités dans l'espace SDN. Nous devons comprendre la différence entre VMware NSX-V et NSX-T pour l'implémenter dans les cas d'utilisation et les scénarios de déploiement appropriés. Cet article vous aidera à comprendre la principale différence entre VMware NSX-V et NSX-T.

![Pasted image 20230305121402.png](../img/Pasted%20image%2020230305121402.png)

---
Générer un certificat valide sinon pas d'accès à l'interface UI
Avec l'ip si pas de DNS, sinon ajouter le nom.
![Pasted image 20230305112437.png](../img/Pasted%20image%2020230305112437.png)

Ajouter : ![Pasted image 20230305120422.png](../img/Pasted%20image%2020230305120422.png)
On doit voir ça dans vcenter : 

![Pasted image 20230305120411.png](../img/Pasted%20image%2020230305120411.png)

Command line 

```
  list
  enable
  exit
  quit
  ping WORD
  traceroute WORD
  ping ipv6 WORD
  traceroute ipv6 WORD
  show ip route
  show ipv6 route
  show system uptime
  show clock
  show version
  show tech-support
  reset
  show process (list|monitor)
  show arp
  show system memory
  show log system
  show log system (follow|reverse|size)
  show log system last NUM
  show filesystems
  show ethernet
  show log manager
  show log manager (follow|reverse|size)
  show log manager last NUM
  show log appmgmt
  show log appmgmt (follow|reverse|size)
  show log appmgmt last NUM
  show log replicator
  show log replicator (follow|reverse|size)
  show log replicator last NUM
  show interface mgmt
  show slots
  debug connection WORD
  show cluster all
  show cluster all details
  show cluster CLUSTER-ID
  show host HOST-ID
  show vm VM-ID
  show vnic VNIC-ID
  show dfw cluster all
  show dfw cluster CLUSTER-ID
  show dfw host HOST-ID
  show dfw host HOST-ID summarize-dvfilter
  show dfw host HOST-ID getfilters
  show dfw vm VM-ID
  show dfw vnic VNIC-ID
  show dfw host HOST-ID filter FILTER-NAME rules
  show dfw host HOST-ID filter FILTER-NAME stats
  show dfw host HOST-ID filter FILTER-NAME filterstats
  show dfw host HOST-ID filter FILTER-NAME rule RULE-ID
  show dfw host HOST-ID filter FILTER-NAME addrsets
  show dfw host HOST-ID filter FILTER-NAME addrsets addrset ADDRSET
  show dfw host HOST-ID filter FILTER-NAME flows
  show dfw host HOST-ID filter FILTER-NAME spoofguard
  show dfw host HOST-ID filter FILTER-NAME discoveredips
  show dfw host HOST-ID filter FILTER-NAME discoveredips stats
  show dlb host HOST-ID filter FILTER-NAME addrsets ADDRSETS validity show
  show dlb host HOST-ID filter FILTER-NAME addrsets ADDRSETS stats (table|entry|all)
  set host event notification enable
  set host event notification disable
  get host event notification status
  get host event notification status detailed
  set host event notification throttle [time-sec]
  get manager event notification throttle
  set manager event notification throttle [time-sec]
  get host event notification throttle
  get host event notification throttle detailed
  set logical-switches mac ageing [TIME_SEC]
  set logical-switches arp ageing [TIME_SEC]
  set logical-switches mac table-size [SIZE]
  set logical-switches arp table-size [SIZE]
  set logical-routers arp ageing [TIME_SEC]
  set tunables default
  set tunables HOST-ID sync
  show logical-routers arp ageing
  show logical-routers arp ageing detailed
  show logical-routers host HOST-ID arp ageing
  show logical-routers arp retry
  show logical-routers arp retry detailed
  show logical-routers host HOST-ID arp retry
  show logical-routers arp probe
  show logical-routers arp probe detailed
  show logical-routers host HOST-ID arp probe
  show logical-switches mac ageing
  show logical-switches mac ageing detailed
  show logical-switches host HOST-ID mac ageing
  show logical-switches mac table-size
  show logical-switches mac table-size detailed
  show logical-switches host HOST-ID mac table-size
  show logical-switches arp ageing
  show logical-switches arp ageing detailed
  show logical-switches host HOST-ID arp ageing
  show logical-switches arp table-size
  show logical-switches arp table-size detailed
  show logical-switches host HOST-ID arp table-size
  show logical-switches proxy-arp
  show logical-switches proxy-arp detailed
  show logical-switches host HOST-ID proxy-arp
  show tunables default
  show tunables detailed
  show tunables host HOST-ID
  show edge all
  show edge EDGE-ID
  show edge (EDGE-ID|EDGE-ID.HA-ID) (version|eventmgr|log|ipset|nat|arp|firewall|flowtable|arp-filter)
  show edge (EDGE-ID|EDGE-ID.HA-ID) log (reverse|routing)
  show edge (EDGE-ID|EDGE-ID.HA-ID) log routing reverse
  show edge (EDGE-ID|EDGE-ID.HA-ID) process (list|snapshot)
  show edge (EDGE-ID|EDGE-ID.HA-ID) system (cpu|memory|storage|network-stats)
  show edge (EDGE-ID|EDGE-ID.HA-ID) messagebus (forwarder|messages)
  show edge (EDGE-ID|EDGE-ID.HA-ID) configuration (application-set|bgp|certificatestore|dhcp|dns|firewall|global|highavailability|interface|interface-set|ipsec|ipset|l2vpn|loadbalancer|multicast|nat|ospf|provider-appset|provider-ipset|routing-global|snmp|sslvpn-plus|static-routing|syslog)
  show edge (EDGE-ID|EDGE-ID.HA-ID) configuration loadbalancer (monitor|pool|rule|virtual)
  show edge (EDGE-ID|EDGE-ID.HA-ID) interface INTERFACE-NAME
  show edge (EDGE-ID|EDGE-ID.HA-ID) firewall flows topN <0-1024>
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip (bgp|forwarding|ospf|route|igmp|pim|mcast)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip route (PREFIX|bgp|ospf|connected|static)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip route bgp (PREFIX)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip route ospf (PREFIX)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip route connected (PREFIX)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip route static (PREFIX)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip forwarding PREFIX
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip mcast (route|forwarding)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip mcast route (PREFIX)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip bgp neighbors
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip ospf (database|interface|neighbors|statistics)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip ospf database (adv-router|asbr-summary|external|network|nssa-external|opaque-area|router|summary)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip igmp (membership|interfaces)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip pim (groups|neighbors|interfaces|bsr)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip pim groups (mapping)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip pim interfaces (stats)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ip pim neighbors (stats)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ipv6 (forwarding|neighbors)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ipv6 forwarding (PREFIX|static)
  show edge (EDGE-ID|EDGE-ID.HA-ID) ipv6 neighbors (IP_ADDR)
  show edge (EDGE-ID|EDGE-ID.HA-ID) packet drops
  show edge (EDGE-ID|EDGE-ID.HA-ID) service (loadbalancer|monitor|ipsec|highavailability|dhcp|dns|l2vpn|sslvpn-plus)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service dns (cache|zones)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service loadbalancer (error|monitor|pool|session|table|virtual)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service loadbalancer session (l4|l7)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service monitor service NAME
  show edge (EDGE-ID|EDGE-ID.HA-ID) service ipsec (site|cacerts|crls|certs|pubkeys|stats|sp|sa|passthrough)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service ipsec stats (sa|ikesa)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service ipsec certs (database)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service l2vpn (conversion-table|trunk-table|bridge|ebtables|site)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service sslvpn-plus (sessions|stats|tunnels)
  show edge (EDGE-ID|EDGE-ID.HA-ID) service dhcp leaseinfo
  show edge (EDGE-ID|EDGE-ID.HA-ID) service highavailability (connection-sync|internal|link)
  show edge (EDGE-ID|EDGE-ID.HA-ID) flowtable rule-id ID
  show edge (EDGE-ID|EDGE-ID.HA-ID) dnslookup SERVER DOMAIN
  show controller list all
  show logical-switch controller CONTROLLER-ID vni VNI-ID (brief|mac|vtep|arp|connection|statistics)
  show logical-switch controller CONTROLLER-ID host HOST-ID (mac|vtep|arp|joined-vnis)
  show logical-switch host HOST-ID vni VNI-ID (verbose|mac|vtep|arp|statistics)
  show logical-switch host HOST-ID (verbose|config-by-vsm|statistics)
  show logical-switch host HOST-ID vni VNI-ID port NAME statistics
  show vm network info host HOST-ID
  show vm network info host HOST-ID wid WORLD-ID
  show logical-switch list all
  show logical-switch list host HOST-ID vni
  show logical-switch list vni VNI-ID host
  show logical-router host HOST-ID connection
  show logical-router host HOST-ID dlr DLR-ID (brief|verbose|route|arp|tunable|control-plane-statistics|mrouting-domain)
  show logical-router host HOST-ID dlr DLR-ID interface NAME (brief|verbose|statistics)
  show logical-router host HOST-ID dlr DLR-ID bridge NAME (verbose|mac-address-table|statistics)
  show logical-router host HOST-ID dlr DLR-ID igmp-group IP (brief|verbose)
  show logical-router list all
  show logical-router list dlr DLR-ID host
  show logical-router resolve host HOST-ID dlr DLR-ID destip DEST-IP
  show logical-router resolve host HOST-ID dlr DLR-ID destip DEST-IP destmask DEST-MASK
  show logical-router resolve host HOST-ID dlr DLR-ID destip DEST-IP srcip SRC-IP
  show logical-router resolve host HOST-ID dlr DLR-ID destip DEST-IP destmask DEST-MASK srcip SRC-IP
  show logical-router controller CONTROLLER-ID statistics
  show logical-router controller CONTROLLER-ID host HOST-ID connection
  show logical-router controller CONTROLLER-ID dlr DLR-ID (brief|interface|route|statistics)
  show logical-router controller CONTROLLER-ID dlr DLR-ID (interface|route|bridge) NAME
  show logical-router controller CONTROLLER-ID dlr DLR-ID bridge NAME mac-address-table
  show edge (EDGE-ID|EDGE-ID.HA-ID) nat64 (bib|rules|sessions|statistics)
  show hardware-gateway list
  show hardware-gateway hsc HSC-ID (brief|certificate)
  show hardware-gateway replicator-nodes
  show hardware-gateway binding all
  show hardware-gateway binding hsc HARDWARE-SWITCH-CONTROLLER-ID all
  show hardware-gateway binding vni VNI-ID all
  show hardware-gateway binding hsc HARDWARE-SWITCH-CONTROLLER-ID vni VNI-ID
  show hardware-gateway host HOST-ID (vnis|bfd-tunnels)
  show hardware-gateway controller CONTROLLER-ID (list|port-bindings|control-nodes)
  show hardware-gateway controller CONTROLLER-ID hsc HARDWARE-SWITCH-CONTROLLER-ID (certificate|inventory)
  show hardware-gateway agent AGENT-IP (status|replication-cluster|hardware-gateway|logical-switches|logging-level|dump)
  show hardware-gateway agent AGENT-IP hardware-gateway UUID
  show hardware-gateway agent AGENT-IP hardware-gateway TOR-ID (tunnels|physical-inventory|bindings|local-macs)
  show hardware-gateway agent AGENT-IP hardware-gateway TOR-ID local-macs VNI
  set hardware-gateway agent AGENT-IP logging-level (ERROR|WARN|INFO|DEBUG|TRACE)
  show PACKAGE-NAME logging-level
  set PACKAGE-NAME logging-level (OFF|FATAL|ERROR|WARN|INFO|DEBUG|TRACE)
  show controller CONTROLLER-IP (java-domain|native-domain) logging-level
  set controller CONTROLLER-IP java-domain logging-level (OFF|FATAL|ERROR|WARN|INFO|DEBUG|TRACE)
  set controller CONTROLLER-IP native-domain logging-level (ERROR|WARN|INFO|DEBUG|TRACE)
  show host HOST-ID (netcpa|vdl2|vdr) logging-level
  set host HOST-ID netcpa logging-level (FATAL|ERROR|WARN|INFO|DEBUG)
  set host HOST-ID vdl2 logging-level (ERROR|INFO|DEBUG)
  set host HOST-ID vdr logging-level (OFF|ERROR|INFO)
  show host HOST-ID health-status
  show host HOST-ID health-status detail
  show interface****
```

