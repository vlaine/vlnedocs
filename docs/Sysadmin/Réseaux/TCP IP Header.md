Paquet TCP/IP - Composition
---
1.  IP header:
	-   IP version
	-   IHL (IP header length)
	-   TOS (Type of Service)
	-   Packet size
	-   IPID (IP Identifier)
	-   Fragmentation
	-   TTL (Time To Live)
	-   Protocol (TCP, UDP, ICMP, etc.)
	-   Checksum of the IP header
	-   Source IP address
	-   Destination IP address

2.  TCP header:
	-   Source Port
	-   Destination Port
	-   Sequence Number
	-   Acknowledgment Number
	-   TCP/HL (TCP header length)
	-   TCP Flags Field (CWR, ECN-Echo, URG, ACK, PSH, RST, SYN, FIN)
	-   TCP Window Size
	-   Checksum of the TCP header
	-   Urgent Pointer Field
	-   TCP Options Field (Max Segment Size, Window Scale, Selective ACK OK, Timestamp, etc.)

![Pasted image 20230330085710.png](img/Pasted%20image%2020230330085710.png)

Un paquet TCP/IP est constitué de deux parties : l'en-tête et les données (payload). L'en-tête contient des informations de contrôle nécessaires pour acheminer le paquet de manière fiable à travers le réseau. Voici la composition de l'en-tête TCP/IP :

Le paquet TCP/IP est composé des éléments suivants :
1.  La version IP - les 4 premiers bits (1 hex) représentent ipv4 ou ipv6. Les 4 bits suivants (1 nibble) représentent la longueur de l'en-tête IP (IHL) en multiples de 4. Par exemple, un IHL de 5 signifie que la longueur de l'en-tête IP est de 20 octets (5 x 4).
2.  TOS (Type of Service) - un champ qui a à voir avec la priorisation du trafic. 00 signifie qu'il n'y a pas de priorisation.
3.  Taille du paquet - la taille totale du paquet en octets, pour que le routeur sache combien d'espace allouer dans le tampon. Par exemple, "00 28" en hexadécimal signifierait 40 octets.
4.  Identificateur IP (IPID) - l'identificateur du paquet pour que l'extrémité réceptrice sache comment organiser les données.
5.  Fragmentation - ce champ fait référence à la façon dont les paquets sont fragmentés. Une valeur de "4000" signifie Ne pas fragmenter, "2" signifie À fragmenter, "8" est réservé, "0" est le dernier paquet fragmenté. 
6.  TTL (Time to Live) - une valeur qui indique le nombre de sauts maximum qu'un paquet peut effectuer avant d'être abandonné. Dans cet exemple, "40" en hexadécimal signifie un TTL de 64.
7.  Encodage - ce champ indique l'encodage IP de ce paquet. Dans cet exemple, une valeur de "06" signifie simplement TCP, 01 signifie ICMP, 11 signifie UDP, 02 signifie IGMP, 09 signifie IGRP, 2F signifie GRE, 32 signifie ESP, 33 signifie AH, 39 signifie SKIP, 58 signifie EIGRP, 59 signifie OSPF, 73 signifie L2TP. 
8.  Checksum de l'en-tête IP - pour valider que l'en-tête n'a pas été modifié.
9.  Adresse IP source. : 32 bits - adresse IP de la machine qui a envoyé le paquet.
10.  Adresse IP de destination. 32 bits - adresse IP de la machine qui doit recevoir le paquet.
11.  Port source : 16 bits - numéro de port utilisé par l'application qui a envoyé le paquet.
12.  Port de destination : 16 bits - numéro de port utilisé par l'application qui doit recevoir le paquet.
13.  Numéro de séquence TCP - utilisé par la couche transport pour ordonner les données, utilisé par le protocole TCP pour numéroter les octets de données envoyés dans le paquet.
14.  Champ d'accusé de réception (ACK) - utilisé pour reconnaître la réception des données.
15.  Longueur de l'en-tête TCP (TCP/HL) - un champ qui indique la longueur de l'en-tête TCP en multiples de 4. Par exemple, "50" en hexadécimal signifie une longueur de 20 octets.
16.  Champs de drapeaux TCP - un champ de 2 octets (8 bits) qui représente les différents drapeaux TCP activés (CWR, ECN-Echo, URG, ACK, PSH, RST, SYN ou FIN). Les bits sont alignés comme suit : | C | E | U | A | P | R | S | F | Dans cet exemple, les caractères hexadécimaux sont "11", ce qui équivaut à 17 en décimal, et les drapeaux ACK et FIN sont activés.
17. 1.  Taille de la fenêtre TCP - indique le nombre de bytes qui peuvent être transférés vers la destination avant qu'un ACK ne soit envoyé.
18.  Checksum de l'en-tête TCP - utilisé pour valider l'intégrité du champ d'en-tête TCP. 
19.  Pointeur urgent - utilisé pour identifier l'emplacement des données urgentes à l'intérieur du paquet. Dans la plupart des cas, il sera "00 00".
20.  Champ d'options TCP - un champ qui peut contenir de 0 à 40 octets. Ce champ dépend de la longueur de l'en-tête TCP (TCP/HL) et contient des options telles que la taille maximale de segment, la mise à l'échelle de la fenêtre, l'accusé sélectif, l'horodatage, etc. Dans cet exemple, le champ d'options TCP est vide car la longueur de l'en-tête TCP est de 20 octets et que la valeur du champ TCP/HL est "5".
21. Payload : Les octets restants du paquet contiennent la charge utile ou les données transmises.

1.  IP header:
	-   IP version
	-   IHL (IP header length)
	-   TOS (Type of Service)
	-   Packet size
	-   IPID (IP Identifier)
	-   Fragmentation
	-   TTL (Time To Live)
	-   Protocol (TCP, UDP, ICMP, etc.)
	-   Checksum of the IP header
	-   Source IP address
	-   Destination IP address

2.  TCP header:
	-   Source Port
	-   Destination Port
	-   Sequence Number
	-   Acknowledgment Number
	-   TCP/HL (TCP header length)
	-   TCP Flags Field (CWR, ECN-Echo, URG, ACK, PSH, RST, SYN, FIN)
	-   TCP Window Size
	-   Checksum of the TCP header
	-   Urgent Pointer Field
	-   TCP Options Field (Max Segment Size, Window Scale, Selective ACK OK, Timestamp, etc.)