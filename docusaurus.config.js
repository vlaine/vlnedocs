// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

// const lightCodeTheme = require('prism-react-renderer').theme.github;
// const darkCodeTheme = require('prism-react-renderer').theme.dracula;
const lightCodeTheme = require('prism-react-renderer').themes.github;
const darkCodeTheme = require('prism-react-renderer').themes.dracula;


/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Vincent Lainé - Documentations - test ',
  tagline: '',
  url: 'https://docs.vlne.fr',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/logo.png',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'vlaine', // Usually your GitHub org/user name.
  projectName: 'vlnedocs', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr-fr',
    locales: ['fr-fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/vlaine/vlnedocs/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/vlaine/vlnedocs/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({     
      navbar: {
        title: '',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo.png',
        },
        items: [
          {
            //type: 'doc',
           // docId: '/docs/intro',
            position: 'left',
            label: 'Docs',
            to: '/docs/intro',
          },
          {
            type: 'doc',
            docId: 'CV/CV',
            position: 'left',
            label: 'CV',
            to: 'CV/CV',
          },
          // {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://github.com/vlaine5/',
            label: 'GitHub',
            position: 'right',
          },
          {
            type: 'docsVersionDropdown',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'About Me',
            items: [
             {
              label: 'LinkedIn',
              href: 'https://www.linkedin.com/in/vincent-laine5/',
             },
//              {
//                label: 'Discord',
//                href: 'https://discordapp.com/',
//              },
//              {
//                label: 'Twitter',
//                href: 'https://twitter.com/',
//              },
            ],
          },
          {
            title: 'More',
            items: [
//              {
//                label: 'Blog',
//                to: '/blog',
//              },
              {
                label: 'Github',
                href: 'https://github.com/vlaine5',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Vincent LAINE, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ['php','bash','powershell','nginx','markdown','makefile','json','http','git','docker','css','sql'],
      },
    }),
};
module.exports = config;
